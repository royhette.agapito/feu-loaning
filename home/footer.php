    <footer>
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <div class="footer-col">
              <figure class="footer-logo"><a href="index.php"><img src="img/master/logo.png" alt=""></a></figure>
              <p>Savings and Loan Management System </p>
            </div>
          </div>
          <div class="col-lg-2">
            <div class="footer-col">
              <h5>POPULAR SITES</h5>
              <ul class="list-group list-group-flush">
                <li class="list-group-item"><i class="fa fa-angle-right" aria-hidden="true"></i>  &nbsp; <a href="about.php">Home</a></li>
                <li class="list-group-item"><i class="fa fa-angle-right" aria-hidden="true"></i>  &nbsp; <a href="services.php">About</a></li>
                <li class="list-group-item"><i class="fa fa-angle-right" aria-hidden="true"></i>  &nbsp; <a href="contact.php">Contact</a></li>
                <li class="list-group-item"><i class="fa fa-angle-right" aria-hidden="true"></i>  &nbsp; <a href="faq.php">Faq</a></li>
              </ul>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="footer-col last-col">
              <div class="contact-information">
                  <h2>Get in Touch</h2>
                  <p>Register and get your loan now!</p>
                  <hr class="contact">
                  <p class="address">P. Paredes, Sampaloc, Manila, Metro Manila</p>
                  <p class="phone">+11-225-888-888-66</p>
                  <p class="email">jofbombasi@gmail.com</p>
                  <hr class="contact">
                  <div class="social-contact"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></div>
                  <div class="social-contact"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></div>
                  <div class="social-contact"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></div>
                  <div class="social-contact"><a href="#"><i class="fa fa-rss" aria-hidden="true"></i></a></div>
                </div>
            </div>
          </div>
        </div>
        <hr class="footer">
        <div class="bottom-footer">
          <div class="copytight">
            <p>© 2019 CCS Savings and Loan Management System</p>
          </div>
          <div class="social-footer">
            <div class="social-items">
              <a href="#">
                <div class="icon-fa"><i class="fa fa-facebook" aria-hidden="true"></i></div>
              </a>
            </div>
            <div class="social-items">
              <a href="#">
                <div class="icon-fa"><i class="fa fa-twitter" aria-hidden="true"></i></div>
              </a>
            </div>
            <div class="social-items">
              <a href="#">
                <div class="icon-fa"><i class="fa fa-instagram" aria-hidden="true"></i></div>
              </a>
            </div>
            <div class="social-items">
              <a href="#">
                <div class="icon-fa"><i class="fa fa-dribbble" aria-hidden="true"></i></div>
              </a>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <a href="#0" class="cd-top">Top</a>
    <!-- ==============================================
      JAVASCRIPTS
      =============================================== -->
    <script src="js/plugins.js"></script>     
    <script src="js/finnexia.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-101241150-1', 'auto');
      ga('send', 'pageview');
    </script> 
  </body>
</html>