<?php include 'header.php'; ?>
  
  <div class="carousel">
    <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <div class="carousel-img-1"></div>
          <div class="carousel-caption">
            <h1 class="animated fadeInLeft">Be Life Confident</h1>
            <h5 class="animated fadeInUp">Growing Your Dreams.</h5>
            <div class="slider-btn">
              <div class="animated inner-btn fadeInUp"><a href="#" class="btn btn-slider">LEARN MORE</a></div>
            </div>
          </div>
        </div>
        <div class="carousel-item">
          <div class="carousel-img-2"></div>
          <div class="carousel-caption">
            <h1 class="animated fadeInLeft">Planning For The Future.</h1>
            <h5 class="animated fadeInUp">Strategies To Grow and Protect.</h5>
            <div class="slider-btn">
              <div class="animated inner-btn fadeInUp"><a href="#" class="btn btn-slider">LEARN MORE</a></div>
            </div>
          </div>
        </div>
        <div class="carousel-item">
          <div class="carousel-img-3"></div>
          <div class="carousel-caption">
            <h1 class="animated fadeInLeft">Get Your Start Here</h1>
            <h5 class="animated fadeInUp">Making Your Future Our Business.</h5>
            <div class="slider-btn">
              <div class="animated inner-btn fadeInUp"><a href="#" class="btn btn-slider">LEARN MORE</a></div>
            </div>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
      </a>
    </div>
  </div>
  <section>
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          <div class="front-section">
            <h6>Your satisfaction defines us</h6>
            <h2>We take your business further</h2>
            <hr class="left">
            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, making this the first true generator on the Internet. It uses a dictionary.</p>
            <div class="front-options">
              <div class="row">
                <div class="col-md-6">
                  <div class="border-icon">
                    <figure class="icon-center"><img src="img/master/money.png" alt=""></figure>
                  </div>
                  <h4>Consultation</h4>
                  <p>looked up one of the more obscure Latin words, consectetur</p>
                </div>
                <div class="col-md-6">
                  <div class="border-icon">
                    <figure class="icon-center"><img src="img/master/saved.png" alt=""></figure>
                  </div>
                  <h4>Saving Adviser</h4>
                  <p>looked up one of the more obscure Latin words, consectetur</p>
                </div>
                <div class="col-md-6">
                  <div class="border-icon">
                    <figure class="icon-center"><img src="img/master/grow.png" alt=""></figure>
                  </div>
                  <h4>Financial Strategy</h4>
                  <p>looked up one of the more obscure Latin words, consectetur</p>
                </div>
                <div class="col-md-6">
                  <div class="border-icon">
                    <figure class="icon-center"><img src="img/master/loan.png" alt=""></figure>
                  </div>
                  <h4>Personal Loans</h4>
                  <p>looked up one of the more obscure Latin words, consectetur</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 front-left">
          <div class="row hover-effects image-hover">
            <div class="col-md-12">
              <div class="image-box">
                <figure><img src="img/images/pic1.jpg" alt="" /></figure>
              </div>
            </div>
            <div class="col-md-12 center-div">
              <div class="image-box">
                <figure><img src="img/images/pic2.jpg" alt="" /></figure>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid counter-parallax">
      <div class="container">
        <div class="row">
          <div class="col-sm-3 col-md-3">
            <div class="counter-statistics">
              <div class="counter">2999</div>
              <h5>DEVELOPED PROJECTS</h5>
            </div>
          </div>
          <div class="col-sm-3 col-md-3">
            <div class="counter-statistics">
              <div class="counter">13111</div>
              <h5>HAPPY CUSTOMERS</h5>
            </div>
          </div>
          <div class="col-sm-3 col-md-3">
            <div class="counter-statistics">
              <div class="counter">877</div>
              <h5>AWARDS</h5>
            </div>
          </div>
          <div class="col-sm-3 col-md-3">
            <div class="counter-statistics">
              <div class="counter">2310</div>
              <h5>ASSOCIATED COMPANIES</h5>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container-fluid inner-color">
      <div class="container">
        <div class="section-title">
          <h2>Why Is It Special?</h2>
          <hr class="center">
          <p>leap into electronic typesetting, remaining essentially unchanged</p>
        </div>
        <div class="row">
          <div class="col-lg-4">
            <div class="front-thumbnail">
              <figure class="box-pic"><a href="#"><img src="img/images/img1.jpg" alt=""></a></figure>
              <div class="rounded-circle">
                <figure class="center-circle-icon"><img src="img/master/cost.png" alt=""></figure>
              </div>
              <div class="figure-caption">
                <h4>Easy to use</h4>
                <p>We build pretty complex tools and this allows us to take designs and turn them into functional quickly and easily.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 center-div">
            <div class="front-thumbnail">
              <figure class="box-pic"><a href="#"><img src="img/images/img3.jpg" alt=""></a></figure>
              <div class="rounded-circle">
                <figure class="center-circle-icon"><img src="img/master/deal.png" alt=""></figure>
              </div>
              <div class="figure-caption">
                <h4>Powerful design</h4>
                <p>We build pretty complex tools and this allows us to take designs and turn them into functional quickly and easily.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="front-thumbnail">
              <figure class="box-pic"><a href="#"><img src="img/images/img2.jpg" alt=""></a></figure>
              <div class="rounded-circle">
                <figure class="center-circle-icon"><img src="img/master/document.png" alt=""></figure>
              </div>
              <div class="figure-caption">
                <h4>Customizability</h4>
                <p>We build pretty complex tools and this allows us to take designs and turn them into functional quickly and easily.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

<?php include 'footer.php'; ?>