<?php include 'header.php'; ?>
<div class="error-page">
  <div class="middle">
    <h1>COMING SOON</h1>
    <h4>Oops! This page can't be found</h4>
    <p>Sorry, but the page you are looking for does not exist or has been removed.</p>
  </div>
</div>
<?php include 'footer.php'; ?>