<?php include 'header.php'; ?>
<div class="sections">
  <div class="container">
    <div class="pages-title">
      <h1>About Us</h1>
      <p>Home &nbsp; > &nbsp; About</p>
    </div>
  </div>
</div>
<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <div class="about">
          <h6>WE TAKE YOUR BUSINESS FURTHER</h6>
          <h2>Planning For The Future</h2>
          <hr class="left">
          <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, making this the first true generator on the Internet. It uses a dictionary.</p>
          <p>Generators on the Internet tend to repeat predefined chunks as necessary</p>
          <p><a class="btn btn-custom" href="#" role="button">LEARN MORE</a></p>
        </div>
      </div>
      <div class="col-lg-6">
        <figure class="about-pic"><img src="img/images/img6.jpg" alt=""></figure>
      </div>
    </div>
    <hr class="about">
    <div class="front-options about-services">
      <div class="row">
        <div class="col-md-6 col-lg-3">
          <div class="border-icon">
            <figure class="icon-center"><img src="img/master/money.png" alt=""></figure>
          </div>
          <h4>Consultation</h4>
          <p>Looked up one of the more obscure Latin.</p>
        </div>
        <div class="col-md-6 col-lg-3">
          <div class="border-icon">
            <figure class="icon-center"><img src="img/master/saved.png" alt=""></figure>
          </div>
          <h4>Saving Adviser</h4>
          <p>Looked up one of the more obscure Latin.</p>
        </div>
        <div class="col-md-6 col-lg-3">
          <div class="border-icon">
            <figure class="icon-center"><img src="img/master/grow.png" alt=""></figure>
          </div>
          <h4>Tax Adviser</h4>
          <p>Looked up one of the more obscure Latin.</p>
        </div>
        <div class="col-md-6 col-lg-3">
          <div class="border-icon">
            <figure class="icon-center"><img src="img/master/loan.png" alt=""></figure>
          </div>
          <h4>Loan Adviser</h4>
          <p>Looked up one of the more obscure Latin.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid inner-color">
    <div class="container">
      <div class="section-title">
        <h2>Why Trusting Us</h2>
        <hr class="center">
        <p>leap into electronic typesetting, remaining essentially unchanged</p>
      </div>
      <div class="row">
        <div class="col-lg-4">
          <div class="front-thumbnail">
            <figure class="box-pic"><a href="#"><img src="img/images/img1.jpg" alt=""></a></figure>
            <div class="rounded-circle">
              <figure class="center-circle-icon"><img src="img/master/cost.png" alt=""></figure>
            </div>
            <div class="figure-caption">
              <h4>Financial Strategy</h4>
              <p>Eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 center-div">
          <div class="front-thumbnail">
            <figure class="box-pic"><a href="#"><img src="img/images/img3.jpg" alt=""></a></figure>
            <div class="rounded-circle">
              <figure class="center-circle-icon"><img src="img/master/deal.png" alt=""></figure>
            </div>
            <div class="figure-caption">
              <h4>Tax Consultant</h4>
              <p>Eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-4">
          <div class="front-thumbnail">
            <figure class="box-pic"><a href="#"><img src="img/images/img2.jpg" alt=""></a></figure>
            <div class="rounded-circle">
              <figure class="center-circle-icon"><img src="img/master/document.png" alt=""></figure>
            </div>
            <div class="figure-caption">
              <h4>Retirement Advisor</h4>
              <p>Eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
 
</section>
<?php include 'footer.php'; ?>