<!DOCTYPE html>
<html lang="en-US" class="no-js">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- ==============================================
      TITLE AND META TAGS
      =============================================== -->
    <title>CCS Savings and Loan | Home</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Quickdev">
    <!-- ==============================================
      FAVICON
      =============================================== -->  
    <link rel="shortcut icon" href="img/master/logo.png">
    <!-- ==============================================
      CSS
      =============================================== -->  
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/navbar.css">
    <link rel="stylesheet" href="css/stylesheet.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <script src="js/modernizr-custom.php"></script>
  </head>
  <body>
    <!-- LOADER -->
    <div id="loader-wrapper">
      <div id="loader"></div>
    </div>
    <!-- LOADER -->
    <div class="top-header">
      <div class="container">
        <div class="top-header-social">
          <div class="inner-header-social"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></div>
          <div class="inner-header-social"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></div>
          <div class="inner-header-social"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></div>
          <div class="inner-header-social"><a href="#"><i class="fa fa-rss" aria-hidden="true"></i></a></div>
        </div>
      </div>
    </div>
    <header>
      <div class="container">
        <div class="header-left">
          <figure class="brand"><a href="index.php"><img src="img/master/logo.png" style="width: 100px !important;" alt=""></a></figure>
        </div>
        <div class="top-location">
          <div class="address">
            <figure class="address-icon"><img src="img/master/address.png" alt=""></figure>
            <h6>Location:</h6>
            <p>P. Paredes, Sampaloc, Manila, Metro Manila</p>
          </div>
          <div class="email">
            <figure class="mail-icon"><img src="img/master/mail.png" alt=""></figure>
            <h6>Email Us:</h6>
            <p>jofbombasi@gmail.com <p>&nbsp;</p></p>
          </div>
          <div class="phone" style="margin-top: -50 !important;">
            <figure class="phone-icon"><img src="img/master/phone.png" alt=""></figure>
            <h6>Call Us:</h6>
            <p>+11-225-888-888-66 <p>&nbsp;</p></p>
          </div>
        </div
      </div>
    </header>
    <nav>
      <div class="container">
        <nav class="navbar navbar-expand-lg">
          <a class="navbar-brand" href="index.php">
            <div class="logo-brand"><img src="img/master/logo.png" style="width: 100px !important;" alt=""></div>
          </a>
          <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="icon-bar top-bar"></span>
          <span class="icon-bar middle-bar"></span>
          <span class="icon-bar bottom-bar"></span>       
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item active">
                <a class="nav-link" href="index.php">HOME</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="about.php">ABOUT</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="contact.php">CONTACT</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="faq.php">FAQ</a>
              </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
              <a class="btn btn-nav" href="login.php">Login / Register</a>
            </form>
          </div>
        </nav>
      </div>
    </nav>