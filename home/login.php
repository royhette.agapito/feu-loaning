<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CCS | Login</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/iofrm-style.css">
    <link rel="stylesheet" type="text/css" href="css/iofrm-theme3.css">
    <link rel="shortcut icon" href="img/master/logo.png">
  </head>
  <body style="overflow: hidden;">
    <div class="form-body">
      <div class="website-logo">
        <a>
          <div class="logo">
            <img class="logo-size" src="img/master/logo.png" alt="">
          </div>
        </a>
      </div>
      <div class="row">
        <div class="img-holder">
          <div class="bg"></div>
          <div class="info-holder">
          </div>
        </div>
        <div class="form-holder">
          <div class="form-content">
            <div class="form-items">
              <h3>Get more things done with us.</h3>
              <div class="page-links">
                <a href="login.php" class="active">Login</a><a href="register.php">Register</a>
              </div>
              <div class="alert alert-warning alert-dismissible fade show with-icon" role="alert">
                Please fill the following form with your information
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form>
                <input class="form-control" type="text" placeholder="Employee ID" required>
                <input class="form-control" type="password" name="password" placeholder="Password" required>
                <div class="form-button">
                  <button id="submit" type="submit" class="ibtn" onclick="window.location.href='index.php'">Login</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>