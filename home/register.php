<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CCS | Register</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/iofrm-style.css">
    <link rel="stylesheet" type="text/css" href="css/iofrm-theme3.css">
    <link rel="shortcut icon" href="img/master/logo.png">
  </head>
  <body style="overflow: hidden;">
    <div class="form-body">
      <div class="website-logo">
        <a href="register.php">
          <div class="logo">
            <img class="logo-size" src="img/master/logo.png" alt="">
          </div>
        </a>
      </div>
      <div class="row">
        <div class="img-holder">
          <div class="bg"></div>
          <div class="info-holder">
          </div>
        </div>
        <div class="form-holder">
          <div class="form-content">
            <div class="form-items">
              <h3>Get in touch</h3>
              <p>Register and get your loan now!</p>
              <div class="page-links">
                <a href="login.php">Login</a><a href="register.php" class="active">Register</a>
              </div>
              <form>
                <input class="form-control" type="text" placeholder="Empployee ID" required>
                <input class="form-control" type="email" name="email" placeholder="E-mail Address" required>
                <input class="form-control" type="text" placeholder="First Name" required>
                <input class="form-control" type="text" placeholder="Last Name" required>
                <input class="form-control" type="text" placeholder="Phone Number" required>
                <input class="form-control" type="password" placeholder="Password" required>
                <input class="form-control" type="password" placeholder="Confirm Password" required>
                <div class="form-button">
                  <button id="submit" type="submit" class="ibtn">Register</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>