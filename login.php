<?php
	if(!isset($_SESSION)) {
		session_start();
	}
	if(isset($_SESSION['userLog']['id'])) {
		if (isset($_SERVER["HTTP_REFERER"])) {
			header("Location: " . $_SERVER["HTTP_REFERER"]);
			 }
	}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>CCS Saving and Loaning | Login</title>
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="assets/plugins/iconic/css/material-design-iconic-font.min.css">
    <!-- bootstrap -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- style -->
    <link rel="stylesheet" href="assets/css/pages/extra_pages.css">
	<!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" /> 

</head>
<body>
    <div class="limiter">
		<div class="container-login100 page-background">
			<div class="wrap-login100">
				<!-- <form class="login100-form validate-form" action="controllers/handlers/users.php?login" method="POST"> -->
					<center>
						<img src="assets/img/logo.png">
					</center>
					<span class="login100-form-title p-b-34 p-t-27">
						Log in
					</span>
					<div class="wrap-input100 validate-input" data-validate = "Enter Employee Id">
						<input id="txtUser" class="input100" type="text" name="id" placeholder="Employee Id">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>
					<div class="wrap-input100 validate-input" data-validate="Enter Password">
						<input id="txtPass" class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" onclick="login();">
							Login
						</button>
					</div>
					<div class="text-center p-t-90">
						<a class="txt1" href="sign_up.php">
							Not a member yet?
						</a>
					</div>
				<!-- </form> -->
			</div>
		</div>
	</div>
    <!-- start js include path -->
    <script src="assets/plugins/jquery/jquery.min.js" ></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- bootstrap -->
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <script src="assets/js/pages/extra_pages/login.js" ></script>
    <!-- end js include path -->
</body>

</html>

<script>

$('#txtPass').keyup(function (e) {
		if(e.which === 13){
			login();
		}
	});


	function login() {
		$.ajax({
			url: 'controllers/handlers/users.php?login&role=members',
			type: "POST",
        	data: {id:$('#txtUser').val(),password:$('#txtPass').val()},
			dataType: 'json',
		}).done(function(response){
			if(response["message"] == "Success") {
				swal({
				  	title:response["message"],
				  	icon: "success",
				  	dangerMode: false,
					}).then((willDelete) => {
						window.location.href = response["role"]+"/dashboard.php";
				});
			}
			else if (response["message"] == "Pending Request") {
				swal("Info", response["message"], "info");
			} else {
				swal("Invalid", response["message"], "error");
			}
		});
	}
</script>