<?php

include("../classes/seasons.php");
$season = new seasons();

if(isset($_GET['insert'])) {
     echo $season->insert($_POST);
}

if(isset($_GET['activate'])) {
    echo $season->activate($_GET['id']);
}

if(isset($_GET['deactivate'])) {
    echo $season->deactivate($_GET['id']);
}

function goBack() {
    if (isset($_SERVER["HTTP_REFERER"])) {
   header("Location: " . $_SERVER["HTTP_REFERER"]);
    }
}

