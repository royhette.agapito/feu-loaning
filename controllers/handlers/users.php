<?php

include("../classes/users.php");
$user = new users();

if(isset($_GET['insert'])) {
     echo $user->insert($_POST,$_GET['role']);
}

if(isset($_GET['login'])) {
    echo $row = $user->login($_POST,$_GET['role']);
}

if(isset($_GET['activate'])) {
    echo $row = $user->activate($_GET['id']);
}

if(isset($_GET['deactivate'])) {
    echo $row = $user->deactivate($_GET['id']);
}

if(isset($_GET['update'])) {
    echo $row = $user->update($_GET['id'],$_POST,$_FILES);
    goBack();
}


if(isset($_GET['update2'])) {
    echo $row = $user->update2($_GET['id'],$_POST);
}

if(isset($_GET['update_from_member'])) {
    echo $row = $user->update($_GET['id'],$_POST,$_FILES);
    if($row == "Success!") {
        $user->update_my_self($_GET['id']);
    }
}




if(isset($_GET['logout'])) {
    $user->logout();
    goBack();
}

if(isset($_GET['update_pass'])) {
    echo $user->update_pass($_POST);
}


function goBack() {
    if (isset($_SERVER["HTTP_REFERER"])) {
   header("Location: " . $_SERVER["HTTP_REFERER"]);
    }
}

