<?php

include("../classes/announcement.php");
$announcement = new announcement();

if(isset($_GET['insert'])) {
     echo $announcement->insert($_POST,$_FILES);
}

if(isset($_GET['activate'])) {
    echo $announcement->activate($_GET['id']);
}
if(isset($_GET['update'])) {
    echo $announcement->update($_POST,$_GET['id'],$_FILES);
    goBack();
}

if(isset($_GET['deactivate'])) {
    echo $announcement->deactivate($_GET['id']);
}

function goBack() {
    if (isset($_SERVER["HTTP_REFERER"])) {
   header("Location: " . $_SERVER["HTTP_REFERER"]);
    }
}

