<?php

class announcement {
    public $conn;
    public $dateToday;


    public function __construct() {
         include("../../env.php");
         $env = new env();
         $this->conn = $env->connectDb();
         $this->dateToday = date('Y-m-d H:i:s');
    }


  


    public function update($data,$id,$image) {

        if(!isset($_SESSION)) {
            session_start();
        }
    
        include("upload.php");
        $obj = new upload();
        
        $res = $this->conn->query("SELECT image FROM tbl_announcement WHERE id = '".$id."' ");
        $row = $res->fetch_assoc();
        $imageName = $row['image'];
    
    
        if($image['image']['size'] > 0 ) {
           $imageName = $obj->transferImage($image["image"]);
        } 
        $this->generateTrails($_SESSION['userLog']['id'],"Update announcement id# ".$id);

        $this->conn->query("UPDATE tbl_announcement SET image = '".$imageName."' ,
         title = '".$data['title']."' , descriptions = '".$data['descriptions']."' , date_update = '".$this->dateToday."'
         WHERE id = '".$id."' ");
         return "Succes!";
    

    }

    public function insert($data,$image) {
        if(!isset($_SESSION)) {
            session_start();
        }

        include("upload.php");
        $obj = new upload();

		$imageName = $obj->transferImage($image["image"]);

        $msg = "";

        $res = $this->conn->query("SELECT * FROM tbl_users WHERE role = 'member' AND status = 1 ");
        while($row = $res->fetch_assoc()) {
            $this->conn->query("INSERT INTO `tbl_notification` ( `loan_id`, `user_id`,`co1` ,  `message`,
            `date_created`, `date_update` , `remarks`) 
            VALUES ( '1231','".$_SESSION['userLog']['id']."','".$row['id']."', '".'NEW ANNOUNCEMENT ('.$data['title'].')! Please check announcement page'."', 
            '".$this->dateToday."', '".$this->dateToday."' , 'read');");
        }

      


        $this->generateTrails($_SESSION['userLog']['id'],"Insert new announcement");
        $this->conn->query("INSERT INTO `tbl_announcement`
         (`image`, `title`, `descriptions`, `date_created`, `date_update`)
          VALUES ('".$imageName."', '".$data['title']."', '".$data['descriptions']."', 
          '".$this->dateToday."', '".$this->dateToday."');");
          $msg =  mysqli_error($this->conn);
          if($msg == "") {
              $msg = "Success!";
          }
          return $msg;
    }

    public function activate($id) {
        if(!isset($_SESSION)) {
            session_start();
        }
        $this->generateTrails($_SESSION['userLog']['id'],"Activate post id#".$id);
        $this->conn->query("UPDATE  tbl_announcement SET status = 1 WHERE id = '".$id."' ;");
        return "Success!";
    }

    public function deactivate($id) {
        if(!isset($_SESSION)) {
            session_start();
        }
        $this->generateTrails($_SESSION['userLog']['id'],"Deactivateivate post id#".$id);
        $this->conn->query("UPDATE tbl_announcement SET status = 0 WHERE id = '".$id."' ;");
        return "Success!";
    }

    public function generateTrails($id,$message) {
        if(!isset($_SESSION)) {
            session_start();
        }
        $id = $_SESSION['userLog']['id'];
        $this->conn->query("INSERT INTO `tbl_userlogs` ( `user_id`, `message` ,`date_created`)
                            VALUES ('".$id."', '".$message."', '".$this->dateToday."');");
      }



}