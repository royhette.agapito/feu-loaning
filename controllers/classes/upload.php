<?php
class upload {
	function transferImage($image) {
		$myRand = substr(md5(rand()), 0, 100);
		$ext = explode(".", basename($image["name"]));
		$imageName = (!empty($image['name']))?$myRand.".".$ext[count($ext)-1]:"";

		if(!empty($imageName) ) {
			$target_dir = "../../assets/uploads/";
			$target_file = $target_dir .$imageName;
			$uploadOk = 1;
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {
			    // echo "Sorry, your file was not uploaded.";
				// if everything is ok, try to upload file
			} 
			else {
			    if (move_uploaded_file($image["tmp_name"], $target_file)) {
		        	// echo "The file ". basename( $image["name"]). " has been uploaded.";
			    } 
			    else {
		        	// echo "Sorry, there was an error uploading your file.";
			    }
			}
		}
		return $imageName;
	}
}
