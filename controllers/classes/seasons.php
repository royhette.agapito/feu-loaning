<?php

class seasons {
    public $conn;
    public $dateToday;


    public function __construct() {
         include("../../env.php");
         $env = new env();
         $this->conn = $env->connectDb();
         $this->dateToday = date('Y-m-d H:i:s');
    }

    public function insert($data) {
        $idd =  $data['id'];
        if(!isset($_SESSION)) {
            session_start();
        }
        $msg = "";
        $res = $this->conn->query("SELECT * FROM tbl_seasons WHERE id = '".$idd."' ");
      
        if($res->num_rows > 0 ) {
         $msg = "Id already used!";
        }else {

         
        $this->generateTrails($_SESSION['userLog']['id'],"Insert Season ".$idd);
        $this->conn->query("INSERT INTO `tbl_seasons`
         (`id`, `start_date`, `end_date`, `first_payment`, `second_payment`,
          `amount_per_share`, `penalty`, `interest`, `date_created`, `date_update`)
          VALUES ('".$idd."', '".$data['start_date']."', '".$data['end_date']."', '".$data['first_payment']."', '".$data['second_payment']."',
          '".$data['amount_per_share']."', '".$data['penalty']."' , '".$data['interest']."' , 
          '".$this->dateToday."', '".$this->dateToday."');");
          $msg =  mysqli_error($this->conn);
        
        
            require '../../PHPMailer-master/PHPMailerAutoload.php';

          $res2 = $this->conn->query("SELECT * FROM tbl_users WHERE role = 'member' ");
          while($row2 = $res2->fetch_assoc()) {


            $email = $row2['email'];
            $name = $row2['first_name'].' '.$row2['sur_name'];

            
            $this->conn->query("INSERT INTO `tbl_notification` ( `loan_id`, `user_id`,`co1` ,  `message`,
            `date_created`, `date_update` , `remarks`) 
            VALUES ( '1231','".$_SESSION['userLog']['id']."','".$row2['id']."', '".'HI'.$name.'New season is available! season number'.$data['id']."', '".$this->dateToday."', '".$this->dateToday."' , 'read');");





            $body =  "<!DOCTYPE html>
            <html>
            <head>
              <title>Notification</title>
        
              <meta charset='UTF-8'>
                <meta http-equiv='X-UA-Compatible' content='IE=edge'>
                <meta name='viewport' content='width=device-width, initial-scale=1'>
            </head>
            <body style='padding: 0px; margin: 0px; font-weight: 600; font-family: sans-serif;' background-color: #f0f0f0;>
        
              <div style='width: 100%; background-color: #FB6C17; color: white; padding: 30px 0px'>
                <center><h1>CCS LOANING</h1></center>
              </div>
        
              <div style='width: 100%'>
                <center>
                  <br><br>
                  <h2 style='color: rgba(0, 0, 0, 0.7); font-size: 34px;'>Hi ".$name." New season is available! season #".$idd."!,<h2><br>
                  <br><br>
                      <span style='color: rgba(0, 0, 0, 0.8);'>This email meant to ".$name."</span><br><br>
                </center>
              </div>
        
            </body>
            </html>";
        
          
            $mailto = $email;
            $mailSub = "NOTIFICATION";
            $mailMsg = $body;
            $mail = new PHPMailer();
            $mail ->IsSmtp();
            $mail ->SMTPDebug = 0;
            $mail ->SMTPAuth = true;
            $mail ->SMTPSecure = 'ssl';
            $mail ->Host = "smtp.gmail.com";
            $mail ->Port = 465; // or 587
            $mail ->IsHTML(true);
            $mail ->Username = "ccsloaning@gmail.com";
            $mail ->Password = "ccsloaning2k19";
            $mail ->SetFrom("ccsloaning@gmail.com");
            $mail ->Subject = $mailSub;
            $mail ->Body = $mailMsg;
            $mail ->AddAddress($mailto);


            if(!$mail->Send()) {
            }
            else{
                if($msg == "") {
                  $msg = "Success!";
              }
            }
        
         

          }
   

    

    }
    echo $msg;

}

    public function activate($id) {
        if(!isset($_SESSION)) {
            session_start();
        }
        $this->generateTrails($_SESSION['userLog']['id'],"Activate Season ".$id);
        $this->conn->query("UPDATE  tbl_seasons SET status = 1 WHERE id = '".$id."' ;");
        $this->conn->query("UPDATE tbl_join_season SET status = 1 WHERE season_id = '".$id."' ;");
        return "Success!";
    }

    public function deactivate($id) {
        if(!isset($_SESSION)) {
            session_start();
        }
        $this->generateTrails($_SESSION['userLog']['id'],"ActDeactivateivate Season ".$id);
        $res = $this->conn->query("SELECT * FROM `tbl_join_season` WHERE season_id = '".$id."' AND status = 1;");
        while($row = $res->fetch_assoc()) { 
          $this->query("INSERT INTO `tbl_notification` ( `loan_id`, `user_id`,`co1` ,  `message`,
            `date_created`, `date_update` , `remarks`) 
            VALUES ( '".$row['season_id']."','".$_SESSION['userLog']['id']."','".$row['user_id']."', '"."Season NO ".$row['season_id']." has been deactivated!"."', '".$this->dateToday."', '".$this->dateToday."' , 'read');");
        }
        $this->conn->query("UPDATE tbl_seasons SET status = 0 WHERE id = '".$id."' ;");
        $this->conn->query("UPDATE tbl_join_season SET status = 0 WHERE season_id = '".$id."' ;");
        return "Success!";
    }

    public function generateTrails($id,$message) {
        if(!isset($_SESSION)) {
            session_start();
        }
        $id = $_SESSION['userLog']['id'];
        $this->conn->query("INSERT INTO `tbl_userlogs` ( `user_id`, `message` ,`date_created`)
                            VALUES ('".$id."', '".$message."', '".$this->dateToday."');");
      }



}