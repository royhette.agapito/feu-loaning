<?php

class users {
    public $conn;
    public $dateToday;
    // public $generate;


    public function __construct() {
         include("../../env.php");
         $env = new env();
        //  include("generate.php");
        //  $this->generate = new generate();
         $this->conn = $env->connectDb();
         $this->dateToday = date('Y-m-d H:i:s');
    }

    public function insert($data,$role) {
        $msg = "";
        $res = $this->conn->query("SELECT * FROM tbl_users WHERE id = '".$data['id']."' ");
        if($res->num_rows > 0 ) {
         $msg = "Id already used!";
        }else {
            if($data['first_name'] == null || $data['first_name'] == ' ' || 2 > strlen($data['first_name']) ){
                $msg = "Fname";
            }else if($data['middle_name'] == null || $data['middle_name'] == ' ' || 2 > strlen($data['middle_name'])){
                $msg = "Mname";
            }else if($data['sur_name'] == null || $data['sur_name'] == ' ' || 2 > strlen($data['sur_name'])){
                $msg = "Lname";
             }else if($data['mobile_number'] == null || $data['mobile_number'] == ' ' || 11 != strlen($data['mobile_number'])){
                    $msg = "Num";
            }else if($data['email'] == null || $data['email'] == ' ' || 2 > strlen($data['email'])){
                $msg = "Email";
            }else if($data['id'] == null || $data['id'] == ' ' || 2 > strlen($data['id'])){
                $msg = "Id";
            }else if($data['password'] == null || $data['password'] == ' ' || 2 > strlen($data['password'])){
                $msg = "Pass1";
            }else if($data['pass2'] == null || $data['pass2'] == ' ' || 2 > strlen($data['pass2'])){
                $msg = "Pass2";
            }else if($data['password'] != $data['pass2']){
                $msg = "Not";
            }else if($data['password'] == $data['pass2']){




        $res1 = $this->conn->query("SELECT * FROM tbl_users WHERE mobile_number = '".$data['mobile_number']."' AND status = 1 ");
        $res2 = $this->conn->query("SELECT * FROM tbl_users WHERE email = '".$data['email']."'  AND status = 1  "); 
        $res3 = $this->conn->query("SELECT * FROM tbl_users WHERE first_name = '".$data['first_name']."' AND sur_name = '".$data['sur_name']."'   AND status = 1  ");

        if($res1->num_rows > 0  || $res2->num_rows > 0 || $res3->num_rows > 0) {
            if($res1->num_rows > 0) {
                $msg =  "Duplicated mobile number";
          return $msg;

            } else if($res2->num_rows > 0) {
                $msg =  "Duplicated email";
          return $msg;


            } else if($res3->num_rows > 0) {
                $msg = "Duplicated firstname and surname";
          return $msg;


            }

        } else {



// start here




$this->conn->query("INSERT INTO `tbl_userlogs` ( `user_id`, `message` ,`date_created`)
VALUES ('".$data['id']."', 'New registered user', '".$this->dateToday."');");
  
// $this->generateTrails($data['id'],"New registered user ");

    require '../../PHPMailer-master/PHPMailerAutoload.php';

  $res123 = $this->conn->query("SELECT * FROM tbl_users WHERE role = 'admin' ; ");
  while($row123 = $res123->fetch_assoc()) {


    $email = $row123['email'];
    $name = $row123['first_name'].' '.$row123['sur_name'];


    $body =  "<!DOCTYPE html>
    <html>
    <head>
      <title>Notification</title>

      <meta charset='UTF-8'>
        <meta http-equiv='X-UA-Compatible' content='IE=edge'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
    </head>
    <body style='padding: 0px; margin: 0px; font-weight: 600; font-family: sans-serif;' background-color: #f0f0f0;>

      <div style='width: 100%; background-color: #FB6C17; color: white; padding: 30px 0px'>
        <center><h1>CCS LOANING</h1></center>
      </div>

      <div style='width: 100%'>
        <center>
          <br><br>
          <h2 style='color: rgba(0, 0, 0, 0.7); font-size: 34px;'>Hi ".$name." New registered user with the ID of ".$data['id']."!,<h2><br>
          <br><br>
              <span style='color: rgba(0, 0, 0, 0.8);'>This email meant to ".$name."</span><br><br>
        </center>
      </div>

    </body>
    </html>";

  
    $mailto = $email;
    $mailSub = "NOTIFICATION";
    $mailMsg = $body;
    $mail = new PHPMailer();
    $mail ->IsSmtp();
    $mail ->SMTPDebug = 0;
    $mail ->SMTPAuth = true;
    $mail ->SMTPSecure = 'ssl';
    $mail ->Host = "smtp.gmail.com";
    $mail ->Port = 465; // or 587
    $mail ->IsHTML(true);
    $mail ->Username = "ccsloaning@gmail.com";
    $mail ->Password = "ccsloaning2k19";
    $mail ->SetFrom("ccsloaning@gmail.com");
    $mail ->Subject = $mailSub;
    $mail ->Body = $mailMsg;
    $mail ->AddAddress($mailto);

    if(!$mail->Send()) {
        $msg = "Error";
    }
    else{
          $msg = "Success!";
    }


  }


// ends here


                $shares = (!isset($data['shares']))?0:$data['shares'];
                $this->conn->query("INSERT INTO `tbl_users`
                (`id`, `first_name`, `middle_name`, `sur_name`, `email`, `password`, `mobile_number`, `shares`, `role` ,
                `date_created`, `date_update`)
                VALUES ('".$data['id']."', '".$data['first_name']."', '".$data['middle_name']."', '".$data['sur_name']."', '".$data['email']."',
                '".$data['password']."', '".$data['mobile_number']."', '".$shares."' , '".$role."' , '".$this->dateToday."', '".$this->dateToday."');");
               // $msg =  mysqli_error($this->conn);
                // if($msg == "") {
                //     $msg = "Success!";
                // }
            }

         
        }

        


        


        }
          return $msg;
    }

    public function activate($id) {
        if(!isset($_SESSION)) {
            session_start();
        }
        $this->generateTrails($id,"Activated Employee ".$id);
        $this->conn->query("UPDATE tbl_users SET status = 1 WHERE id = '".$id."' ;");
        return "Success!";
    }

    public function deactivate($id) {
        if(!isset($_SESSION)) {
            session_start();
        }
        $this->generateTrails($id,"Deactivated Employee ".$id);
        $this->conn->query("UPDATE tbl_users SET status = 0 WHERE id = '".$id."' ;");
        return "Success!";
    }

    public function logout() {
        if(!isset($_SESSION)) {
            session_start();
        }
        session_destroy();
    }

    public function update_pass($data) {
        if(!isset($_SESSION)) {
            session_start();
        }
        $this->conn->query("UPDATE tbl_users SET password = '".$data['newpassword']."' WHERE id = '".$_SESSION['userLog']['id']."' ;");
        $_SESSION['userLog']['password'] = $data['newpassword'];
        return "Success!";
    }
    public function update($id,$data,$image) {
        if(!isset($_SESSION)) {
            session_start();
        }

        include("upload.php");
        $obj = new upload();
        
        $res = $this->conn->query("SELECT image FROM tbl_users WHERE id = '".$id."' ");
        $row = $res->fetch_assoc();
        $imageName = $row['image'];


        if($image['image']['size'] > 0 ) {
           $imageName = $obj->transferImage($image["image"]);
        }
        
        $res1 = $this->conn->query("SELECT * FROM tbl_users WHERE mobile_number = '".$data['mobile_number']."'  AND id <> '".$id."'  AND status = 1 ");
        $res2 = $this->conn->query("SELECT * FROM tbl_users WHERE email = '".$data['email']."'  AND id <> '".$id."'  AND status = 1 "); 
        $res3 = $this->conn->query("SELECT * FROM tbl_users WHERE first_name = '".$data['first_name']."' AND sur_name = '".$data['sur_name']."'  AND id <> '".$id."'  AND status = 1  ");

        if($res1->num_rows > 0  || $res2->num_rows > 0 || $res3->num_rows > 0) {
            if($res1->num_rows > 0) {
                return "Duplicated mobile number";
            } else if($res2->num_rows > 0) {
                return "Duplicated email";
            } else if($res3->num_rows > 0) {
                return "Duplicated firstname and surname";
            }
        } else {
            $this->generateTrails($id,"Update Employee ".$id);
            $this->conn->query("UPDATE tbl_users SET first_name = '".$data['first_name']."' , sur_name = '".$data['sur_name']."' , email = '".$data['email']."'  , mobile_number = '".$data['mobile_number']."' , image = '".$imageName."' WHERE id = '".$id."' ;");
            return "Success!";
        }

        

     
    }

    public function update2($id,$data) {
        if(!isset($_SESSION)) {
            session_start();
        }

       
        
        $res1 = $this->conn->query("SELECT * FROM tbl_users WHERE mobile_number = '".$data['mobile_number']."' AND id <> '".$id."'  AND status = 1 ");
        $res2 = $this->conn->query("SELECT * FROM tbl_users WHERE email = '".$data['email']."'  AND id <> '".$id."'  AND status = 1 ");
        $res3 = $this->conn->query("SELECT * FROM tbl_users WHERE first_name = '".$data['first_name']."' AND sur_name = '".$data['sur_name']."'  AND id <> '".$id."'  AND status = 1  ");

        if($res1->num_rows > 0  || $res2->num_rows > 0 || $res3->num_rows > 0) {
            if($res1->num_rows > 0) {
                return "Duplicated mobile number";
            } else if($res2->num_rows > 0) {
                return "Duplicated email";
            } else if($res3->num_rows > 0) {
                return "Duplicated firstname and surname";
            }
        } else {
            $this->generateTrails($id,"Update Employee ".$id);
            $this->conn->query("UPDATE tbl_users SET first_name = '".$data['first_name']."' , sur_name = '".$data['sur_name']."' , email = '".$data['email']."'  , mobile_number = '".$data['mobile_number']."' WHERE id = '".$id."' ;");
            return "Success!";
        }

        

     
    }



    public function generateTrails($id,$message) {
        if(!isset($_SESSION)) {
            session_start();
        }
        $id = $_SESSION['userLog']['id'];
        $this->conn->query("INSERT INTO `tbl_userlogs` ( `user_id`, `message` ,`date_created`)
                            VALUES ('".$id."', '".$message."', '".$this->dateToday."');");
      }

      public function update_my_self($id) {

        $res = $this->conn->query("SELECT * FROM tbl_users WHERE id = '".$id."' ;");
        $row = $res->fetch_assoc();
        if(!isset($_SESSION)) {
                    session_start();
                }
               $this->generateTrails($row['id'],"Update profile");
               $_SESSION['userLog'] = $row;

      }


    public function login($data,$role) {
        if($role == "members") {
            $res = $this->conn->query("SELECT * FROM tbl_users WHERE id = '".$data['id']."' AND password = '".$data['password']."' AND role = 'member' ;");
        } else {
            $res = $this->conn->query("SELECT * FROM tbl_users WHERE id = '".$data['id']."' AND password = '".$data['password']."' AND role <> 'member' ;");
        }
        $row = $res->fetch_assoc();
        $resArr = [];
        $msg = "";
        if($res->num_rows > 0) {
            if($row['status'] == 1) {
                if(!isset($_SESSION)) {
                    session_start();
                }
               $this->generateTrails($row['id'],"Login");
               $_SESSION['userLog'] = $row;
               $msg = "Success";
            } else {
                $msg =  "Pending Request";
            }
         
        } else {
            $msg =  "Invalid Username/Password";
        }
        return json_encode(array("message"=>$msg,"role"=>$row['role']));
    }


}