
<?php include 'header.php'; ?>

<?php

$res = $conn->query("SELECT season_id FROM tbl_join_season WHERE user_id = '".$_SESSION['userLog']['id']."' AND status = 1 ");
$row = $res->fetch_assoc();
$seasonId = $row['season_id'];
$first;
$second;
$season;
if($res->num_rows > 0) {
  $res2 = $conn->query("SELECT * FROM tbl_seasons WHERE id = '".$seasonId."' ");
  $row2 = $res2->fetch_assoc();
  $first = $row2["first_payment"];
  $second = $row2["second_payment"];
  $season = $row2['id'];
  $season_start = explode("-",$row2["start_date"]);
  $season_end = explode("-",$row2["end_date"]);
  $month_first =  explode("-",$row2["start_date"])[1];
  $month_second =  explode("-",$row2["end_date"])[1];

  $diff = $month_second - $month_first;


}
?>
<link rel="stylesheet" type="text/css" href="../assets/css/calendar.css">
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<script>

  $(document).ready(function() {
    
      var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    
    /*  className colors
    
    className: default(transparent), important(red), chill(pink), success(green), info(blue)
    
    */    
    
      
    /* initialize the external events
    -----------------------------------------------------------------*/
  
    $('#external-events div.external-event').each(function() {
    
      // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
      // it doesn't need to have a start or end
      var eventObject = {
        title: $.trim($(this).text()) // use the element's text as the event title
      };
      
      // store the Event Object in the DOM element so we can get to it later
      $(this).data('eventObject', eventObject);
      
      // make the event draggable using jQuery UI
      $(this).draggable({
        zIndex: 999,
        revert: true,      // will cause the event to go back to its
        revertDuration: 0  //  original position after the drag
      });
      
    });
  
  
    /* initialize the calendar
    -----------------------------------------------------------------*/
    
    var calendar =  $('#calendar').fullCalendar({
      header: {
        left: 'title',
        center: 'agendaDay,agendaWeek,month',
        right: 'prev,next today'
      },
      editable: false,
      firstDay: 1, //  1(Monday) this can be changed to 0(Sunday) for the USA system
      selectable: false,
      defaultView: 'month',
      
      axisFormat: 'h:mm',
      columnFormat: {
                month: 'ddd',    // Mon
                week: 'ddd d', // Mon 7
                day: 'dddd M/d',  // Monday 9/7
                agendaDay: 'dddd d'
            },
            titleFormat: {
                month: 'MMMM yyyy', // September 2009
                week: "MMMM yyyy", // September 2009
                day: 'MMMM yyyy'                  // Tuesday, Sep 8, 2009
            },
      allDaySlot: false,
      selectHelper: true,
      select: function(start, end, allDay) {
        var title = prompt('Event Title:');
        if (title) {
          calendar.fullCalendar('renderEvent',
            {
              title: title,
              start: start,
              end: end,
              allDay: allDay
            },
            true // make the event "stick"
          );
        }
        calendar.fullCalendar('unselect');
      },
      droppable: false, // this allows things to be dropped onto the calendar !!!
      drop: function(date, allDay) { // this function is called when something is dropped
      
        // retrieve the dropped element's stored Event Object
        var originalEventObject = $(this).data('eventObject');
        
        // we need to copy it, so that multiple events don't have a reference to the same object
        var copiedEventObject = $.extend({}, originalEventObject);
        
        // assign it the date that was reported
        copiedEventObject.start = date;
        copiedEventObject.allDay = allDay;
        
        // render the event on the calendar
        // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
        $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
        
        // is the "remove after drop" checkbox checked?
        if ($('#drop-remove').is(':checked')) {
          // if so, remove the element from the "Draggable Events" list
          $(this).remove();
        }
        
      },
      
      events: [
        {
          title: 'Start Of Season',
        start: new Date(y,<?=$season_start[1]-1?> , <?=$season_start[2]?>)
        },
        {
          title: 'End Of Season',
        start: new Date(y,<?=$season_end[1]-1?> , <?=$season_end[2]?>)
        },

        <?php 
        for($i = $month_first ; $i <= $month_second; $i ++) { 
        ?>
        {
          <?php if($i == $month_first ) {
            if($first >  $season_start[2] ) {   ?>
            title: 'First Payment',
            start: new Date(y,<?=(integer)$i-1?> , <?=(integer)$first?>)
        <?php }  } else if($i == $month_second ) {
            if($first <  $season_end[2] ) {   ?>
            title: 'First Payment',
            start: new Date(y,<?=(integer)$i-1?> , <?=(integer)$first?>)
        <?php }  } 
        else {  ?>
          title: 'First Payment',
            start: new Date(y,<?=(integer)$i-1?> , <?=(integer)$first?>)
        <?php } ?>
        }
        ,

        {
          <?php if($i == $month_first ) {
            if($second >  $season_end[2] ) {   ?>
            title: 'Second Payment',
            start: new Date(y,<?=(integer)$i-1?> , <?=(integer)$second?>)
            <?php }  } else if($i == $month_second ) {
            if($second <  $season_end[2] ) {   ?>
            title: 'Second Payment',
            start: new Date(y,<?=(integer)$i-1?> , <?=(integer)$second?>)
        <?php }  } 
         else {  ?>
          title: 'Second Payment',
            start: new Date(y,<?=(integer)$i-1?> , <?=(integer)$second?>)
        <?php } ?>
        },


      <?php } ?>
        // {
        //   id: 999,
        //   title: 'Repeating Event',
        //   start: new Date(y, m, d-3, 16, 0),
        //   allDay: false,
        //   className: 'info'
        // },
        // {
        //   id: 999,
        //   title: 'Repeating Event',
        //   start: new Date(y, m, d+4, 16, 0),
        //   allDay: false,
        //   className: 'info'
        // },
        // {
        //   title: 'Meeting',
        //   start: new Date(y, m, d, 10, 30),
        //   allDay: false,
        //   className: 'important'
        // },
        // {
        //   title: 'Lunch',
        //   start: new Date(y, m, d, 12, 0),
        //   end: new Date(y, m, d, 14, 0),
        //   allDay: false,
        //   className: 'important'
        // },
        // {
        //   title: 'Birthday Party',
        //   start: new Date(y, m, d+1, 19, 0),
        //   end: new Date(y, m, d+1, 22, 30),
        //   allDay: false,
        // },
        // {
        //   title: 'Click for Google',
        //   start: new Date(y, m, 28),
        //   end: new Date(y, m, 29),
        //   url: 'https://ccp.cloudaccess.net/aff.php?aff=5188',
        //   className: 'success'
        // }
      ],      
    });
    
    



    function showpanel() {     
      $('.fc-button-agendaWeek').click();
      $('.fc-button-agendaWeek').css("display","none");
      $('.fc-button-agendaDay').css("display","none");
    $('.fc-button-month').click();
    $('.fc-button-month').css("display","none");
    $('.fc-button-today').css("display","none");
 }

 // use setTimeout() to execute
 setTimeout(showpanel, 1000)





  });

</script>
<style>

    
  #external-events {
    float: left;
    width: 150px;
    padding: 0 10px;
    text-align: left;
    }
    
  #external-events h4 {
    font-size: 16px;
    margin-top: 0;
    padding-top: 1em;
    }
    
  .external-event { /* try to mimick the look of a real event */
    margin: 10px 0;
    padding: 2px 4px;
    background: #3366CC;
    color: #fff;
    font-size: .85em;
    cursor: pointer;
    }
    
  #external-events p {
    margin: 1.5em 0;
    font-size: 11px;
    color: #666;
    }
    
  #external-events p input {
    margin: 0;
    vertical-align: middle;
    }

  #calendar {
/*    float: right; */
        margin: 0 auto;
    background-color: #FFFFFF;
      border-radius: 6px;
        box-shadow: 0 1px 2px #C3C3C3;
    -webkit-box-shadow: 0px 0px 21px 2px rgba(0,0,0,0.18);
-moz-box-shadow: 0px 0px 21px 2px rgba(0,0,0,0.18);
box-shadow: 0px 0px 21px 2px rgba(0,0,0,0.18);
    }

</style>


    <!-- start page content -->
    <div class="page-content-wrapper">
      <div class="page-content">
        <div class="page-bar">
          <div class="page-title-breadcrumb">
            <div class=" pull-left">
              <div class="page-title">Calendar Season</div>
            </div>
            <ol class="breadcrumb page-breadcrumb pull-right">
              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
              </li>
              <li class="active">Calendar</li>
            </ol>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div class="card-box">
              <div class="card-head">
                <header>Season # <?=$season?></header>
              </div>
              <div class="card-body ">
                <div class="panel-body">
                  <div id="calendar"> </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- end page content -->
  </div>
  <!-- end page container -->
<?php include 'footer.php'; ?>

<script src="../assets/js/calendar.js"></script>

<script type="text/javascript">
  $("#mycalendar").addClass("active");
</script>