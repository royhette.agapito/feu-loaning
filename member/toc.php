
<?php include 'header.php'; ?>

    <!-- start page content -->
    <div class="page-content-wrapper">
      <div class="page-content">
        <div class="page-bar">
          <div class="page-title-breadcrumb">
            <div class=" pull-left">
              <div class="page-title">Terms And Conditions</div>
            </div>
            <ol class="breadcrumb page-breadcrumb pull-right">
              <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
              </li>
              <li class="active">Terms And Conditions</li>
            </ol>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <div class="card-box">
              <div class="card-head">
                <header>Must Read!</header>
              </div>
              <div class="card-body ">
                <div class="panel-body">
                  <br>
                  1. Members shall give semi-monthly contribution depending on the number of shares from June 15, 2017 to November 30, 2017
                  <br><br>2. One share is equivalent to 100 pesos
                  <br><br>3. Maximum loanable amount is No. of shares x 2000 for members.
                  <br><br>4. Non members will share the loanable amount of their co-maker who must be a member.
                  <br><br>5. Loan period is from June 15, 2017 to November 15, 2017 only.
                  <br><br>6. In the event that the member decides to withdraw his shares from the CCS Paluwagan before November 15, 2017, he will only receive his principal
                  contributions without interest and provided he is not a co-maker. If he is a co-maker, his contributions will not be released until the lendee has fully
                  settled his loan.
                  <br><br>7. When the loanable amount has not been paid until November 15, 2017, the contributions of both the co-maker and the lendee will not be released.
                  <br><br>8. Each loan is payable from 1 to 3 months at 2% per month.
                  <br><br>9. Members can have a series of loans as long as the maximum loanable amount is not yet reached (including the loans of non-members you co-make).
                  <br><br>10. The amount paid for loans will automatically be added to the maximum loanable amount.
                  <br><br>11. Penalty for unpaid contribution shall apply at 1% every week starting from every 15 th and 30 th of the month.
                  <br><br>12. If the lendee avails of the loan 3 days before payday, the first payment will be on the payday after the current.
                  13. A patronage fee of 1% of loan amount will be charged to non-members of CCS Paluwagan and will be given to co-maker.
                  <div class="row mt-5">
                    <div class="col">
                      <a href="dashboard.php" class="btn btn-round btn-info">Got it!</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- end page content -->
  </div>
  <!-- end page container -->
<?php include 'footer.php'; ?>

<script src="../assets/js/calendar.js"></script>

<script type="text/javascript">
  $("#dashboard").addClass("active");
</script>