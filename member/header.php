<?php
if(!isset($_SESSION)) {
  session_start();
}

if(empty($_SESSION['userLog']['id'])) {
  header("location:../login.php");
}


 $img = (empty($_SESSION["userLog"]["image"]))?"../assets/img/empty.png":"../assets/uploads/".$_SESSION["userLog"]["image"];



include("../env.php");
$env = new env();
$conn = $env->connectDb();



$date_now = date("Y-m-d"); 

$check_season = $conn->query("SELECT * FROM tbl_seasons WHERE status = 1 ");
while($row_season = $check_season->fetch_assoc()) {
  if($date_now > $row_season['end_date']) {
    $conn->query("UPDATE tbl_seasons SET status = 0  WHERE id = '".$row_season['id']."' ; ");
  }
}



?>
<!DOCTYPE html>
<html lang="en">
  <!-- BEGIN HEAD -->
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="Responsive Admin Template" />
    <meta name="author" content="SmartUniversity" />
    <title>CCS | Saving and Loaning</title>
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
    <!-- icons -->
    <link href="../assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <!--bootstrap -->
    <!-- dropzone -->
    <link href="../assets/plugins/dropzone/dropzone.css" rel="stylesheet" media="screen">
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- <link rel="stylesheet" type="text/css" href="../assets/css/pages/formlayout.css"> -->
    <!-- morris chart -->
    <link href="../assets/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- full calendar -->
    <!-- <link href="../assets/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" /> -->
    <!-- data tables -->
    <link href="../assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="../assets/css/sweetalert.min.css">
    <!-- Material Design Lite CSS -->
    <link rel="stylesheet" href="../assets/plugins/material/material.min.css">
    <link rel="stylesheet" href="../assets/css/material_style.css">
    <!-- animation -->
    <link href="../assets/css/pages/animate_page.css" rel="stylesheet">
    <!-- Template Styles -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/theme-color.css" rel="stylesheet" type="text/css" />

    <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo.png" />
  </head>
  <!-- END HEAD -->
  <body ng-app="myApp" ng-cloak class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">
    <div class="page-wrapper">
      <!-- start header -->
      <div ng-controller="headerCtrl" ng-cloak>
      <div class="page-header navbar navbar-fixed-top" >
      
        <div class="page-header-inner ">
          <!-- logo start -->
          <div class="page-logo">
            <a href="dashboard.php">
            <img alt="" src="../assets/img/logo.png" width="40px">
            <span class="logo-default" >CCS </span> </a>
          </div>
          <!-- logo end -->
          <ul class="nav navbar-nav navbar-left in">
            <li><a href="#" class="menu-toggler sidebar-toggler"><i class="icon-menu"></i></a></li>
          </ul>
          <!-- start mobile menu -->
          <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
          <span></span>
          </a>
          <!-- end mobile menu -->
          <!-- start header menu -->


          


          <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
              <!-- start notification dropdown -->
              <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <i class="fa fa-bell-o"></i>
                <span class="badge headerBadgeColor1"> {{AllNotificationData.length}} </span>
                </a>
                <ul class="dropdown-menu animated swing">
                  <li class="external">
                    <h3><span class="bold">Notifications</span></h3>
                  </li>
                  <li ng-show="AllNotificationData1.length!=0">
                    <ul class="dropdown-menu-list small-slimscroll-style" data-handle-color="#637283">
                      <li ng-repeat="data in AllNotificationData1">
                        <a href="javascript:;" style="background-color:#d8d7d7" ng-if="data.remarks==null" data-toggle="modal" data-target="#applyform" ng-click="getDataLoan(data.id,data.loan_id,data.user_id)">
                          <span class="time">{{data.date_created}}</span>
                          <span class="details"data-toggle="modal">
                            <span class="notification-icon circle deepPink-bgcolor" data-toggle="modal" data-target="#applyform"><i class="fa fa-thumbs-up"></i></span> 
                            <span class="notification-icon circle deepPink-bgcolor" data-toggle="modal" data-target="#applyform"><i class="fa fa-thumbs-down"></i></span>
                            {{data.message}} 
                          </span>
                        </a>
                        <a href="javascript:;" ng-if="data.remarks1 == 0 && data.remarks!=null" ng-click="updateRemarksToRead(data.id)">
                          <span class="time">{{data.date_created}}</span>
                          <span class="details">
                            {{data.message}} 
                          </span>
                        </a>
                        <a href="javascript:;" style="background-color:#d8d7d7" ng-if="data.remarks1 == 1 && data.remarks!=null" ng-click="updateRemarksToRead(data.id)">
                          <span class="time">{{data.date_created}}</span>
                          <span class="details">
                            {{data.message}} 
                          </span>
                        </a>
                      </li>
                    </ul>
                    <div class="dropdown-menu-footer">
                      <a href="javascript:void(0)"> All notifications </a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- end notification dropdown -->
              <!-- start manage user dropdown -->
              <li class="dropdown dropdown-user">
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <img alt="" style="width: 30px; height: 30px" class="img-circle " src='<?=$img?>'/>
                <span class="username username-hide-on-mobile"> <?=$_SESSION['userLog']['first_name']?> </span>
                <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-default animated jello">
                  <li>
                    <a href="profile.php">
                    <i class="icon-user" id="profile"></i> Profile </a>
                  </li>
                  <li>
                    <a href="../controllers/handlers/users.php?logout">
                    <i class="icon-logout"></i> Log Out </a>
                  </li>
                </ul>
              </li>
              <!-- end manage user dropdown -->
            </ul>
          </div>
        </div>
      </div>
      <!-- Modal  Apply Loan -->
          <div class="modal fade" id="applyform" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">

            <div class="modal-dialog" role="document">
              <div class="modal-content card-box">
                <div class="modal-body">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="card-head">
                        <header>Loan Information</header>
                      </div>
                      <div class="card-body">
                        <div class="row">
                          <div class="col-md-6 p-t-20">
                            <div class="form-group">
                              <label>Date Loan</label>
                              <input type="text"  class="form-control" ng-model="loanDetails.date_created" disabled>
                            </div>
                          </div>
                          <div class="col-md-6 p-t-20">
                            <div class="form-group">
                              <label>Id</label>
                              <input type="text"  class="form-control" ng-model="loanDetails.user_id" disabled>
                            </div>
                          </div>
                          <div class="col-md-6 p-t-20">
                            <div class="form-group">
                              <label>First Name</label>
                              <input type="text" value="{{loanDetails.first_name}}" class="form-control" disabled>
                            </div>
                          </div>
                          <div class="col-md-6 p-t-20">
                            <div class="form-group">
                              <label>Last Name</label>
                              <input type="text" value="{{loanDetails.sur_name}}" class="form-control" disabled>
                            </div>
                          </div>
                          <div class="col-md-12 p-t-20">
                            <div class="form-group">
                              <label>Amount</label>
                              <input type="number" min="1" max="10000" class="form-control" ng-model="loanDetails.amount" disabled>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default" data-dismiss="modal">Close</button>
                  <button type="button" ng-click="RejectCoMaker()" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">
                    <i class="fa fa-thumbs-down"></i> Reject
                  </button>
                  <button type="button" ng-click="AcceptCoMaker()" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">
                    <i class="fa fa-thumbs-up"></i> Approved
                  </button>
                </div>
              </div>
            </div>
          </div>
          </div>
      <!-- end header -->
      <!-- start page container -->
      <div class="page-container">
        <!-- start sidebar menu -->
        <div class="sidebar-container">
          <div class="sidemenu-container navbar-collapse collapse fixed-menu">
            <div id="remove-scroll">
              <ul class="sidemenu page-header-fixed p-t-20" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                <li class="sidebar-toggler-wrapper hide">
                  <div class="sidebar-toggler">
                    <span></span>
                  </div>
                </li>
                <li class="sidebar-user-panel">
                  <div class="user-panel">
                    <div class="row">
                      <div class="sidebar-userpic">
                        <img style="width: 100px; height: 100px" src="<?=$img?>" class="img-responsive" alt=""> 
                      </div>
                    </div>
                    <div class="profile-usertitle">
                      <div class="sidebar-userpic-name"> <?=$_SESSION['userLog']['first_name']?>&nbsp;<?=$_SESSION['userLog']['sur_name']?></div>
                      <div class="profile-usertitle-job"> Member </div>
                    </div>
                    <div class="sidebar-userpic-btn">
                      <a class="tooltips" href="profile.php" data-placement="top" data-original-title="Profile">
                        <i class="material-icons">person_outline</i>
                      </a>
                      <label class="btn btn-circle btn-success btn-xs m-b-10">Online</label>
                      <a class="tooltips" href="../controllers/handlers/users.php?logout" data-placement="top" data-original-title="Logout">
                        <i class="material-icons">input</i>
                      </a>
                  </div>
                  </div>
                </li>
                <li class="menu-heading">
                  <span>-- Main</span>
                </li>
                <li class="nav-item" id="dashboard">
                  <a href="dashboard.php" class="nav-link nav-toggle"> <i class="material-icons">dashboard</i>
                  <span class="title">Dashboard</span> 
                  </a>
                </li>
                <li class="nav-item" id="announcement">
                  <a href="announcement.php" class="nav-link nav-toggle"> <i class="material-icons">announcement</i>
                  <span class="title">Announcements</span> 
                  </a>
                </li>
                <li class="nav-item" id="mycalendar">
                  <a href="calendar.php" class="nav-link nav-toggle"> <i class="material-icons">event</i>
                  <span class="title">Calendar</span> 
                  </a>
                </li>
                <li class="nav-item" id="saving">
                  <a href="saving.php" class="nav-link nav-toggle"> <i class="material-icons">attach_money</i>
                  <span class="title">Savings</span> 
                  </a>
                </li>
                <li class="nav-item" id="loans">
                  <a href="loan.php" class="nav-link nav-toggle"> <i class="material-icons">payment</i>
                  <span class="title">Loans</span> 
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <!-- end sidebar menu --> 

        