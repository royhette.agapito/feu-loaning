
        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy; CCS Savings and Loaning System By
            <a href="mailto:redstartheme@gmail.com" target="_top" class="makerCss">FEU Tech</a>
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->

    <script src="../assets/plugins/jquery/jquery.min.js" ></script>
    <script src="../assets/plugins/popper/popper.min.js" ></script>
    <script src="../assets/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
    <script src="../assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../assets/plugins/jquery-ui/jquery-ui.min.js" ></script>
    <!-- bootstrap -->
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- dropzone -->
    <script src="../assets/plugins/dropzone/dropzone.js" ></script>
    <script src="../assets/plugins/dropzone/dropzone-call.js" ></script>
    <!-- morris chart -->
    <script src="../assets/plugins/morris/morris.min.js" ></script>
    <script src="../assets/plugins/morris/raphael-min.js" ></script>
    <script src="../assets/js/pages/chart/morris/morris_home_data.js" ></script>
    <!-- calendar -->
    <!-- <script src="../assets/plugins/moment/moment.min.js" ></script>
    <script src="../assets/plugins/fullcalendar/fullcalendar.min.js" ></script>
    <script src="../assets/js/pages/calendar/calendar.min.js" ></script> -->
    <script src="../assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
    <script src="../assets/js/pages/table/table_data.js" ></script>
    <!-- Common js-->
    <script src="../assets/js/app.js" ></script>
    <script src="../assets/js/layout.js" ></script>
    <script src="../assets/js/theme-color.js" ></script>
    <!-- Material -->
    <script src="../assets/plugins/material/material.min.js"></script>
    <!-- animation -->
    <script src="../assets/js/pages/ui/animations.js" ></script>
    <!-- end js include path -->
    <script src="../assets/js/angular.min.js" ></script>
    <script src="../assets/js/angular-datatables.min.js"></script>
    <script src="../assets/js/jquery.dataTables.min.js"></script>
    <script src="../assets/js/angular-animate.min.js" ></script>
    <script src="../assets/js/angular-cookies.js" ></script>
    <script src="../assets/js/sweetalert.min.js" ></script>
    <script src="../assets/js/pages/sweet-alert/sweet-alert-data.js" ></script>
    <script type="text/javascript">
    	var app = angular.module("myApp", ['ngCookies','datatables']);
        app.controller('headerCtrl', function($scope, $http, $filter, $timeout, $cookies){
            $scope.loanDetails = {
                date_created:'',
                user_id:'',
                first_name:'',
                sur_name:'',
                amount:'',
            };
            $scope.getNotifDetailData={
                id:'',
                loanId:'',
                sender:'',
            }
            $scope.getNotification = function(){
                $http.post('api/notification.php?getAll').then(function(response){
                    $scope.AllNotificationData = response.data;
                });
                $http.post('api/notification.php?getAll1').then(function(response){
                    $scope.AllNotificationData1 = response.data;
                });
            }
            $scope.updateRemarksToRead = function(id){
                $http.post('api/notification.php?UpdateRemarks1',{id:id}).then(function(response){
                    $scope.getNotification();
                });
            }
            $scope.getDataLoan = function(id,loanId,sender){
                $scope.getNotifDetailData.id = id;
                $scope.getNotifDetailData.loanId = loanId;
                $scope.getNotifDetailData.sender = sender;
                $http.post('api/notification.php?getLoanDetails',{loanId:loanId}).then(function(response){
                    $scope.loanDetails.date_created = response.data.date_created;
                    $scope.loanDetails.user_id = response.data.user_id;
                    $scope.loanDetails.first_name = response.data.first_name;
                    $scope.loanDetails.sur_name = response.data.sur_name;
                    $scope.loanDetails.amount = response.data.amount;
                    console.log($scope.loanDetails);
                });
            }
            $scope.getNotification();
            $scope.AcceptCoMaker = function(){
                $http.post('api/notification.php?accept',{notifId:$scope.getNotifDetailData.id,loanId:$scope.getNotifDetailData.loanId,sender:$scope.getNotifDetailData.sender}).then(function(response){
                    $scope.getNotification();
                });
                $scope.getNotification();
            }
            $scope.RejectCoMaker = function(){
                $('#applyform').modal('hide');
                swal({
                    title: "Write Reason!",
                    text: "Write something:",
                    type: "input",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    animation: "slide-from-top",
                    inputPlaceholder: "Write something"
                }, function (inputValue) {
                    if (inputValue === false) return false;
                    if (inputValue === "") {
                        swal.showInputError("You need to write something!"); return false
                    }
                    $http.post('api/notification.php?reject',{notifId:$scope.getNotifDetailData.id,loanId:$scope.getNotifDetailData.loanId,sender:$scope.getNotifDetailData.sender,reason:inputValue}).then(function(response){
                        $scope.getNotification();
                    });
                    $scope.getNotification();
                    swal("Nice!", "You wrote: " + inputValue, "success");
                });
            }
        });
		app.controller('dashboardCtrl', function($scope, $http, $filter, $timeout, $cookies){
            $scope.NoShare = 5;
            $scope.seasonId = '';
            $scope.PayForDataAmount = {
                amountD:'',
                amountDeposit:'',
                max:'',
                id:''
            };
            $scope.paymentOption={
                options:'',
            };
            $scope.files = undefined;
            $scope.insertApplyLoan = {
                coMaker:'',
                amount:'',
                month:'',
            }
            $scope.types={
                type :1,
                Option :1,
            };
            $scope.checkpaymentStatus = {
                first:'',
                second:'',
                Third:'',
                Fourth:'',
                fifth:'',
                sixth:'',
            }
            $scope.getSelectedMemberData = {
                user_id:'',
                fname:'',
                lname:'',
                shares:'',
                amount:'',
                remarks:'',
                totalDays:'',
            }
            $scope.showJoinSeasonData = '';

            $scope.MySeasons = {
                id:'',
                start_date:'',
                end_date:'',
                first_payment:'',
                second_payment:'',
                penalty:'',
                amount_per_share:'',
                max_share:'',
            }
            $scope.PayForData = '';
            $scope.getAllComaker = function(){
                $http.post('api/loan.php?getAllComaker').then(function(response){
                    $scope.AllComakerData = response.data;
                });
            }
            $scope.getAllPrincipal = function(){
                $http.post('api/loan.php?getAllPrincipal').then(function(response){
                    $scope.AllPrincipalData = response.data;
                    console.log(response);
                });
            }
            $scope.getAllPrincipal();
            $scope.ChangeLoanAmount = function(id){
                var index = $scope.PayForData.findIndex(x=>x.id==id);
                $scope.PayForDataAmount.max = $scope.PayForData[index].amount;
                $scope.PayForDataAmount.amountD = $scope.PayForData[index].amount;
            }
            $scope.PayFor = function(){
                $http.post('api/loan.php?PayFor',{seasonId:$scope.MySeasons.id}).then(function(response){
                    console.log(response);
                    $scope.PayForData = response.data;
                    // if(response.data == true){
                    //     swal("success");
                    // }else{
                    //     $('#apply').modal('hide');
                    //     swal('error');
                    //     console.log(response);
                    // }
                });
            }
            $scope.PayFor();
            $scope.print = function(){
                var divToPrint=document.getElementById("order_table1");
                newWin= window.open("");
                newWin.document.write(divToPrint.outerHTML);
                newWin.print();
                newWin.close();
            }
            $scope.print1 = function(){
                var divToPrint=document.getElementById("order_table2");
                newWin= window.open("");
                newWin.document.write(divToPrint.outerHTML);
                newWin.print();
                newWin.close();
            }
            $scope.print2 = function(){
                var divToPrint=document.getElementById("order_table3");
                newWin= window.open("");
                newWin.document.write(divToPrint.outerHTML);
                newWin.print();
                newWin.close();
            }
            $scope.ApplyLoansss = function(coMaker,id,amount,month){
                if(coMaker == null){
                    swal('select CoMaker');
                }else if(amount == null){
                    swal('input amount'); 
                }else if(month == null){
                    swal('select month'); 
                }else if(month != null && amount != null && coMaker != null){
                    var index = $scope.getAllCoMaker.findIndex(x=>x.id==coMaker);
                    if(amount<=$scope.getAllCoMaker[index].savings){
                        swal("success");
                        $('#apply').modal('hide');
                        $scope.PayFor();
                        $http.post('api/loan.php?apply',{comaker:coMaker,seasonId:id,month:month,amount:amount}).then(function(response){
                        });
                    }else{
                        swal("insufficient savings");
                    }
                }else{
                    console.log(response);
                }
                
            }
            $scope.getAvailableComaker = function(amount){
                try {
                    $scope.ApplyLoan();
                    var index = $scope.getAllCoMaker.findIndex(x=>x.savings>=amount);
                    $scope.insertApplyLoan.coMaker = $scope.getAllCoMaker[index].id;
                }
                catch(err) {
                    $scope.insertApplyLoan.coMaker = '';
                }
            }
            $scope.getessss = function(){
                $http.post('api/season.php?getAll').then(function(response){
                    $scope.allAvailableSeasons = response.data;
                });
            }
            $scope.getessss();
            $scope.getMyseason = function(){
                $http.post('api/season.php?getMySeason').then(function(response){
                    $scope.MySeasons.id = response.data.id;
                    $scope.seasonId = response.data.id;
                    $scope.MySeasons.start_date = response.data.start_date;
                    $scope.MySeasons.end_date = response.data.end_date;
                    $scope.MySeasons.first_payment = response.data.first_payment;
                    $scope.MySeasons.second_payment = response.data.second_payment;
                    $scope.MySeasons.penalty = response.data.penalty;
                    $scope.MySeasons.amount_per_share = response.data.amount_per_share;
                    $scope.MySeasons.max_share = response.data.max_share;
                    if(response.data == ''){
                        $scope.MySeasons.id = '';
                    }else{
                        $scope.getSelectedMemberData.shares = response.data.joined[0].shares;
                    }
                });
            }
            $scope.getMyseason();
            $scope.ViewJoinSeason = function(){
                $scope.showJoinSeasonData = "read";
                $scope.getessss();
                $scope.viewAllseason();
            }
            $scope.viewAllseason = function(){
                $http.post('api/saving.php?getAll').then(function(response){
                    $scope.allSavingData = response.data;
                });
            }
            $scope.ApplyLoan = function(){
                $http.post('api/loan.php?getComakers').then(function(response){
                    $scope.getAllCoMaker = response.data;
                });
            }
            $scope.backJoinSeason = function(){
                $scope.showJoinSeasonData = '';
            }
            $scope.viewAvailableSeason = function(id){
                $http.post('api/season.php?ViewAvailableSeason',{id:id}).then(function(response){
                    $scope.viewAvailableSeasonData = response.data;
                });
            }
            $scope.JoinSeason = function(id,share){
                if((share>=5)&& (share<=50)){
                    $http.post('api/season.php?joinSeason',{seasonId:id,share:share}).then(function(response){
                        if(response.data == true){
                            $('#setting').modal('hide');
                            swal("Successfully Created", {icon: "success", customClass: 'animated tada'});
                            $scope.getMyseason();
                        }else{
                            swal("Error",{icon: "error", customClass: 'animated tada'}).then((willTrue)=>{
                            });
                        }
                        //     swal("Save Successfully! qwe");
                        //     $scope.getMyseason();
                        //     $('#setting').modal('hide');
                        // }else{
                        //     swal("error");
                        //     console.log(response);
                        // }
                    });
                }else{
                    swal("error");
                }
                
            }
            $scope.saveDepositSlipLoan = function(amountD,amount,id,type){
                var remark;
                var types;
                if($scope.files == undefined){
                    $('#imageupload').modal('hide');
                    swal("Select an Image");
                }else if(type==0){
                    types = "LOAN";
                    var file = $scope.files[0];
                    var fd = new FormData();
                    fd.append('file', file);
                    $http.post("api/upload.php", fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined,'Process-Data': false}
                    }).then(function(response){
                        $http.post("api/loan.php?insert", {image: response.data,seasonId: id, type:types, amount: amountD}).then(function(response){
                            if(response.data=true){
                                swal("Successfully Upload!");
                                $scope.files = undefined;
                                $scope.viewAllseason();
                                $('#imageupload').modal('hide');
                            }else{
                                swal("error");
                                console.log(response);
                            }
                            $scope.getuserdetails();
                        });
                    });

                }else{
                    swal("error");
                }
            }
            $scope.saveDepositSlip = function (id,type,Option){
                var remark;
                var types;
                if($scope.files == undefined){
                    $('#imageupload').modal('hide');
                    swal("Select an Image");
                }
                else if(type == 1){
                    types = "SAVINGS";
                    var file = $scope.files[0];
                    var fd = new FormData();
                    fd.append('file', file);
                    $http.post("api/upload.php", fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined,'Process-Data': false}
                    }).then(function(response){
                        $http.post("api/saving.php?insert", {image: response.data,seasonId: id, type:types, amount: $scope.getSelectedMemberData.amount,remarks:$scope.getSelectedMemberData.remarks}).then(function(response){
                            if(response.data=true){
                                swal("Successfully Upload!");
                                $scope.files = undefined;
                                $scope.viewAllseason();
                                $('#imageupload').modal('hide');
                            }else{
                                swal("error");
                                console.log(response);
                            }
                            $scope.getuserdetails();
                        });
                    });
                }else{
                    swal("error");
                }
            }
            $scope.onChangeSelectedPayment = function(){
                $http.post('api/getSeasonData.php',{id: $scope.seasonId}).then(function(response){
                    try {
                        var index = response.data.checkTblTransac.findIndex(x=>x.remarks=="First Payment");
                        var index1 = response.data.checkTblTransac.findIndex(x=>x.remarks=="Second Payment");
                        var index2 = response.data.checkTblTransac.findIndex(x=>x.remarks=="Third Payment");
                        var index3 = response.data.checkTblTransac.findIndex(x=>x.remarks=="Fourth Payment");
                        var index4 = response.data.checkTblTransac.findIndex(x=>x.remarks=="Fifth Payment");
                        var index5 = response.data.checkTblTransac.findIndex(x=>x.remarks=="Sixth Payment");
                        if((index<0) && (index1<0) &&(index2<0) && (index3<0) && (index4<0) && (index5<0)){
                            $scope.checkpaymentStatus.first = '';
                            $scope.checkpaymentStatus.second = 'meron';
                            $scope.checkpaymentStatus.Third = 'meron';
                            $scope.checkpaymentStatus.Fourth = 'meron';
                            $scope.checkpaymentStatus.fifth = 'meron';
                            $scope.checkpaymentStatus.sixth = 'meron';
                        }else if((index>=0) && (index1<0) &&(index2<0) && (index3<0) && (index4<0) && (index5<0)){
                            $scope.checkpaymentStatus.first = 'meron';
                            $scope.checkpaymentStatus.second = '';
                            $scope.checkpaymentStatus.Third = 'meron';
                            $scope.checkpaymentStatus.Fourth = 'meron';
                            $scope.checkpaymentStatus.fifth = 'meron';
                            $scope.checkpaymentStatus.sixth = 'meron';
                        }else if((index>=0) && (index1>=0) &&(index2<0) && (index3<0) && (index4<0) && (index5<0)){
                            $scope.checkpaymentStatus.first = 'meron';
                            $scope.checkpaymentStatus.second = 'meron';
                            $scope.checkpaymentStatus.Third = '';
                            $scope.checkpaymentStatus.Fourth = 'meron';
                            $scope.checkpaymentStatus.fifth = 'meron';
                            $scope.checkpaymentStatus.sixth = 'meron';
                        }else if((index>=0) && (index1>=0) &&(index2>=0) && (index3<0) && (index4<0) && (index5<0)){
                            $scope.checkpaymentStatus.first = 'meron';
                            $scope.checkpaymentStatus.second = 'meron';
                            $scope.checkpaymentStatus.Third = 'meron';
                            $scope.checkpaymentStatus.Fourth = '';
                            $scope.checkpaymentStatus.fifth = 'meron';
                            $scope.checkpaymentStatus.sixth = 'meron';
                        }else if((index>=0) && (index1>=0) &&(index2>=0) && (index3>=0) && (index4<0) && (index5<0)){
                            $scope.checkpaymentStatus.first = 'meron';
                            $scope.checkpaymentStatus.second = 'meron';
                            $scope.checkpaymentStatus.Third = 'meron';
                            $scope.checkpaymentStatus.Fourth = 'meron';
                            $scope.checkpaymentStatus.fifth = '';
                            $scope.checkpaymentStatus.sixth = 'meron';
                        }else if((index>=0) && (index1>=0) &&(index2>=0) && (index3>=0) && (index4>=0) && (index5<0)){
                            $scope.checkpaymentStatus.first = 'meron';
                            $scope.checkpaymentStatus.second = 'meron';
                            $scope.checkpaymentStatus.Third = 'meron';
                            $scope.checkpaymentStatus.Fourth = 'meron';
                            $scope.checkpaymentStatus.fifth = 'meron';
                            $scope.checkpaymentStatus.sixth = '';
                        }else if((index>=0) && (index1>=0) &&(index2>=0) && (index3>=0) && (index4>=0) && (index5>=0)){
                            $scope.checkpaymentStatus.first = 'meron';
                            $scope.checkpaymentStatus.second = 'meron';
                            $scope.checkpaymentStatus.Third = 'meron';
                            $scope.checkpaymentStatus.Fourth = 'meron';
                            $scope.checkpaymentStatus.fifth = 'meron';
                            $scope.checkpaymentStatus.sixth = 'meron';
                        }
                    } catch (error) {
                    }
                    if((response.data.totalDays>=1) && (response.data.totalDays<=30)){
                        $scope.getSelectedMemberData.totalDays =1;
                        if($scope.paymentOption.options =='0'){
                            if(response.data.firstpaymentDateEnd<=0){
                                $scope.getSelectedMemberData.remarks="First Payment";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares * response.data.amount_per_share;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * " + response.data.amount_per_share + "amount";
                            }else if((response.data.firstpaymentDateEnd>0) && (response.data.firstpaymentDateEnd<=7)){
                                $scope.getSelectedMemberData.remarks="First Payment";
                                var ewq = response.data.amount_per_share * .01;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.firstpaymentDateEnd>7) && (response.data.firstpaymentDateEnd<=14)){
                                $scope.getSelectedMemberData.remarks="First Payment";
                                var ewq = response.data.amount_per_share * .02;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.firstpaymentDateEnd>14) && (response.data.firstpaymentDateEnd<=21)){
                                $scope.getSelectedMemberData.remarks="First Payment";
                                var ewq = response.data.amount_per_share * .03;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }
                        }else if($scope.paymentOption.options  =='1'){
                            if(response.data.secpaymentDateEnd<=0){
                                $scope.getSelectedMemberData.remarks="Second Payment";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares * response.data.amount_per_share;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * " + response.data.amount_per_share + "amount";
                            }else if((response.data.secpaymentDateEnd>0) && (response.data.secpaymentDateEnd<=7)){
                                $scope.getSelectedMemberData.remarks="Second Payment";
                                var ewq = response.data.amount_per_share * .01;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.secpaymentDateEnd>7) && (response.data.secpaymentDateEnd<=14)){
                                $scope.getSelectedMemberData.remarks="Second Payment";
                                var ewq = response.data.amount_per_share * .02;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.secpaymentDateEnd>14) && (response.data.secpaymentDateEnd<=21)){
                                $scope.getSelectedMemberData.remarks="Second Payment";
                                var ewq = response.data.amount_per_share * .03;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }
                        }
                    }else if((response.data.totalDays>=31) && (response.data.totalDays<=60)){
                        $scope.getSelectedMemberData.totalDays =2;
                        if($scope.paymentOption.options =='0'){
                            if(response.data.firstpaymentDateEnd<=0){
                                $scope.getSelectedMemberData.remarks="First Payment";
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * " + response.data.amount_per_share + "amount";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares * response.data.amount_per_share;
                            }else if((response.data.firstpaymentDateEnd>0) && (response.data.firstpaymentDateEnd<=7)){
                                $scope.getSelectedMemberData.remarks="First Payment";
                                var ewq = response.data.amount_per_share * .01;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.firstpaymentDateEnd>7) && (response.data.firstpaymentDateEnd<=14)){
                                $scope.getSelectedMemberData.remarks="First Payment";
                                var ewq = response.data.amount_per_share * .02;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.firstpaymentDateEnd>14) && (response.data.firstpaymentDateEnd<=21)){
                                $scope.getSelectedMemberData.remarks="First Payment";
                                var ewq = response.data.amount_per_share * .03;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }
                        }else if($scope.paymentOption.options  =='1'){
                            if(response.data.secpaymentDateEnd<=0){
                                $scope.getSelectedMemberData.remarks="Second Payment";
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * " + response.data.amount_per_share + "amount";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares * response.data.amount_per_share;
                            }else if((response.data.secpaymentDateEnd>0) && (response.data.secpaymentDateEnd<=7)){
                                $scope.getSelectedMemberData.remarks="Second Payment";
                                var ewq = response.data.amount_per_share * .01;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.secpaymentDateEnd>7) && (response.data.secpaymentDateEnd<=14)){
                                $scope.getSelectedMemberData.remarks="Second Payment";
                                var ewq = response.data.amount_per_share * .02;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.secpaymentDateEnd>14) && (response.data.secpaymentDateEnd<=21)){
                                $scope.getSelectedMemberData.remarks="Second Payment";
                                var ewq = response.data.amount_per_share * .03;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }
                        }else if($scope.paymentOption.options =='2'){
                            if(response.data.thirdpaymentDateEnd<=0){
                                $scope.getSelectedMemberData.remarks="Third Payment";
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * " + response.data.amount_per_share + "amount";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares * response.data.amount_per_share;
                            }else if((response.data.thirdpaymentDateEnd>0) && (response.data.thirdpaymentDateEnd<=7)){
                                $scope.getSelectedMemberData.remarks="Third Payment";
                                var ewq = response.data.amount_per_share * .01;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.thirdpaymentDateEnd>7) && (response.data.thirdpaymentDateEnd<=14)){
                                $scope.getSelectedMemberData.remarks="Third Payment";
                                var ewq = response.data.amount_per_share * .02;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.thirdpaymentDateEnd>14) && (response.data.thirdpaymentDateEnd<=21)){
                                $scope.getSelectedMemberData.remarks="Third Payment";
                                var ewq = response.data.amount_per_share * .03;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }
                        }else if($scope.paymentOption.options =='3'){
                            if(response.data.fourthpaymentDateEnd<=0){
                                $scope.getSelectedMemberData.remarks="Fourth Payment";
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * " + response.data.amount_per_share + "amount";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares * response.data.amount_per_share;
                            }else if((response.data.fourthpaymentDateEnd>0) && (response.data.fourthpaymentDateEnd<=7)){
                                $scope.getSelectedMemberData.remarks="Fourth Payment";
                                var ewq = response.data.amount_per_share * .01;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.fourthpaymentDateEnd>7) && (response.data.fourthpaymentDateEnd<=14)){
                                $scope.getSelectedMemberData.remarks="Fourth Payment";
                                var ewq = response.data.amount_per_share * .02;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.fourthpaymentDateEnd>14) && (response.data.fourthpaymentDateEnd<=21)){
                                $scope.getSelectedMemberData.remarks="Fourth Payment";
                                var ewq = response.data.amount_per_share * .03;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }
                        }
                    }else if((response.data.totalDays>=61) && (response.data.totalDays<90)){
                        $scope.getSelectedMemberData.totalDays =3;
                        if($scope.paymentOption.options =='0'){
                            if(response.data.firstpaymentDateEnd<=0){
                                $scope.getSelectedMemberData.remarks="First Payment";
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * " + response.data.amount_per_share + "amount";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares * response.data.amount_per_share;
                            }else if((response.data.firstpaymentDateEnd>0) && (response.data.firstpaymentDateEnd<=7)){
                                $scope.getSelectedMemberData.remarks="First Payment";
                                var ewq = response.data.amount_per_share * .01;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.firstpaymentDateEnd>7) && (response.data.firstpaymentDateEnd<=14)){
                                $scope.getSelectedMemberData.remarks="First Payment";
                                var ewq = response.data.amount_per_share * .02;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.firstpaymentDateEnd>14) && (response.data.firstpaymentDateEnd<=21)){
                                $scope.getSelectedMemberData.remarks="First Payment";
                                var ewq = response.data.amount_per_share * .03;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }
                        }else if($scope.paymentOption.options  =='1'){
                            if(response.data.secpaymentDateEnd<=0){
                                $scope.getSelectedMemberData.remarks="Second Payment";
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * " + response.data.amount_per_share + "amount";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares * response.data.amount_per_share;
                            }else if((response.data.secpaymentDateEnd>0) && (response.data.secpaymentDateEnd<=7)){
                                $scope.getSelectedMemberData.remarks="Second Payment";
                                var ewq = response.data.amount_per_share * .01;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.secpaymentDateEnd>7) && (response.data.secpaymentDateEnd<=14)){
                                $scope.getSelectedMemberData.remarks="Second Payment";
                                var ewq = response.data.amount_per_share * .02;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.secpaymentDateEnd>14) && (response.data.secpaymentDateEnd<=21)){
                                $scope.getSelectedMemberData.remarks="Second Payment";
                                var ewq = response.data.amount_per_share * .03;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }
                        }else if($scope.paymentOption.options  =='2'){
                            if(response.data.thirdpaymentDateEnd<=0){
                                $scope.getSelectedMemberData.remarks="Third Payment";
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * " + response.data.amount_per_share + "amount";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares * response.data.amount_per_share;
                            }else if((response.data.thirdpaymentDateEnd>0) && (response.data.thirdpaymentDateEnd<=7)){
                                $scope.getSelectedMemberData.remarks="Third Payment";
                                var ewq = response.data.amount_per_share * .01;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.thirdpaymentDateEnd>7) && (response.data.thirdpaymentDateEnd<=14)){
                                $scope.getSelectedMemberData.remarks="Third Payment";
                                var ewq = response.data.amount_per_share * .02;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.thirdpaymentDateEnd>14) && (response.data.thirdpaymentDateEnd<=21)){
                                $scope.getSelectedMemberData.remarks="Third Payment";
                                var ewq = response.data.amount_per_share * .03;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }
                        }else if($scope.paymentOption.options  =='3'){
                            if(response.data.fourthpaymentDateEnd<=0){
                                $scope.getSelectedMemberData.remarks="Fourth Payment";
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * " + response.data.amount_per_share + "amount";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares * response.data.amount_per_share;
                            }else if((response.data.fourthpaymentDateEnd>0) && (response.data.fourthpaymentDateEnd<=7)){
                                $scope.getSelectedMemberData.remarks="Fourth Payment";
                                var ewq = response.data.amount_per_share * .01;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.fourthpaymentDateEnd>7) && (response.data.fourthpaymentDateEnd<=14)){
                                $scope.getSelectedMemberData.remarks="Fourth Payment";
                                var ewq = response.data.amount_per_share * .02;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.fourthpaymentDateEnd>14) && (response.data.fourthpaymentDateEnd<=21)){
                                $scope.getSelectedMemberData.remarks="Fourth Payment";
                                var ewq = response.data.amount_per_share * .03;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }
                        }else if($scope.paymentOption.options  =='4'){
                            if(response.data.fifthpaymentDateEnd<=0){
                                $scope.getSelectedMemberData.remarks="Fifth Payment";
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * " + response.data.amount_per_share + "amount";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares * response.data.amount_per_share;
                            }else if((response.data.fifthpaymentDateEnd>0) && (response.data.fifthpaymentDateEnd<=7)){
                                $scope.getSelectedMemberData.remarks="Fifth Payment";
                                var ewq = response.data.amount_per_share * .01;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.fifthpaymentDateEnd>7) && (response.data.fifthpaymentDateEnd<=14)){
                                $scope.getSelectedMemberData.remarks="Fifth Payment";
                                var ewq = response.data.amount_per_share * .02;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.fifthpaymentDateEnd>14) && (response.data.fifthpaymentDateEnd<=21)){
                                $scope.getSelectedMemberData.remarks="Fifth Payment";
                                var ewq = response.data.amount_per_share * .03;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }
                        }else if($scope.paymentOption.options  =='5'){
                            if(response.data.sixthpaymentDateEnd<=0){
                                $scope.getSelectedMemberData.remarks="Sixth Payment";
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * " + response.data.amount_per_share + "amount";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares * response.data.amount_per_share;
                            }else if((response.data.sixthpaymentDateEnd>0) && (response.data.sixthpaymentDateEnd<=7)){
                                $scope.getSelectedMemberData.remarks="Sixth Payment";
                                var ewq = response.data.amount_per_share * .01;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.sixthpaymentDateEnd>7) && (response.data.sixthpaymentDateEnd<=14)){
                                $scope.getSelectedMemberData.remarks="Sixth Payment";
                                var ewq = response.data.amount_per_share * .02;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.sixthpaymentDateEnd>14) && (response.data.sixthpaymentDateEnd<=21)){
                                $scope.getSelectedMemberData.remarks="Sixth Payment";
                                var ewq = response.data.amount_per_share * .03;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }
                        }
                    }
                });
            }
            $scope.uploadedFile = function(element) {
                var reader = new FileReader();
                reader.onload = function(event) {
                $scope.$apply(function($scope) {
                    $scope.files = element.files;
                    $scope.src = event.target.result
                });
                }
                reader.readAsDataURL(element.files[0]);
            }
        });
        app.controller('announcementCtrl', function($scope, $http, $filter, $timeout, $cookies){
            $scope.showReadMore = '';
            $http.post('api/announcement.php?getAll').then(function(response){
                $scope.allAnnouncementData = response.data;
            });
            $scope.ReadMore = function(id){
                $http.post('api/announcement.php?getReadAnnouncement',{id:id}).then(function(response){
                    $scope.AnnouncementData = response.data;
                });
                $scope.showReadMore = "read";
            }
            $scope.CloseReadMore = function(){
                $scope.showReadMore = '';
            }
        });
        app.controller('savingsCtrl', function($scope, $http, $filter, $timeout, $cookies){
            $http.post('api/saving.php?getAll').then(function(response){
                $scope.allSavingData = response.data;
                console.log(response);
            });
            $scope.print = function(){
                var divToPrint=document.getElementById("order_table1");
                newWin= window.open("");
                newWin.document.write(divToPrint.outerHTML);
                newWin.print();
                newWin.close();
            }
        });
        app.controller('loanCtrl', function($scope, $http, $filter, $timeout, $cookies){
            $scope.getAllPrincipal = function(){
                $http.post('api/loan.php?getAllPrincipal').then(function(response){
                    $scope.AllPrincipalData = response.data;
                });
            }
            $scope.getAllPrincipal();
            $scope.getAllComaker = function(){
                $http.post('api/loan.php?getAllComaker').then(function(response){
                    $scope.AllComakerData = response.data;
                });
            }
            $scope.print = function(){
                var divToPrint=document.getElementById("order_table1");
                newWin= window.open("");
                newWin.document.write(divToPrint.outerHTML);
                newWin.print();
                newWin.close();
            }
            $scope.print1 = function(){
                var divToPrint=document.getElementById("order_table2");
                newWin= window.open("");
                newWin.document.write(divToPrint.outerHTML);
                newWin.print();
                newWin.close();
            }
        });
        app.controller('profileCtrl', function($scope, $http, $filter, $timeout, $cookies){
            $scope.addNewMember = 0;
            $scope.userData;
            $scope.NewMember={
                empId:'',
                fname:'',
                mname:'',
                lname:'',
				email:'',
                phone:'',
                password:'',
                confirmPass:'',
            }
            $scope.userDetails = {
                id:'',
                email:'',
                fname:'',
                lname:'',
                phone:'',
            }
            $scope.addNewMembers = function(){
                if($scope.NewMember.empId == null || $scope.NewMember.empId == ""){
                    swal("Write your Member ID",{
                        icon: "warning",
                        dangerMode: true,
                    });
                }else if($scope.NewMember.fname == null || $scope.NewMember.fname == ""){
                    swal("Write your first name",{
                        icon: "warning",
                        dangerMode: true,
                    });}
                else if($scope.NewMember.mname == null || $scope.NewMember.mname == ""){
                    swal("Write your first name",{
                        icon: "warning",
                        dangerMode: true,
                    });
                }else if($scope.NewMember.lname == null || $scope.NewMember.lname == ""){
                    swal("Write your last name",{
                        icon: "warning",
                        dangerMode: true,
                    });
                }else if($scope.NewMember.email == null || $scope.NewMember.email == ""){
                    swal("Write email",{
                        icon: "warning",
                        dangerMode: true,
                    });
                }else if($scope.NewMember.phone == null || $scope.NewMember.phone == ""){
                    swal("Write phone number",{
                        icon: "warning",
                        dangerMode: true,
                    });
                }else if($scope.NewMember.password == null || $scope.NewMember.password == ""){
                    swal("Write Password",{
                        icon: "warning",
                        dangerMode: true,
                    });
                }else if($scope.NewMember.confirmPass == null || $scope.NewMember.confirmPass == ""){
                    swal("Write Confirm Password",{
                        icon: "warning",
                        dangerMode: true,
                    });
                }else if($scope.NewMember.password != $scope.NewMember.confirmPass){
                    swal("Password And Confirm Password is not Equal",{
                        icon: "warning",
                        dangerMode: true,
                    });
                }else if($scope.NewMember.password == $scope.NewMember.confirmPass){
                    $http.post('controller/CreateUser.php', {UserId: $scope.NewMember.empId, fname: $scope.NewMember.fname, lname: $scope.NewMember.lname, email: $scope.NewMember.email, phone: $scope.NewMember.phone, password: $scope.NewMember.password}).then(function(response){
                        if(response.data = "true"){
                            swal("Successfully Created", {icon: "success",}).then((WillOk)=>{
                                $scope.NewMember.empId = '';
                                $scope.NewClient.fname='';
                                $scope.NewClient.lname='';
                                $scope.NewClient.email='';
                                $scope.NewClient.password='';
                                $scope.NewClient.confirmPass='';
                                    $scope.addNewMember = 0;
                            });
                        }else{
                            swal("Error",{icon: "error", customClass: 'animated tada'}).then((willTrue)=>{
                            });
                        }
                    });
                }else{}
            }
            $scope.activedeactivateStatus = function(id,status){
                $http.post('controller/activatedeactivate.php',{id:id,status:status}).then(function(response){
                    console.log(response);
                    $scope.getallUser();
                });
            }
            $scope.getallUser = function(){
                $http.post('controller/getAllUser.php').then(function(response){
                    console.log(response);
                    $scope.userData = response.data;
                });
            }
            $scope.getallUser();
            $scope.updateUser = function(id,first_name,sur_name,email,mobile_number){
                $scope.userDetails.id = id;
                $scope.userDetails.email = email;
                $scope.userDetails.fname = first_name;
                $scope.userDetails.lname = sur_name;
                $scope.userDetails.phone = mobile_number;
            }
            $scope.saveUpdateUser = function(){
                var index = $scope.userData.findIndex(x=>x.id==$scope.userDetails.id);
                if($scope.userData[index].email == $scope.userDetails.email && $scope.userData[index].first_name == $scope.userDetails.fname && $scope.userData[index].sur_name == $scope.userDetails.lname && $scope.userData[index].mobile_number == $scope.userDetails.phone){
                    swal("no data change");
                }else{
                    if($scope.userDetails.email == null || $scope.userDetails.email == ' '){
                        swal("Write Email");
                    }else if($scope.userDetails.fname == null || $scope.userDetails.fname == ' '){
                        swal("Write First name");
                    }else if($scope.userDetails.lname == null || $scope.userDetails.lname == ' '){
                        swal("Write Last name");
                    }else if($scope.userDetails.phone == null || $scope.userDetails.phone == ' '){
                        swal("Write Mobile number");
                    }else{
                        $http.post('controller/updateDetailsUser.php',{id:$scope.userDetails.id,email:$scope.userDetails.email,fname:$scope.userDetails.fname,lname:$scope.userDetails.lname,phone:$scope.userDetails.phone}).then(function(response){
                            if(response.data = true){
                                $('#exampleModal').modal('hide');
                                swal('successfully update');
                            }else{
                                swal('error');
                            }
                        });
                    }
                }
            }
            $scope.addButton = function(){
                $scope.addNewMember = 1;
            }
            $scope.cancelButton = function(){
                $scope.addNewMember = 0;
            }
        });
        
    </script>
  </body>


</html>