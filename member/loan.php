<?php include 'header.php'; ?>
<!-- start page content -->
<div class="page-content-wrapper" ng-controller="loanCtrl" ng-cloak>
  <div class="page-content">
    <div class="page-bar">
      <div class="page-title-breadcrumb">
        <div class=" pull-left">
          <div class="page-title">Loans</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
          </li>
          <li class="active">Loans</li>
        </ol>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">  
        <div class="profile-tab-box">
          <div class="p-l-20">
            <ul class="nav ">
              <li class="nav-item tab-all" ng-click="getAllPrincipal()"><a
                class="nav-link active show" href="#tab1" data-toggle="tab">As Principal</a></li>
              <li class="nav-item tab-all p-l-20" ng-click="getAllComaker()"><a class="nav-link"
                href="#tab2" data-toggle="tab">As Co-Maker</a></li>
            </ul>
          </div>
        </div>
        <!-- Tab panes -->
        <div class="tab-content">
          <div class="tab-pane active fontawesome-demo" id="tab1">
            <div class="card card-topline-green">
              <div class="card-head">
                <header>List of Principal</header>
                <div class="tools">
                  <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                </div>
              </div>
              <div class="card-body ">
                <div class="row p-b-20">
                  <div class="col-md-6 col-sm-6 col-6">
                  </div>
                  <div class="col-md-6 col-sm-6 col-6">
                    <div class="btn-group pull-right">
                      <button class="btn deepPink-bgcolor  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                      <i class="fa fa-angle-down"></i>
                      </button>
                      <ul class="dropdown-menu pull-right">
                        <li>
                          <a href="javascript:;" ng-click="print()">
                          <i class="fa fa-print"></i> Print </a>
                        </li>
                        <li>
                          <a href="javascript:;">
                          <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                        </li>
                        <li>
                          <a href="javascript:;">
                          <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                
                <div class="table-responsive">
                  <table id="order_table1" datatable="ng" dt-options="vm.dtOptions" class="table table-striped table-bordered table-hover table-checkable order-column full-width">
                    <thead>
                      <tr>
                        <th> Employee ID </th>
                        <th> First Name </th>
                        <th> Last Name </th>
                        <th> Co-Maker </th>
                        <th>Status</th>
                        <th> Payment </th>
                        <th> Payment Date </th>
                        <th> Total </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="odd gradeX" ng-repeat="data in AllPrincipalData">
                        <td> {{data.id}} </td>
                        <td>
                        {{data.first_name}}
                        </td>
                        <td>
                        {{data.sur_name}}
                        </td>
                        <td>
                        <div ng-if="data.data.co1name != null">{{data.co1name}}</div>
                        <div ng-if="data.data.co1name != null && data.data.co2name != null">{{data.co1name +' & '+ data.co2name}}</div>
                        <div ng-if="data.data.co2name != null">{{data.co2name}}</div>
                        </td>
                        <td>
                          <span ng-if="data.status==null" class="label label-sm label-danger">Pending</span>
                          <span ng-if="data.status==1" class="label label-sm label-success">Approved</span>
                          <span ng-if="data.status==0" class="label label-sm label-danger">Declined</span>
                        </td>
                        <td>
                        {{data.amount}}
                        </td>
                        <td>
                        {{data.date_created}}
                        </td>
                        <td>
                        {{data.amount}}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane" id="tab2">
            <div class="card card-topline-green">
              <div class="card-head">
                <header>List of Co-Maker</header>
                <div class="tools">
                  <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                </div>
              </div>
              <div class="card-body ">
                <div class="row p-b-20">
                  <div class="col-md-6 col-sm-6 col-6">
                  </div>
                  <div class="col-md-6 col-sm-6 col-6">
                    <div class="btn-group pull-right">
                      <button class="btn deepPink-bgcolor  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                      <i class="fa fa-angle-down"></i>
                      </button>
                      <ul class="dropdown-menu pull-right">
                        <li>
                          <a href="javascript:;" ng-click="print1()">
                          <i class="fa fa-print"></i> Print </a>
                        </li>
                        <li>
                          <a href="javascript:;">
                          <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                        </li>
                        <li>
                          <a href="javascript:;">
                          <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                
                <div class="table-responsive">
                  <table id="order_table2" datatable="ng" dt-options="vm.dtOptions" class="table table-striped table-bordered table-hover table-checkable order-column full-width">
                    <thead>
                      <tr>
                        <th> Employee ID </th>
                        <th> First Name </th>
                        <th> Last Name </th>
                        <th> Co-Maker </th>
                        <th>Status</th>
                        <th> Payment </th>
                        <th> Payment Date </th>
                        <th> Total </th>
                      </tr>
                    </thead>
                    <tbody>
                    <tr class="odd gradeX" ng-repeat="data in AllComakerData">
                        <td> {{data.id}} </td>
                        <td>
                        {{data.first_name}}
                        </td>
                        <td>
                        {{data.sur_name}}
                        </td>
                        <td>
                        <div ng-if="data.data.co1name != null">{{data.co1name}}</div>
                        <div ng-if="data.data.co1name != null && data.data.co2name != null">{{data.co1name +' & '+ data.co2name}}</div>
                        <div ng-if="data.data.co2name != null">{{data.co2name}}</div>
                        </td>
                        <td>
                          <span ng-if="data.status==null" class="label label-sm label-danger">Pending</span>
                          <span ng-if="data.status==1" class="label label-sm label-success">Approved</span>
                          <span ng-if="data.status==0" class="label label-sm label-danger">Declined</span>
                        </td>
                        <td>
                        {{data.amount}}
                        </td>
                        <td>
                        {{data.date_created}}
                        </td>
                        <td>
                        {{data.amount}}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end page content -->
</div>
<!-- end page container -->
<?php include 'footer.php'; ?>
<script type="text/javascript">
  $("#loans").addClass("active");
</script>