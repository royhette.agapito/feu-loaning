<?php include 'header.php'; ?>
<div class="page-content-wrapper">
<div class="page-content">
  <div class="page-bar">
    <div class="page-title-breadcrumb">
      <div class=" pull-left">
        <div class="page-title">User Profile</div>
      </div>
      <ol class="breadcrumb page-breadcrumb pull-right">
        <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
        </li>
        <li class="active">User Profile</li>
      </ol>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <!-- BEGIN PROFILE SIDEBAR -->
      <div class="profile-sidebar">
        <div class="card card-topline-green">
          <div class="card-body no-padding height-9">
            <div class="row">
              <div class="profile-userpic">
                <!-- <form id="id_dropzone" class="dropzone">
                  <div class="dz-message">
                    <img src="../assets/img/dp.jpg" class="img-responsive" alt=""> 
                    <h3>Drop image here or click to upload.</h3>
                    <span class="note needsclick">(Please upload <strong>JPG and PNG</strong> files only.)</span>
                  </div>
                </form> -->
                <form id="frm_update">

                <div class="dropzone">
                  <label for="fileLoader" class="mdl-js-button dz-message" data-tooltip="Add an Image" data-position="center">
                    <img style="width: 200px; height: 200px" id="imgshow" src="<?=$img?>" class="img-responsive" alt=""> 
                  </label>
                  <input type="file" id="fileLoader" style="display:none" name="image" title="Load File" />
                </div>
                
              </div>
            </div>
            <div class="profile-usertitle">
              <div class="profile-usertitle-name"> <?=$_SESSION['userLog']['first_name']?> <?=$_SESSION['userLog']['sur_name']?> </div>
              <div class="profile-usertitle-job"> Member </div>
              <button type="submit" class="btn btn-success mb-3 float-right" onclick="update();">Save & Update</button>
            </div>
            <!-- END SIDEBAR USER TITLE -->
          </div>
        </div>
      </div>
      <!-- END BEGIN PROFILE SIDEBAR -->
      <!-- BEGIN PROFILE CONTENT -->
      <div class="profile-content">
        <div class="row">
          <div class="profile-tab-box">
            <div class="p-l-20">
              <ul class="nav">
                <li class="nav-item tab-all"><a
                  class="nav-link active show" href="#tab1" data-toggle="tab">About Me</a></li>

                <li class="nav-item tab-all p-l-20"><a class="nav-link"
                  href="#tab2" data-toggle="tab">Setting</a></li>
              </ul>
            </div>
          </div>
          <div class="white-box">
            <!-- Tab panes -->
            <div class="tab-content">
              <div class="tab-pane active fontawesome-demo" id="tab1">
                <div id="biography" >
                  <div class="row">
                    <div class="col-md-12 b-r">
                      <strong>Employee ID</strong>
                      <br>
                      <p class="text-muted"><?=$_SESSION['userLog']['id']?></p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6 col-6 b-r">
                      <strong>First Name</strong>
                      <br>
                      <p class="text-muted"><?=$_SESSION['userLog']['first_name']?></p>
                    </div>
                    <div class="col-md-6 col-6 b-r">
                      <strong>Last Name</strong>
                      <br>
                      <p class="text-muted"><?=$_SESSION['userLog']['sur_name']?></p>
                    </div>
                    <div class="col-md-6 col-6 b-r">
                      <strong>Mobile</strong>
                      <br>
                      <p class="text-muted"><?=$_SESSION['userLog']['mobile_number']?></p>
                    </div>
                    <div class="col-md-6 col-6 b-r">
                      <strong>Email</strong>
                      <br>
                      <p class="text-muted"><?=$_SESSION['userLog']['email']?></p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="tab2">
                <div class="row">
                  <div class="col-md-12 col-sm-12">
                <div class="card-body " id="bar-parent1">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="empid">Employee ID</label>
                          <input type="text" class="form-control" id="empid" placeholder="Enter employee ID" value="<?=$_SESSION['userLog']['id']?>" readonly>
                        </div>
                      </div>
                      <div class="col-md-6">  
                        <div class="form-group">
                          <label for="pnumber">Phone Number</label>
                          <input type="number" class="form-control" id="phone_number" name="mobile_number" placeholder="Enter phone number" value="<?=$_SESSION['userLog']['mobile_number']?>">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="fname">First Name</label>
                          <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Enter first name" value="<?=$_SESSION['userLog']['first_name']?>">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="lname">Last Name</label>
                          <input type="text" class="form-control" id="sur_name" name="sur_name" placeholder="Enter last name" value="<?=$_SESSION['userLog']['sur_name']?>">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="lname">Email</label>
                          <input type="text" class="form-control" id="email" name="email" placeholder="Enter last name" value="<?=$_SESSION['userLog']['email']?>">
                        </div>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-success mb-3 float-right" onclick="update();">Save & Update</button>
                  </form>
                  <div class="card-head"></div><br>
                  <form id="frm_pass">
                    <div class="row">
                      <div class="col-md-4">
                        <div class="form-group">
                          <label for="oldpassword">Old Password</label>
                          <input type="password" class="form-control" onkeyup="check();" id="oldpassword" name="oldpassword" placeholder="Old Password">
                        </div>
                      </div>
                      <div class="col-md-4">
                      <div class="form-group">
                        <label for="newpassword">New Password</label>
                        <input type="password" class="form-control" onkeyup="check2();" id="newpassword" name="newpassword" placeholder="New Password">
                      </div>
                    </div>
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="confirmpassword">Confirm Password</label>
                        <input type="password" class="form-control" onkeyup="check2();" id="confirmpassword" name="confirmpassword" placeholder="Confirm Password">
                      </div>
                    </div>
                    </div>
                    <button type="submit" onclick="updatePassword();" class="btn btn-success mb-3 float-right">Save & Update</button>
                  </form>
                </div>
              </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- END PROFILE CONTENT -->
    </div>
  </div>
</div>
<!-- end page content -->
<!-- start chat sidebar -->
<div class="chat-sidebar-container" data-close-on-body-click="false">
  <div class="chat-sidebar">
    <ul class="nav nav-tabs">
      <li class="nav-item">
        <a href="#quick_sidebar_tab_1" class="nav-link active tab-icon"  data-toggle="tab">Theme
        </a>
      </li>
      <li class="nav-item">
        <a href="#quick_sidebar_tab_2" class="nav-link tab-icon"  data-toggle="tab"> Chat
        </a>
      </li>
      <li class="nav-item">
        <a href="#quick_sidebar_tab_3" class="nav-link tab-icon"  data-toggle="tab">  Settings
        </a>
      </li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane chat-sidebar-settings in show active animated shake" role="tabpanel" id="quick_sidebar_tab_1">
        <div class="slimscroll-style">
          <div class="theme-light-dark">
            <h6>Sidebar Theme</h6>
            <button type="button" data-theme="white" class="btn lightColor btn-outline btn-circle m-b-10 theme-button">Light Sidebar</button>
            <button type="button" data-theme="dark" class="btn dark btn-outline btn-circle m-b-10 theme-button">Dark Sidebar</button>
          </div>
          <div class="theme-light-dark">
            <h6>Sidebar Color</h6>
            <ul class="list-unstyled">
              <li class="complete">
                <div class="theme-color sidebar-theme">
                  <a href="#" data-theme="white"><span class="head"></span><span class="cont"></span></a>
                  <a href="#" data-theme="dark"><span class="head"></span><span class="cont"></span></a>
                  <a href="#" data-theme="blue"><span class="head"></span><span class="cont"></span></a>
                  <a href="#" data-theme="indigo"><span class="head"></span><span class="cont"></span></a>
                  <a href="#" data-theme="cyan"><span class="head"></span><span class="cont"></span></a>
                  <a href="#" data-theme="green"><span class="head"></span><span class="cont"></span></a>
                  <a href="#" data-theme="red"><span class="head"></span><span class="cont"></span></a>
                </div>
              </li>
            </ul>
            <h6>Header Brand color</h6>
            <ul class="list-unstyled">
              <li class="theme-option">
                <div class="theme-color logo-theme">
                  <a href="#" data-theme="logo-white"><span class="head"></span><span class="cont"></span></a>
                  <a href="#" data-theme="logo-dark"><span class="head"></span><span class="cont"></span></a>
                  <a href="#" data-theme="logo-blue"><span class="head"></span><span class="cont"></span></a>
                  <a href="#" data-theme="logo-indigo"><span class="head"></span><span class="cont"></span></a>
                  <a href="#" data-theme="logo-cyan"><span class="head"></span><span class="cont"></span></a>
                  <a href="#" data-theme="logo-green"><span class="head"></span><span class="cont"></span></a>
                  <a href="#" data-theme="logo-red"><span class="head"></span><span class="cont"></span></a>
                </div>
              </li>
            </ul>
            <h6>Header color</h6>
            <ul class="list-unstyled">
              <li class="theme-option">
                <div class="theme-color header-theme">
                  <a href="#" data-theme="header-white"><span class="head"></span><span class="cont"></span></a>
                  <a href="#" data-theme="header-dark"><span class="head"></span><span class="cont"></span></a>
                  <a href="#" data-theme="header-blue"><span class="head"></span><span class="cont"></span></a>
                  <a href="#" data-theme="header-indigo"><span class="head"></span><span class="cont"></span></a>
                  <a href="#" data-theme="header-cyan"><span class="head"></span><span class="cont"></span></a>
                  <a href="#" data-theme="header-green"><span class="head"></span><span class="cont"></span></a>
                  <a href="#" data-theme="header-red"><span class="head"></span><span class="cont"></span></a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <!-- Start Doctor Chat --> 
      <div class="tab-pane chat-sidebar-chat animated slideInRight" id="quick_sidebar_tab_2">
        <div class="chat-sidebar-list">
          <div class="chat-sidebar-chat-users slimscroll-style" data-rail-color="#ddd" data-wrapper-class="chat-sidebar-list">
            <div class="chat-header">
              <h5 class="list-heading">Online</h5>
            </div>
            <ul class="media-list list-items">
              <li class="media">
                <img class="media-object" src="../assets/img/user/user3.jpg" width="35" height="35" alt="...">
                <i class="online dot"></i>
                <div class="media-body">
                  <h5 class="media-heading"><?=$_SESSION['userLog']['first_name']?></h5>
                  <div class="media-heading-sub"><?=$_SESSION['userLog']['sur_name']?></div>
                </div>
              </li>
              <li class="media">
                <div class="media-status">
                  <span class="badge badge-success">5</span>
                </div>
                <img class="media-object" src="../assets/img/user/user1.jpg" width="35" height="35" alt="...">
                <i class="busy dot"></i>
                <div class="media-body">
                  <h5 class="media-heading">Rajesh</h5>
                  <div class="media-heading-sub">Director</div>
                </div>
              </li>
              <li class="media">
                <img class="media-object" src="../assets/img/user/user5.jpg" width="35" height="35" alt="...">
                <i class="away dot"></i>
                <div class="media-body">
                  <h5 class="media-heading">Jacob Ryan</h5>
                  <div class="media-heading-sub">Ortho Surgeon</div>
                </div>
              </li>
              <li class="media">
                <div class="media-status">
                  <span class="badge badge-danger">8</span>
                </div>
                <img class="media-object" src="../assets/img/user/user4.jpg" width="35" height="35" alt="...">
                <i class="online dot"></i>
                <div class="media-body">
                  <h5 class="media-heading">Kehn Anderson</h5>
                  <div class="media-heading-sub">CEO</div>
                </div>
              </li>
              <li class="media">
                <img class="media-object" src="../assets/img/user/user2.jpg" width="35" height="35" alt="...">
                <i class="busy dot"></i>
                <div class="media-body">
                  <h5 class="media-heading">Sarah Smith</h5>
                  <div class="media-heading-sub">Anaesthetics</div>
                </div>
              </li>
              <li class="media">
                <img class="media-object" src="../assets/img/user/user7.jpg" width="35" height="35" alt="...">
                <i class="online dot"></i>
                <div class="media-body">
                  <h5 class="media-heading">Vlad Cardella</h5>
                  <div class="media-heading-sub">Cardiologist</div>
                </div>
              </li>
            </ul>
            <div class="chat-header">
              <h5 class="list-heading">Offline</h5>
            </div>
            <ul class="media-list list-items">
              <li class="media">
                <div class="media-status">
                  <span class="badge badge-warning">4</span>
                </div>
                <img class="media-object" src="../assets/img/user/user6.jpg" width="35" height="35" alt="...">
                <i class="offline dot"></i>
                <div class="media-body">
                  <h5 class="media-heading">Jennifer Maklen</h5>
                  <div class="media-heading-sub">Nurse</div>
                  <div class="media-heading-small">Last seen 01:20 AM</div>
                </div>
              </li>
              <li class="media">
                <img class="media-object" src="../assets/img/user/user8.jpg" width="35" height="35" alt="...">
                <i class="offline dot"></i>
                <div class="media-body">
                  <h5 class="media-heading">Lina Smith</h5>
                  <div class="media-heading-sub">Ortho Surgeon</div>
                  <div class="media-heading-small">Last seen 11:14 PM</div>
                </div>
              </li>
              <li class="media">
                <div class="media-status">
                  <span class="badge badge-success">9</span>
                </div>
                <img class="media-object" src="../assets/img/user/user9.jpg" width="35" height="35" alt="...">
                <i class="offline dot"></i>
                <div class="media-body">
                  <h5 class="media-heading">Jeff Adam</h5>
                  <div class="media-heading-sub">Compounder</div>
                  <div class="media-heading-small">Last seen 3:31 PM</div>
                </div>
              </li>
              <li class="media">
                <img class="media-object" src="../assets/img/user/user10.jpg" width="35" height="35" alt="...">
                <i class="offline dot"></i>
                <div class="media-body">
                  <h5 class="media-heading">Anjelina Cardella</h5>
                  <div class="media-heading-sub">Physiotherapist</div>
                  <div class="media-heading-small">Last seen 7:45 PM</div>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div class="chat-sidebar-item">
          <div class="chat-sidebar-chat-user">
            <div class="page-quick-sidemenu">
              <a href="javascript:;" class="chat-sidebar-back-to-list">
              <i class="fa fa-angle-double-left"></i>Back
              </a>
            </div>
            <div class="chat-sidebar-chat-user-messages">
              <div class="post out">
                <img class="avatar" alt="" src="../assets/img/empty.png" />
                <div class="message">
                  <span class="arrow"></span> <a href="javascript:;" class="name">Kiran Patel</a> <span class="datetime">9:10</span>
                  <span class="body-out"> could you send me menu icons ? </span>
                </div>
              </div>
              <div class="post in">
                <img class="avatar" alt="" src="../assets/img/user/user5.jpg" />
                <div class="message">
                  <span class="arrow"></span> <a href="javascript:;" class="name">Jacob Ryan</a> <span class="datetime">9:10</span>
                  <span class="body"> please give me 10 minutes. </span>
                </div>
              </div>
              <div class="post out">
                <img class="avatar" alt="" src="../assets/img/empty.png" />
                <div class="message">
                  <span class="arrow"></span> <a href="javascript:;" class="name">Kiran Patel</a> <span class="datetime">9:11</span>
                  <span class="body-out"> ok fine :) </span>
                </div>
              </div>
              <div class="post in">
                <img class="avatar" alt="" src="../assets/img/user/user5.jpg" />
                <div class="message">
                  <span class="arrow"></span> <a href="javascript:;" class="name">Jacob Ryan</a> <span class="datetime">9:22</span>
                  <span class="body">Sorry for
                  the delay. i sent mail to you. let me know if it is ok or not.</span>
                </div>
              </div>
              <div class="post out">
                <img class="avatar" alt="" src="../assets/img/dp.jpg" />
                <div class="message">
                  <span class="arrow"></span> <a href="javascript:;" class="name">Kiran Patel</a> <span class="datetime">9:26</span>
                  <span class="body-out"> it is perfect! :) </span>
                </div>
              </div>
              <div class="post out">
                <img class="avatar" alt="" src="../assets/img/dp.jpg" />
                <div class="message">
                  <span class="arrow"></span> <a href="javascript:;" class="name">Kiran Patel</a> <span class="datetime">9:26</span>
                  <span class="body-out"> Great! Thanks. </span>
                </div>
              </div>
              <div class="post in">
                <img class="avatar" alt="" src="../assets/img/user/user5.jpg" />
                <div class="message">
                  <span class="arrow"></span> <a href="javascript:;" class="name">Jacob Ryan</a> <span class="datetime">9:27</span>
                  <span class="body"> it is my pleasure :) </span>
                </div>
              </div>
            </div>
            <div class="chat-sidebar-chat-user-form">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Type a message here...">
                <div class="input-group-btn">
                  <button type="button" class="btn deepPink-bgcolor">
                  <i class="fa fa-arrow-right"></i>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End Doctor Chat --> 
      <!-- Start Setting Panel --> 
      <div class="tab-pane chat-sidebar-settings animated slideInUp" id="quick_sidebar_tab_3">
        <div class="chat-sidebar-settings-list slimscroll-style">
          <div class="chat-header">
            <h5 class="list-heading">Layout Settings</h5>
          </div>
          <div class="chatpane inner-content ">
            <div class="settings-list">
              <div class="setting-item">
                <div class="setting-text">Sidebar Position</div>
                <div class="setting-set">
                  <select class="sidebar-pos-option form-control input-inline input-sm input-small ">
                    <option value="left" selected="selected">Left</option>
                    <option value="right">Right</option>
                  </select>
                </div>
              </div>
              <div class="setting-item">
                <div class="setting-text">Header</div>
                <div class="setting-set">
                  <select class="page-header-option form-control input-inline input-sm input-small ">
                    <option value="fixed" selected="selected">Fixed</option>
                    <option value="default">Default</option>
                  </select>
                </div>
              </div>
              <div class="setting-item">
                <div class="setting-text">Sidebar Menu </div>
                <div class="setting-set">
                  <select class="sidebar-menu-option form-control input-inline input-sm input-small ">
                    <option value="accordion" selected="selected">Accordion</option>
                    <option value="hover">Hover</option>
                  </select>
                </div>
              </div>
              <div class="setting-item">
                <div class="setting-text">Footer</div>
                <div class="setting-set">
                  <select class="page-footer-option form-control input-inline input-sm input-small ">
                    <option value="fixed">Fixed</option>
                    <option value="default" selected="selected">Default</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="chat-header">
              <h5 class="list-heading">Account Settings</h5>
            </div>
            <div class="settings-list">
              <div class="setting-item">
                <div class="setting-text">Notifications</div>
                <div class="setting-set">
                  <div class="switch">
                    <label class = "mdl-switch mdl-js-switch mdl-js-ripple-effect" 
                      for = "switch-1">
                    <input type = "checkbox" id = "switch-1" 
                      class = "mdl-switch__input" checked>
                    </label>
                  </div>
                </div>
              </div>
              <div class="setting-item">
                <div class="setting-text">Show Online</div>
                <div class="setting-set">
                  <div class="switch">
                    <label class = "mdl-switch mdl-js-switch mdl-js-ripple-effect" 
                      for = "switch-7">
                    <input type = "checkbox" id = "switch-7" 
                      class = "mdl-switch__input" checked>
                    </label>
                  </div>
                </div>
              </div>
              <div class="setting-item">
                <div class="setting-text">Status</div>
                <div class="setting-set">
                  <div class="switch">
                    <label class = "mdl-switch mdl-js-switch mdl-js-ripple-effect" 
                      for = "switch-2">
                    <input type = "checkbox" id = "switch-2" 
                      class = "mdl-switch__input" checked>
                    </label>
                  </div>
                </div>
              </div>
              <div class="setting-item">
                <div class="setting-text">2 Steps Verification</div>
                <div class="setting-set">
                  <div class="switch">
                    <label class = "mdl-switch mdl-js-switch mdl-js-ripple-effect" 
                      for = "switch-3">
                    <input type = "checkbox" id = "switch-3" 
                      class = "mdl-switch__input" checked>
                    </label>
                  </div>
                </div>
              </div>
            </div>
            <div class="chat-header">
              <h5 class="list-heading">General Settings</h5>
            </div>
            <div class="settings-list">
              <div class="setting-item">
                <div class="setting-text">Location</div>
                <div class="setting-set">
                  <div class="switch">
                    <label class = "mdl-switch mdl-js-switch mdl-js-ripple-effect" 
                      for = "switch-4">
                    <input type = "checkbox" id = "switch-4" 
                      class = "mdl-switch__input" checked>
                    </label>
                  </div>
                </div>
              </div>
              <div class="setting-item">
                <div class="setting-text">Save Histry</div>
                <div class="setting-set">
                  <div class="switch">
                    <label class = "mdl-switch mdl-js-switch mdl-js-ripple-effect" 
                      for = "switch-5">
                    <input type = "checkbox" id = "switch-5" 
                      class = "mdl-switch__input" checked>
                    </label>
                  </div>
                </div>
              </div>
              <div class="setting-item">
                <div class="setting-text">Auto Updates</div>
                <div class="setting-set">
                  <div class="switch">
                    <label class = "mdl-switch mdl-js-switch mdl-js-ripple-effect" 
                      for = "switch-6">
                    <input type = "checkbox" id = "switch-6" 
                      class = "mdl-switch__input" checked>
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include 'footer.php'; ?>

<script type="text/javascript">
  var check_old = false;
  var check_same = false;

  var password = "<?=$_SESSION['userLog']['password']?>";
  console.log(password);
  function check() {

    var old = $('#oldpassword').val();
    if(old != password) {
      check_old = false;
      $('#oldpassword').css("color","red");
      $('#oldpassword').css("border-color","red");

    } else {
      check_old = true;
      $('#oldpassword').css("color","black");
      $('#oldpassword').css("border-color","black");

    }
  }

  function check2() {
    var pass1 = $('#newpassword').val();
    var pass2 = $('#confirmpassword').val();
    if(pass1 != pass2){
      check_same = false;
      $('#newpassword').css("color","red");
      $('#confirmpassword').css("border-color","red");
      $('#newpassword').css("border-color","red");
      $('#confirmpassword').css("color","red");

    }else {
      check_same = true;
      $('#newpassword').css("color","black");
      $('#confirmpassword').css("color","black");
      $('#confirmpassword').css("border-color","black");
      $('#newpassword').css("border-color","black");
    }
  }

  function updatePassword() {
    if(check_old == false && check_same == false) {
      swal("Invalid", "Check your old password And Password not match", "error");
    }  else if(check_same == false) {
      swal("Invalid", "Password not match", "error");
    } else if (check_old == false)  {
      swal("Invalid", "Old Password not match", "error");
    }
    else {
      var values = $("#frm_pass").serialize();
	console.log(values);
		$.ajax({
			url: '../controllers/handlers/users.php?update_pass&role=member',
			type: "POST",
        	data: values,
		}).done(function(response){
			if(response == "Success!") {
			swal({
				  	title:response,
				  	icon: "success",
					message:"Change password",
				  	dangerMode: false,
					}).then((willDelete) => {
						window.location.href = 	"profile.php";
				});
			}
			
    });

    }
  }


  function update() {
    if($('#first_name').val() == "" || $('#sur_name').val() == "" || $('#phone_number').val() == "" || $('#email').val() == "") {
      swal("Invalid", "Input All fields", "error");
    }
    else {
         // Get form
         var form = $('#frm_update')[0];
// Create an FormData object 
    var data = new FormData(form);
    // data: {image:$('#'),first_name:$('#first_name').val(),sur_name:$('#sur_name').val(),mobile_number:$('#phone_number').val(),email:$('#email').val()},


		$.ajax({
			url: '../controllers/handlers/users.php?update_from_member&role=member&id=<?=$_SESSION['userLog']['id']?>',
      type: "POST",
      data: data,
      enctype: 'multipart/form-data',
            processData: false,  // Important!
        contentType: false,
        cache: false,
    }).done(function(response){
			if(response == "Success!") {
			swal({
				  	title:response,
				  	icon: "success",
					message:"Update",
				  	dangerMode: false,
					}).then((willDelete) => {
						window.location.href = 	"profile.php";
				});
			}
			
    });

    }
  }







  Dropzone.options.myAwesomeDropzone = {
  maxFiles: 1,
  accept: function(file, done) {
    console.log("uploaded");
    done();
  },
  init: function() {
    this.on("maxfilesexceeded", function(file){
        alert("No more files please!");
    });
  }
};
$("#fileLoader").change(function () {
       if (this.files && this.files[0]) {
           var reader = new FileReader();
           reader.onload = function (e) {
               $('#imgshow').attr('src', e.target.result);
           }
           reader.readAsDataURL(this.files[0]);
       }
   });
</script>
