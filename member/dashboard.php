<?php include 'header.php'; ?>
  <!-- start page content -->
  <div class="page-content-wrapper" ng-controller="dashboardCtrl" ng-cloak>
    <!-- Modal  Setting -->
    <div class="modal fade" id="setting" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content card-box">
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="card-head">
                  <header>Settings</header>
                </div>
                <div class="card-body row">
                  <div class="col-lg-12 p-t-20">
                    <div class="form-group">
                      <label>Shares</label>
                      <input type="number" ng-model="NoShare" class="form-control" min="5" max="50" oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');" onKeyDown="if(this.value.length==2 && event.keyCode!=2) return false;">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer full-width text-center">
            <button type="button" class="btn purple btn-outline btn-circle margin-0" ng-click="JoinSeason(viewAvailableSeasonData.id,NoShare)">Join Season</button>
            <button type="button" class="btn blue btn-outline btn-circle margin-0" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="availableseason" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="card-head">
            <header>Available Season</header>
            <div class="tools">
              <a class="t-collapse btn-color fa fa-times" data-dismiss="modal"></a>
            </div>
          </div>
          <div class="card-body no-padding height-10">
            <div class="row">
              <div class="noti-information notification-menu">
                <div class="notification-list mail-list not-list small-slimscroll-style">
                  <a href="javascript:;" class="single-mail"> <span class="icon blue-bgcolor"> <i class="fa fa-calendar"></i>
                    </span># {{viewAvailableSeasonData.id}}
                    <span class="notificationtime">
                    </span>
                    <small>Season number</small>
                  </a>
                  <a href="javascript:;" class="single-mail"> <span class="icon blue-bgcolor"> <i class="fa fa-calendar"></i>
                    </span>{{viewAvailableSeasonData.first_payment}}
                    <span class="notificationtime">
                    </span>
                    <small>First Payment</small>
                  </a>
                  <a href="javascript:;" class="single-mail"> <span class="icon blue-bgcolor"> <i class="fa fa-calendar"></i>
                    </span>{{viewAvailableSeasonData.second_payment}}
                    <span class="notificationtime">
                    </span>
                    <small>Second Payment</small>
                  </a>
                  <a href="javascript:;" class="single-mail"> <span class="icon blue-bgcolor"> <i class="fa fa-calendar"></i>
                    </span>{{viewAvailableSeasonData.amount_per_share}}
                    <span class="notificationtime">
                    </span>
                    <small>Amount Per Share</small>
                  </a>
                  
                  <div class="full-width text-center">
                    <button type="button" class="btn purple btn-outline btn-circle margin-0"  data-dismiss="modal" data-toggle="modal" data-target="#setting">Join Season</button>
                    <!-- <button type="button" class="btn purple btn-outline btn-circle margin-0" ng-click="JoinSeason(viewAvailableSeasonData.id)">Join Season</button> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="page-content" ng-show="showJoinSeasonData == ''" ng-hide="showJoinSeasonData != ''">
      <div class="page-bar">
        <div class="page-title-breadcrumb">
          <div class=" pull-left">
            <div class="page-title">Dashboard</div>
          </div>
          <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Dashboard</li>
          </ol>
        </div>
      </div>
 
      <div class="row">
        <div class="col-md-6 col-sm-12 col-12">
          <div class="card  card-box" ng-if="MySeasons.id==''">
            <div class="card-head">
              <header>Available Seasons</header>
              <div class="tools">
                <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
              </div>
            </div>
            <div class="card-body no-padding height-9">
              <div class="row" >
                <div class="noti-information notification-menu">
                  <div class="notification-list mail-list not-list small-slimscroll-style">
                    <a href="javascript:;" class="single-mail" ng-click="viewAvailableSeason(data.id)" ng-repeat="data in allAvailableSeasons" data-toggle="modal" data-target="#availableseason"> <span class="icon blue-bgcolor" > <i class="fa fa-calendar"></i>
                    </span>#{{data.id}}
                    <span class="notificationtime">
                    <small>Season number</small>
                    </span>
                    </a>
                    <a href="javascript:;" ng-if="allAvailableSeasons.length<=2" style="visibility:hidden" class="single-mail"> 
                      <span class="icon blue-bgcolor"> <i class="fa fa-calendar"></i></span>
                      <span class="notificationtime">Please Choose Season</span>
                      <small>Choose Available seasons</small>
                    </a>
                    <div class="full-width text-center" ng-if="allAvailableSeasons.length<=2">
                      <button type="button" style="visibility:hidden" class="btn purple btn-outline btn-circle margin-0">View Season</button>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-sm-12 col-12">
          <div class="card  card-box">
            <div class="card-head">
              <header>Joined Season</header>
              <div class="tools">
                <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
              </div>
            </div>
            <div class="card-body no-padding height-9">
              <div class="row">
                <div class="noti-information notification-menu">
                  <div class="notification-list mail-list not-list small-slimscroll-style">
                    <div ng-if="MySeasons.id != ''">
                      <a href="javascript:;" class="single-mail"> <span class="icon blue-bgcolor"> <i class="fa fa-calendar"></i>
                        </span>#{{MySeasons.id}}
                        <span class="notificationtime">
                        </span>
                        <small>Season number</small>
                      </a>
                      <a href="javascript:;" class="single-mail"> <span class="icon blue-bgcolor" > <i class="fa fa-calendar"></i>
                        </span>{{MySeasons.start_date+" - "+MySeasons.end_date}}
                        <span class="notificationtime">
                        <small>Duration</small>
                        </span>
                      </a>
                      <a href="javascript:;" class="single-mail"> <span class="icon blue-bgcolor" > <i class="fa fa-calendar"></i>
                        </span>{{MySeasons.first_payment}}
                        <span class="notificationtime">
                        <small>First payment</small>
                        </span>
                      </a>
                      <a href="javascript:;" class="single-mail"> <span class="icon blue-bgcolor" > <i class="fa fa-calendar"></i>
                        </span>{{MySeasons.second_payment}}
                        <span class="notificationtime">
                        <small>First payment</small>
                        </span>
                      </a>
                      <a href="javascript:;" class="single-mail"> <span class="icon blue-bgcolor" > <i class="fa fa-calendar"></i>
                        </span>{{MySeasons.amount_per_share}}
                        <span class="notificationtime">
                        <small>Amount per Share</small>
                        </span>
                      </a>
                      <div class="full-width text-center">
                        <button type="button" class="btn purple btn-outline btn-circle margin-0" ng-click="ViewJoinSeason()">View Season</button>
                      </div>
                    </div>
                    <div ng-if="MySeasons.id == ''">
                      <a href="javascript:;" class="single-mail"> <span class="icon blue-bgcolor"> <i class="fa fa-calendar"></i>
                        </span>Please Choose Season
                        <span class="notificationtime">
                        </span>
                        <small>Choose Available seasons</small>
                      </a>
                      <a href="javascript:;" style="visibility:hidden" class="single-mail"> 
                        <span class="icon blue-bgcolor"> <i class="fa fa-calendar"></i></span>
                        <span class="notificationtime">Please Choose Season</span>
                        <small>Choose Available seasons</small>
                      </a>
                      <div class="full-width text-center">
                        <button type="button" style="visibility:hidden" class="btn purple btn-outline btn-circle margin-0">View Season</button>
                      </div>
                    </div>
                      
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="page-content" ng-show="showJoinSeasonData != ''" ng-hide="showJoinSeasonData == ''">
      <!-- Modal  Image Upload -->
      <div class="modal fade" id="imageupload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content card-box">
            <div class="modal-body">
              <div class="row">
                <div class="col-sm-12">
                  
                  <div class="card-head">
                    <header>Deposit Slip Upload</header>
                  </div>
                  <div class="card-body row">
                    <div class="col-12">
                      <center>
                        <div class="w-50">
                          <label for="fileLoader" class="mdl-js-button dropzone" data-tooltip="Add an Image" data-position="center">
                            <img id="imgshow" src="../assets/img/dp.jpg" class="img-responsive" alt=""> 
                          </label>
						              <input type="file" id="fileLoader" style="display:none" onchange="angular.element(this).scope().uploadedFile(this)" name="files" title="Load File" />
                        </div> 
                      </center>
                    </div>
                    <div class="col-lg-6 p-t-20">
                      <div class="form-group">
                        <label>Saving / Loan</label>
                        <select class="form-control" ng-model="types.type">
                          <option ng-value="1">Saving</option>
                          <option ng-value="0">Loan</option>
                        </select>
                      </div>
                    </div>
                    <div ng-if="types.type != 0"></div>

                    <div ng-if="types.type == 1" class="col-lg-6 p-t-20">
                      <div class="form-group">
                        <label>Payment</label>
                        <div ng-show="getSelectedMemberData.totalDays==1">
                          <select class="form-control" ng-model="paymentOption.options" ng-change="onChangeSelectedPayment()">
                            <option ng-if="checkpaymentStatus.first == ''" ng-value="0">First Payment</option>
                            <option ng-if="checkpaymentStatus.second == ''" ng-value="1">Second Payment</option>
                          </select>
                        </div>
                        <div ng-show="getSelectedMemberData.totalDays==2">
                          <select class="form-control" ng-model="paymentOption.options" ng-change="onChangeSelectedPayment()">
                            <option ng-if="checkpaymentStatus.first == ''" ng-value="0">First Payment</option>
                            <option ng-if="checkpaymentStatus.second == ''" ng-value="1">Second Payment</option>
                            <option ng-if="checkpaymentStatus.Third == ''" ng-value="2">third Payment</option>
                            <option ng-if="checkpaymentStatus.Fourth == ''" ng-value="3">Fourth Payment</option>
                          </select>
                        </div>
                        <div ng-show="getSelectedMemberData.totalDays==3">
                          <select class="form-control" ng-model="paymentOption.options" ng-change="onChangeSelectedPayment()">
                            <option ng-if="checkpaymentStatus.first == ''" ng-value="0">First Payment</option>
                            <option ng-if="checkpaymentStatus.second == ''" ng-value="1">Second Payment</option>
                            <option ng-if="checkpaymentStatus.Third == ''" ng-value="2">third Payment</option>
                            <option ng-if="checkpaymentStatus.Fourth == ''" ng-value="3">Fourth Payment</option>
                            <option ng-if="checkpaymentStatus.fifth == ''" ng-value="4">fifth Payment</option>
                            <option ng-if="checkpaymentStatus.sixth == ''" ng-value="5">sixth Payment</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div ng-if="types.type == 1" class="col-lg-6 p-t-20">
                      <div class="form-group">
                        <label>Share</label>
                        <input type="number" ng-model="getSelectedMemberData.shares" class="form-control" disabled>
                      </div>
                    </div>
                    <div ng-if="types.type == 1" class="col-lg-6 p-t-20">
                      <div class="form-group">
                        <label>Amount</label>
                          <input type="text" class="form-control" ng-model="getSelectedMemberData.amount" disabled>
                          <!-- <input type="text" ng-show="types.Option == 0" class="form-control" ng-model="MySeasons.second_payment" disabled> -->
                      </div>
                    </div>
                    <div ng-if="types.type == 0" class="col-lg-6 p-t-20">
                      <div class="form-group">
                        <label>Pay For:</label>
                        <select class="form-control" ng-model="PayForDataAmount.id" ng-change="ChangeLoanAmount(PayForDataAmount.id)">
                          <option ng-repeat="data in PayForData" ng-value="{{data.id}}">{{data.amount}}</option>
                        </select>
                      </div>
                    </div>
                    <div ng-if="types.type == 1" class="col-lg-12 p-t-20">
                      <div class="form-group">
                        <label>BreakDown:</label><br>
                        <center>{{breakdown}}</center>
                      </div>
                    </div>
                    <div ng-if="types.type == 0" class="col-lg-12 p-t-20">
                      <div class="form-group">
                        <label>Amount to deposit</label>
                        <input type="number" min="1" max="{{PayForDataAmount.max}}" pattern="/^-?\d+\.?\d*$/" ng-model="PayForDataAmount.amountDeposit" class="form-control">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default" data-dismiss="modal">Close</button>
              <button type="button" ng-if="types.type == 1" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink" ng-click="saveDepositSlip(MySeasons.id,types.type,types.Option)">Save changes</button>
              <button type="button" ng-if="types.type == 0" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink" ng-click="saveDepositSlipLoan(PayForDataAmount.amountDeposit,PayForDataAmount.amount,MySeasons.id,types.type)">Save changes</button>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal  Apply Loan -->
      <div class="modal fade" id="apply" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content card-box">
            <div class="modal-body">
              <div class="row">
                <div class="col-sm-12">
                  <div class="card-head">
                    <header>Loan Information</header>
                  </div>
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-6 p-t-20">
                        <div class="form-group">
                          <label>First Name</label>
                          <input type="text" value="<?=$_SESSION['userLog']['first_name']?>" class="form-control" disabled>
                        </div>
                      </div>
                      <div class="col-md-6 p-t-20">
                        <div class="form-group">
                          <label>Last Name</label>
                          <input type="text" value="<?=$_SESSION['userLog']['sur_name']?>" class="form-control" disabled>
                        </div>
                      </div>
                      <div class="col-md-12 p-t-20">
                        <div class="form-group">
                          <label>Amount</label>
                          <input type="number" min="1" max="10000" class="form-control" ng-model="insertApplyLoan.amount" ng-change="getAvailableComaker(insertApplyLoan.amount)">
                        </div>
                      </div>
                      <div class="col-lg-12 p-t-20"> 
                        <div class="form-group">
                          <label>Co-Maker</label>
                          <select class="form-control" ng-model="insertApplyLoan.coMaker">
                            <option ng-value="{{data.id}}" ng-if="data.savings >= insertApplyLoan.amount" ng-repeat="data in getAllCoMaker">{{data.name}}</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row form-group">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-6">
                        <center>
                          <div class="radio p-0">
                            <input type="radio" name="optionsRadios" ng-model="insertApplyLoan.month" id="optionsRadios1" value="1" checked="">
                            <label for="optionsRadios1">
                              1 Month
                            </label>
                          </div>
                        </center>
                      </div>

                      <div class="col-lg-4 col-md-4 col-sm-4 col-6">
                        <center>
                          <div class="radio p-0">
                            <input type="radio" name="optionsRadios" ng-model="insertApplyLoan.month" id="optionsRadios2" value="2" checked="">
                            <label for="optionsRadios2">
                              2 Months
                            </label>
                          </div>
                        </center>
                      </div>

                      <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                        <center>
                          <div class="radio p-0">
                            <input type="radio" name="optionsRadios" ng-model="insertApplyLoan.month" id="optionsRadios3" value="3" checked="">
                            <label for="optionsRadios3">
                              3 Months
                            </label>
                          </div>
                        </center>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        <div class="checkbox checkbox-icon-black">
                          <input id="checkbox1" type="checkbox">
                          <label for="checkbox1" class="mr-2">
                              I agree to the <a href="toc.php" target="_blank">Terms and Condition</a> of the company.
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink" ng-click="ApplyLoansss(insertApplyLoan.coMaker,MySeasons.id,insertApplyLoan.amount,insertApplyLoan.month)">Apply Loan</button>
            </div>
          </div>
        </div>
      </div>
      <div class="page-bar">
        <div class="page-title-breadcrumb">
          <div class=" pull-left">
            <div class="page-title">View Season # {{allAvailableSeasons[0].id}}</div>
          </div>
          <ol class="breadcrumb page-breadcrumb pull-right">
            <li>
              <i class="fa fa-home"></i>&nbsp;
              <a class="parent-item" href="dashboard.php">Home</a>&nbsp;
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a class="parent-item" ng-click="backJoinSeason()">Dasboard</a>&nbsp;
              <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">View Season</li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">  
          <div class="profile-tab-box">
            <div class="p-l-20">
              <ul class="nav ">
                <li class="nav-item tab-all mt-3"><a
                  class="nav-link active show" href="#tab1" data-toggle="tab">Savings</a></li>
                <li class="nav-item tab-all p-l-20 mt-3"><a class="nav-link"
                  href="#tab2" ng-click="PayFor();$scope.getAllComaker();" data-toggle="tab">Loans</a></li>
                <li class="nav-item tab-all p-l-20 mt-3">
                  <div class="btn-group">
                    <button class="btn btn-info" ng-click="onChangeSelectedPayment(); PayFor();" data-toggle="modal" data-target="#imageupload">
                      Image Upload<i class="fa fa-plus"></i>
                    </button>
                  </div>
                </li>
              </ul>
            </div>
          </div>
          <!-- Tab panes -->
          <div class="tab-content">
            <div class="tab-pane active fontawesome-demo" id="tab1">
              <div class="card card-topline-green">
                <div class="card-head">
                  <header>List of Savings</header>
                  <div class="tools">
                    <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="row p-b-20">
                    <div class="col-md-6 col-sm-6 col-6">
                      <!-- <div class="btn-group"> 
                        <button class="btn btn-info" data-toggle="modal" data-target="#setting">
                          Settings<i class="fa fa-cog"></i>
                        </button>
                      </div> -->
                    </div>
                    <div class="col-md-6 col-sm-6 col-6">
                      <div class="btn-group pull-right">
                        <button class="btn deepPink-bgcolor  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                        <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                          <li>
                            <a href="javascript:;" ng-click="print()">
                            <i class="fa fa-print"></i> Print </a>
                          </li>
                          <li>
                            <a href="javascript:;">
                            <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                          </li>
                          <li>
                            <a href="javascript:;">
                            <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  
                  <div class="table-responsive">
                    <table id="order_table1" datatable="ng" dt-options="vm.dtOptions" class="table table-striped table-bordered table-hover table-checkable order-column full-width">
                      <thead>
                        <tr>
                          
                          <th> Season Number </th>
                          <th> Employee ID </th>
                          <th> First Name </th>
                          <th> Last Name </th>
                          <th> Status </th>
                          <th> Date Created</th>
                          <th> Amount </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr class="odd gradeX" ng-repeat="data in allSavingData">
                          <td> {{data.season_id}} </td>
                          <td> {{data.user_id}} </td>
                          <td>
                          {{data.first_name}}
                          </td>
                          <td>
                          {{data.sur_name}}
                          </td>
                          <td>
                            <span ng-if="data.status==null" class="label label-sm label-danger">Pending</span>
                            <span ng-if="data.status==1" class="label label-sm label-success">Approved</span>
                            <span ng-if="data.status==0" class="label label-sm label-danger">Declined</span>
                          </td>
                          <td>
                          {{data.date_created}}
                          </td>
                          <td>
                          {{data.amount}}
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="tab2">
              <div class="card card-topline-green">
                <div class="card-head">
                  <header></header>
                  <div class="tools">
                    <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="row">
                    <div class="profile-tab-box">
                      <div class="p-l-20">
                        <ul class="nav ">
                          <li class="nav-item tab-all"><a
                            class="nav-link active show" href="#tabprincipal" ng-click="getAllPrincipal()" data-toggle="tab">As Principal</a></li>
                          <li class="nav-item tab-all p-l-20"><a class="nav-link"
                            href="#tabcomaker" ng-click="getAllComaker()" data-toggle="tab">As Co-Maker</a></li>
                          <li class="nav-item tab-all p-l-20">
                            <div class="btn-group"> 
                            <button ng-show="PayForData == ''" class="btn btn-info" data-toggle="modal" ng-click="ApplyLoan()" data-target="#apply">
                              Apply Loan<i class="fa fa-usd"></i>
                            </button>
                          </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div class="white-box">
                      
                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div class="tab-pane active fontawesome-demo" id="tabprincipal">
                          <div class="row p-b-20">
                            <div class="col-md-12 col-sm-12 col-12">
                              <div class="btn-group pull-right">
                                <button class="btn deepPink-bgcolor  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                  <li>
                                    <a href="javascript:;" ng-click="print1()">
                                    <i class="fa fa-print"></i> Print </a>
                                  </li>
                                  <li>
                                    <a href="javascript:;">
                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                  </li>
                                  <li>
                                    <a href="javascript:;">
                                    <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </div>
                          <div id="biography">
                            <div class="table-responsive">
                              <table id="order_table2" datatable="ng" dt-options="vm.dtOptions" class="table table-striped table-bordered table-hover table-checkable order-column full-width">
                                <thead>
                                  <tr>
                                    <th> Employee ID </th>
                                    <th> First Name </th>
                                    <th> Last Name </th>
                                    <th> Amount Left </th>
                                    <th> Co-Maker </th>
                                    <th> Status </th>
                                    <th> Payment </th>
                                    <th> Payment Date </th>
                                    <th> Total </th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr class="odd gradeX" ng-repeat="data in AllPrincipalData">
                                    <td> {{data.id}} </td>
                                    <td>
                                    {{data.first_name}}
                                    </td>
                                    <td>
                                    {{data.sur_name}}
                                    </td>
                                    <td>{{data.amount_left}}</td>
                                    <td>
                                    <div ng-if="data.co1name != null">{{data.co1name}}</div>
                                    <div ng-if="data.co1name != null && data.co2name != null">{{data.co1name +' & '+ data.co2name}}</div>
                                    <div ng-if="data.co2name != null">{{data.co2name}}</div>
                                    </td>
                                    <td>
                                      <span ng-if="data.status==null" class="label label-sm label-danger">Pending</span>
                                      <span ng-if="data.status==1" class="label label-sm label-success">Approved</span>
                                      <span ng-if="data.status==0" class="label label-sm label-danger">Declined</span>
                                    </td>
                                    <td>
                                    {{data.amount}}
                                    </td>
                                    <td>
                                    {{data.date_created}}
                                    </td>
                                    <td>
                                    {{data.amount}}
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                        <div class="tab-pane" id="tabcomaker">
                          <div class="row p-b-20">
                            <div class="col-md-12 col-sm-12 col-12">
                              <div class="btn-group pull-right">
                                <button class="btn deepPink-bgcolor  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu pull-right">
                                  <li>
                                    <a href="javascript:;" ng-click="print2()">
                                    <i class="fa fa-print"></i> Print </a>
                                  </li>
                                  <li>
                                    <a href="javascript:;">
                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                  </li>
                                  <li>
                                    <a href="javascript:;">
                                    <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          </div>
                          <div class="table-responsive">
                            <table id="order_table3" datatable="ng" dt-options="vm.dtOptions" class="table table-striped table-bordered table-hover table-checkable order-column full-width">
                              <thead>
                                <tr>
                                  <th> Employee ID </th>
                                  <th> First Name </th>
                                  <th> Last Name </th>
                                  <th> Amount Left </th>
                                  <th> Co-Maker </th>
                                  <th> Payment </th>
                                  <th> Payment Date </th>
                                  <th> Total </th>
                                </tr>
                              </thead>
                              <tbody>
                              <tr class="odd gradeX" ng-repeat="data in AllComakerData">
                                  <td> {{data.id}} </td>
                                  <td>
                                  {{data.first_name}}
                                  </td>
                                  <td>
                                  {{data.sur_name}}
                                  </td>
                                  <td>{{data.amount_left}}</td>
                                  <td>
                                  <div ng-if="data.co1name != null">{{data.co1name}}</div>
                                  <div ng-if="data.co1name != null && data.co2name != null">{{data.co1name +' & '+ data.co2name}}</div>
                                  <div ng-if="data.co2name != null">{{data.co2name}}</div>
                                  </td>
                                  <td>
                                  {{data.amount}}
                                  </td>
                                  <td>
                                  {{data.date_created}}
                                  </td>
                                  <td>
                                  {{data.amount}}
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end page content -->

<?php include 'footer.php'; ?>

<script type="text/javascript">
  $("#dashboard").addClass("active");
</script>
<script>
   $("#fileLoader").change(function () {
       if (this.files && this.files[0]) {
           var reader = new FileReader();
           reader.onload = function (e) {
               $('#imgshow').attr('src', e.target.result);
           }
           reader.readAsDataURL(this.files[0]);
       }
   });
</script>