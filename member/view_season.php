<?php include 'header.php'; ?>
  
 
  <!-- start page content -->
  <div class="page-content-wrapper">
    <div class="page-content">
      <!-- Modal  Image Upload -->
      <div class="modal fade" id="imageupload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content card-box">
            <div class="modal-body">
              <div class="row">
                <div class="col-sm-12">
                  
                  <div class="card-head">
                    <header>Deposit Slip Upload</header>
                  </div>
                  <div class="card-body row">
                    <div class="col-12">
                      <center>
                        <div class="w-50">
                          <form id="id_dropzone" class="dropzone">
                            <div class="dz-message">
                              <img src="../assets/img/dp.jpg" class="img-responsive" alt=""> 
                            </div>
                          </form>
                        </div> 
                      </center>
                    </div>
                    <div class="col-lg-6 p-t-20">
                      <div class="form-group">
                        <label>Saving / Loan</label>
                        <select class="form-control">
                          <option>Saving</option>
                          <option>Loan</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                      <div class="form-group">
                        <label>Payment</label>
                        <select class="form-control">
                          <option>First Payment</option>
                          <option>Second Payment</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                      <div class="form-group">
                        <label>Penalty</label>
                        <input type="number" class="form-control" disabled>
                      </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                      <div class="form-group">
                        <label>Amount</label>
                        <input type="text" class="form-control" disabled>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Save changes</button>
            </div>
          </div>
        </div>
      </div>

      <!-- Modal  Setting -->
      <div class="modal fade" id="setting" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content card-box">
            <div class="modal-body">
              <div class="row">
                <div class="col-sm-12">
                  <div class="card-head">
                    <header>Settings</header>
                  </div>
                  <div class="card-body row">
                    <div class="col-lg-12 p-t-20">
                      <div class="form-group">
                        <label>Shares</label>
                        <input type="number" class="form-control">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Save changes</button>
            </div>
          </div>
        </div>
      </div>

      <!-- Modal  Apply Loan -->
      <div class="modal fade" id="apply" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content card-box">
            <div class="modal-body">
              <div class="row">
                <div class="col-sm-12">
                  <div class="card-head">
                    <header>Settings</header>
                  </div>
                  <div class="card-body">
                    <div class="row">
                      <div class="col-md-6 p-t-20">
                        <div class="form-group">
                          <label>First Name</label>
                          <input type="text" class="form-control" disabled>
                        </div>
                      </div>
                      <div class="col-md-6 p-t-20">
                        <div class="form-group">
                          <label>Last Name</label>
                          <input type="text" class="form-control" disabled>
                        </div>
                      </div>
                      <div class="col-md-12 p-t-20">
                        <div class="form-group">
                          <label>Amount</label>
                          <input type="number" class="form-control">
                        </div>
                      </div>
                      <div class="col-lg-12 p-t-20"> 
                        <div class="form-group">
                          <label>Co-Maker</label>
                          <select class="form-control">
                            <option>asd1</option>
                            <option>asd2</option>
                          </select>
                        </div>
                      </div>
                    </div>

                    <div class="row form-group">
                      <div class="col-lg-4 col-md-4 col-sm-4 col-6">
                        <center>
                          <div class="radio p-0">
                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">
                            <label for="optionsRadios1">
                              1 Month
                            </label>
                          </div>
                        </center>
                      </div>

                      <div class="col-lg-4 col-md-4 col-sm-4 col-6">
                        <center>
                          <div class="radio p-0">
                            <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2" checked="">
                            <label for="optionsRadios1">
                              2 Months
                            </label>
                          </div>
                        </center>
                      </div>

                      <div class="col-lg-4 col-md-4 col-sm-4 col-12">
                        <center>
                          <div class="radio p-0">
                            <input type="radio" name="optionsRadios" id="optionsRadios3" value="option3" checked="">
                            <label for="optionsRadios3">
                              3 Months
                            </label>
                          </div>
                        </center>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Save changes</button>
            </div>
          </div>
        </div>
      </div>
      <div class="page-bar">
        <div class="page-title-breadcrumb">
          <div class=" pull-left">
            <div class="page-title">View Season #</div>
          </div>
          <ol class="breadcrumb page-breadcrumb pull-right">
            <li>
              <i class="fa fa-home"></i>&nbsp;
              <a class="parent-item" href="dasboard.php">Home</a>&nbsp;
              <i class="fa fa-angle-right"></i>
            </li>
            <li>
              <a class="parent-item" href="dasboard.php">Dasboard</a>&nbsp;
              <i class="fa fa-angle-right"></i>
            </li>
            <li class="active">View Season</li>
          </ol>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">  
          <div class="profile-tab-box">
            <div class="p-l-20">
              <ul class="nav ">
                <li class="nav-item tab-all"><a
                  class="nav-link active show" href="#tab1" data-toggle="tab">Savings</a></li>
                <li class="nav-item tab-all p-l-20"><a class="nav-link"
                  href="#tab2" data-toggle="tab">Loans</a></li>
              </ul>
            </div>
          </div>
          <!-- Tab panes -->
          <div class="tab-content">
            <div class="tab-pane active fontawesome-demo" id="tab1">
              <div class="card card-topline-green">
                <div class="card-head">
                  <header>List of Savings</header>
                  <div class="tools">
                    <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="row p-b-20">
                    <div class="col-md-6 col-sm-6 col-6">
                      <div class="btn-group mt-3">
                        <button class="btn btn-info" data-toggle="modal" data-target="#imageupload">
                          Image Upload<i class="fa fa-plus"></i>
                        </button>
                      </div>
                      <div class="btn-group mt-3"> 
                        <button class="btn btn-info" data-toggle="modal" data-target="#setting">
                          Settings<i class="fa fa-cog"></i>
                        </button>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-6">
                      <div class="btn-group pull-right mt-3">
                        <button class="btn deepPink-bgcolor  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                        <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                          <li>
                            <a href="javascript:;">
                            <i class="fa fa-print"></i> Print </a>
                          </li>
                          <li>
                            <a href="javascript:;">
                            <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                          </li>
                          <li>
                            <a href="javascript:;">
                            <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  
                  <div class="table-responsive">
                    <table datatable="ng" dt-options="vm.dtOptions" class="table table-striped table-bordered table-hover table-checkable order-column full-width">
                      <thead>
                        <tr>
                          <th> Employee ID </th>
                          <th> First Name </th>
                          <th> Last Name </th>
                          <th> Co-Maker </th>
                          <th> Payment </th>
                          <th> Payment Date </th>
                          <th> Total </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr class="odd gradeX">
                          <td> shuxer </td>
                          <td>
                            <a href="mailto:shuxer@gmail.com"> shuxer@gmail.com </a>
                          </td>
                          <td>
                            <div class="place"></div>
                          </td>
                          <td> 12 Jan 2012 </td>
                          <td class="valigntop">
                            <button class="btn btn-primary btn-xs tooltips" data-placement="top" data-original-title="Edit">
                              <i class="fa fa-pencil"></i>
                            </button>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="tab2">
              <div class="card card-topline-green">
                <div class="card-head">
                  <header></header>
                  <div class="tools">
                    <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="row">
                    <div class="profile-tab-box">
                      <div class="p-l-20">
                        <ul class="nav ">
                          <li class="nav-item tab-all mt-2"><a
                            class="nav-link active show" href="#tabprincipal" data-toggle="tab">As Principal</a></li>
                          <li class="nav-item tab-all p-l-20 mt-2"><a class="nav-link"
                            href="#tabcomaker" data-toggle="tab">As Co-Maker</a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="white-box">
                      <div class="row p-b-20">
                        <div class="col-md-6 col-sm-6 col-6">
                          <div class="btn-group mt-2">
                            <button class="btn btn-info" data-toggle="modal" data-target="#imageupload">
                              Image Upload<i class="fa fa-plus"></i>
                            </button>
                          </div>
                          <div class="btn-group mt-2"> 
                            <button class="btn btn-info" data-toggle="modal" data-target="#apply">
                              Apply Loan<i class="fa fa-usd"></i>
                            </button>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-6">
                          <div class="btn-group pull-right">
                            <button class="btn deepPink-bgcolor  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                            <i class="fa fa-angle-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                              <li>
                                <a href="javascript:;">
                                <i class="fa fa-print"></i> Print </a>
                              </li>
                              <li>
                                <a href="javascript:;">
                                <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                              </li>
                              <li>
                                <a href="javascript:;">
                                <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div class="tab-pane active fontawesome-demo" id="tabprincipal">
                          <div id="biography">
                            <div class="table-responsive">
                              <table datatable="ng" dt-options="vm.dtOptions" class="table table-striped table-bordered table-hover table-checkable order-column full-width">
                                <thead>
                                  <tr>
                                    <th> Employee ID </th>
                                    <th> First Name </th>
                                    <th> Last Name </th>
                                    <th> Co-Maker </th>
                                    <th> Payment </th>
                                    <th> Payment Date </th>
                                    <th> Total </th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr class="odd gradeX">
                                    <td> shuxer </td>
                                    <td>
                                      <a href="mailto:shuxer@gmail.com"> shuxer@gmail.com </a>
                                    </td>
                                    <td>
                                      <div class="place"></div>
                                    </td>
                                    <td> 12 Jan 2012 </td>
                                    <td class="valigntop">
                                      <button class="btn btn-primary btn-xs tooltips" data-placement="top" data-original-title="Edit">
                                        <i class="fa fa-pencil"></i>
                                      </button>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                        <div class="tab-pane" id="tabcomaker">
                          <div class="table-responsive">
                            <table datatable="ng" dt-options="vm.dtOptions" class="table table-striped table-bordered table-hover table-checkable order-column full-width">
                              <thead>
                                <tr>
                                  <th> Employee ID </th>
                                  <th> First Name </th>
                                  <th> Last Name </th>
                                  <th> Co-Maker </th>
                                  <th> Payment </th>
                                  <th> Payment Date </th>
                                  <th> Total </th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr class="odd gradeX">
                                  <td> shuxer </td>
                                  <td>
                                    <a href="mailto:shuxer@gmail.com"> shuxer@gmail.com </a>
                                  </td>
                                  <td>
                                    <div class="place"></div>
                                  </td>
                                  <td> 12 Jan 2012 </td>
                                  <td class="valigntop">
                                    <button class="btn btn-primary btn-xs tooltips" data-placement="top" data-original-title="Edit">
                                      <i class="fa fa-pencil"></i>
                                    </button>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
 
    
    </div>
  </div>
  <!-- end page content -->

<?php include 'footer.php'; ?>

<script type="text/javascript">
  $("#dashboard").addClass("active");
</script>