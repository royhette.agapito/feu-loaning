<?php
 include("../../env.php");
 $env = new env();
 $conn = $env->connectDb();
 $dateToday = date('Y-m-d H:i:s');
$data = json_decode(file_get_contents("php://input"));
 if(!isset($_SESSION)) {
    session_start();
  }

if(isset($_GET['getAll'])) {
    $res = $conn->query("SELECT * FROM  tbl_seasons WHERE status = 1 ");
    $arr = [];
    while($row = $res->fetch_assoc()) {
        array_push($arr,$row);
    }
    echo json_encode($arr,JSON_NUMERIC_CHECK);
}
if(isset($_GET['ViewAvailableSeason'])) {
    $res = $conn->query("SELECT * FROM  tbl_seasons WHERE id = '$data->id' ");
    $row = $res->fetch_assoc();
    echo json_encode($row,JSON_NUMERIC_CHECK);
}

if(isset($_GET['getMySeason'])) {
    $res = $conn->query("SELECT * FROM tbl_join_season WHERE user_id = '".$_SESSION['userLog']['id']."' AND status = 1");
    $row = $res->fetch_assoc();
    $seasonId = $row['season_id'];
    if($res->num_rows > 0) {
        $res2 = $conn->query("SELECT * FROM tbl_seasons WHERE id = '".$seasonId."' ");
        $row2 = $res2->fetch_assoc();
        $row2['joined'][] = $row;
        echo json_encode($row2,JSON_NUMERIC_CHECK);
    } else {
        echo false;
    }

}

if(isset($_GET['joinSeason'])) {
    if($conn->query("INSERT INTO `tbl_join_season` (`user_id`, `co_maker_id`, `season_id`, `shares`,
     `date_created`, `date_update`) 
     VALUES ('".$_SESSION['userLog']['id']."', NULL, '".$data->seasonId."', '".$data->share."', '".$dateToday."', '".$dateToday."');")) {
         echo true;
         $conn->query("INSERT INTO `tbl_userlogs` ( `user_id`, `message` ,`date_created`)
  VALUES ('".$_SESSION['userLog']['id']."', '"."member no ".$_SESSION['userLog']['id']." has been Join Season NO!".$data->seasonId."', '".$dateToday."');");
     } else {
         echo false;
     }
}
