<?php
include("../../env.php");
$env = new env();
$conn = $env->connectDb();
$data = json_decode(file_get_contents("php://input"));
$dateToday = date('Y-m-d H:i:s');
if(!isset($_SESSION)) {
    session_start();
  }

if(isset($_GET['getAllPrincipal'])) {  
  $arr = [];
  $arrId = [];
   $res = $conn->query("SELECT tbl_transactions.id, tbl_transactions.user_id, tbl_transactions.season_id,
    tbl_transactions.type, tbl_transactions.amount, tbl_transactions.image, tbl_transactions.date_created,
     tbl_transactions.date_update, tbl_transactions.status,tbl_users.first_name,tbl_users.sur_name,tbl_seasons.penalty,
     (SELECT CONCAT(a.first_name,' ',a.middle_name,' ',a.sur_name) FROM tbl_users as a WHERE a.id = tbl_loan.co1_user_id ) as co1name ,
     (SELECT CONCAT(a.first_name,' ',a.middle_name,' ',a.sur_name) FROM tbl_users as a WHERE a.id = tbl_loan.co2_user_id ) as co2name ,tbl_loan.amount_left
      FROM  tbl_transactions LEFT JOIN tbl_users ON tbl_transactions.user_id = tbl_users.id LEFT JOIN tbl_seasons
       ON tbl_transactions.season_id = tbl_seasons.id LEFT JOIN tbl_loan ON 
       tbl_transactions.user_id = tbl_loan.user_id WHERE tbl_transactions.type = 'LOAN' AND
        tbl_transactions.user_id = '".$_SESSION['userLog']['id']."' AND tbl_transactions.status = 1 ");
   while($row = $res->fetch_assoc()) {
    $b = false;
    for($i = 0 ; $i < count($arrId) ; $i ++) {
      if($arrId[$i] == $row['id']) {
        $b = true;
        break;
      } else {
        $b = false;
      }
    }
    array_push($arrId,$row['id']);


    if($b == false) {
      array_push($arr,$row);
    }

  }
 
  echo json_encode($arr,JSON_NUMERIC_CHECK);
}

if(isset($_GET['getAllComaker'])) {
  $arr = [];
  $arrId = [];
  $res2 = $conn->query("SELECT * FROM tbl_loan WHERE co1_user_id = '".$_SESSION['userLog']['id']."' ");
  while($row2 = $res2->fetch_assoc()) {
    $res = $conn->query("SELECT tbl_transactions.id, tbl_transactions.user_id, tbl_transactions.season_id,
    tbl_transactions.type, tbl_transactions.amount, tbl_transactions.image, tbl_transactions.date_created,
     tbl_transactions.date_update, tbl_transactions.status,tbl_users.first_name,tbl_users.sur_name,tbl_seasons.penalty,
     (SELECT CONCAT(a.first_name,' ',a.middle_name,' ',a.sur_name) FROM tbl_users as a WHERE a.id = tbl_loan.co1_user_id ) as co1name ,
     (SELECT CONCAT(a.first_name,' ',a.middle_name,' ',a.sur_name) FROM tbl_users as a WHERE a.id = tbl_loan.co2_user_id ) as co2name ,tbl_loan.amount_left
      FROM  tbl_transactions LEFT JOIN tbl_users ON tbl_transactions.user_id = tbl_users.id LEFT JOIN tbl_seasons
       ON tbl_transactions.season_id = tbl_seasons.id LEFT JOIN tbl_loan ON 
       tbl_transactions.user_id = tbl_loan.user_id WHERE tbl_transactions.type = 'LOAN' AND
        tbl_transactions.user_id = '".$row2['user_id']."' AND tbl_transactions.status = 1  ");
      while($row = $res->fetch_assoc()) {
        $b = false;
        for($i = 0 ; $i < count($arrId) ; $i ++) {
          if($arrId[$i] == $row['id']) {
            $b = true;
            break;
          } else {
            $b = false;
          }
        }
        array_push($arrId,$row['id']);
    
    
        if($b == false) {
          array_push($arr,$row);
        }
    
      }
    }
     
      echo json_encode($arr,JSON_NUMERIC_CHECK);
}

 if(isset($_GET['apply'])) {
    if($conn->query("INSERT INTO `tbl_loan` (`user_id`, `co1_user_id`, `co2_user_id`, `season_id`, `amount`, `amount_left`, `month`, `date_created`)
      VALUES ('".$_SESSION['userLog']['id']."', '".$data->comaker."', null, '".$data->seasonId."',
      '".$data->amount."','".$data->amount."', '".$data->month."', '".$dateToday."');")) {
        echo true;
        $res = $conn->query("SELECT id FROM tbl_loan ORDER BY date_created DESC LIMIT 1 ");
        $row = $res->fetch_assoc();
        
    $res2 = $conn->query("SELECT email , CONCAT(first_name,' ',middle_name, ' ',sur_name) as name
     FROM tbl_users WHERE id = '".$data->comaker."' ");
    $row2 = $res2->fetch_assoc();
    require '../../PHPMailer-master/PHPMailerAutoload.php';
    $email = $row2['email'];
    $name = $row2['name'];
    $body =  "<!DOCTYPE html>
    <html>
    <head>
      <title>Notification</title>

      <meta charset='UTF-8'>
        <meta http-equiv='X-UA-Compatible' content='IE=edge'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
    </head>
    <body style='padding: 0px; margin: 0px; font-weight: 600; font-family: sans-serif;' background-color: #f0f0f0;>

      <div style='width: 100%; background-color: #FB6C17; color: white; padding: 30px 0px'>
        <center><h1>CCS LOANING</h1></center>
      </div>

      <div style='width: 100%'>
        <center>
          <br><br>
          <h2 style='color: rgba(0, 0, 0, 0.7); font-size: 34px;'>Hi ".$name." You're are invited by ".$_SESSION['userLog']['first_name'].' '.$_SESSION['userLog']['sur_name']." to be hes comaker!,<h2><br>
          <br><br>
              <span style='color: rgba(0, 0, 0, 0.8);'>This email meant to ".$name."</span><br><br>
        </center>
      </div>

    </body>
    </html>";

   
    $mailto = $email;
    $mailSub = "NOTIFICATION";
    $mailMsg = $body;
    $mail = new PHPMailer();
    $mail ->IsSmtp();
    $mail ->SMTPDebug = 0;
    $mail ->SMTPAuth = true;
    $mail ->SMTPSecure = 'ssl';
    $mail ->Host = "smtp.gmail.com";
    $mail ->Port = 465; // or 587
    $mail ->IsHTML(true);
    $mail ->Username = "ccsloaning@gmail.com";
    $mail ->Password = "ccsloaning2k19";
    $mail ->SetFrom("ccsloaning@gmail.com");
    $mail ->Subject = $mailSub;
    $mail ->Body = $mailMsg;
    $mail ->AddAddress($mailto);
    if(!$mail->Send()) {
        // echo "Mail Not Sent";
    }
    else{
        // echo "Mail Sent";
    }

  


    if($conn->query("UPDATE `tbl_transactions` SET status = 0 WHERE id = '".$data->id."' ")) {
    } else {
    }
          $conn->query("INSERT INTO `tbl_notification` ( `loan_id`, `user_id`,`co1` ,  `message`,
           `date_created`, `date_update`) 
           VALUES ( '".$row['id']."','".$_SESSION['userLog']['id']."','".$data->comaker."', '".' REQUESTING FOR COMAKER FROM '.$_SESSION['userLog']['first_name'].' '.$_SESSION['userLog']['sur_name']."', '".$dateToday."', '".$dateToday."');");
    } else {
    }
 }

 if(isset($_GET['getComakers'])) {
     $ress = $conn->query("SELECT * FROM tbl_loan");
     if($ress->num_rows > 0) {
        $res = $conn->query("SELECT tbl_users.id,CONCAT(first_name,' ',middle_name,' ',sur_name) as name,
        (SELECT SUM(amount) FROM tbl_transactions WHERE tbl_transactions.user_id = tbl_users.id AND tbl_transactions.type = 'SAVINGS') as savings 
        FROM tbl_users 
           WHERE tbl_users.id <> '".$_SESSION['userLog']['id']."' AND tbl_users.role = 'member' AND tbl_users.status = 1  ");
     } else {
        $res = $conn->query("SELECT tbl_users.id,CONCAT(first_name,' ',middle_name,' ',sur_name) as name,
        (SELECT SUM(amount) FROM tbl_transactions WHERE tbl_transactions.user_id = tbl_users.id AND tbl_transactions.type = 'SAVINGS') as savings 
        FROM tbl_users 
           WHERE tbl_users.id <> '".$_SESSION['userLog']['id']."' AND tbl_users.role = 'member' AND tbl_users.status = 1  ");
     }
     
     $arr = [];
     while($row = $res->fetch_assoc()) {
         if($row['savings'] > 0 ) {
            array_push($arr,$row);
         } 
     }
     echo json_encode($arr,JSON_NUMERIC_CHECK);
 }

 if(isset($_GET['PayFor'])){
  $res = $conn->query("SELECT * FROM `tbl_loan` WHERE user_id = '".$_SESSION['userLog']['id']."' AND season_id='".$data->seasonId."' AND status = 1 AND treasurer_status IS NOT NULL");
  $arr = [];
  while($row = $res->fetch_assoc()) {
      array_push($arr,$row);
  }
  echo json_encode($arr,JSON_NUMERIC_CHECK);
 }
 if(isset($_GET['insert'])) {
    $imageName = $data->image;
    if($conn->query("INSERT INTO `tbl_transactions` (`user_id`, `season_id`, `type`, `amount`,`image`,
     `date_created`, `date_update`) 
     VALUES ('".$_SESSION['userLog']['id']."', '".$data->seasonId."', '".$data->type."', '".$data->amount."',
      '".$imageName."','".$dateToday."', '".$dateToday."');")) {
        echo true;
    } else {
        echo false;
    }
  }





?>