<?php
include("../../env.php");
$env = new env();
$conn = $env->connectDb();
$data = json_decode(file_get_contents("php://input"));
$id = $data->id;
if(!isset($_SESSION)) {
    session_start();
  }

$data = [];
$res = $conn->query("SELECT * FROM `tbl_seasons` WHERE id = '$id'");
while($row = $res->fetch_assoc()) { 
    $res1 = $conn->query("SELECT * FROM `tbl_transactions` WHERE user_id = '".$_SESSION['userLog']['id']."' AND season_id='$id' AND (status = 1 OR status IS NULL)");
    if($res1->num_rows > 0) {
        while($row1 = $res1->fetch_assoc()) { 
            $row['checkTblTransac'][] = $row1;
        }
    }else{
        $row['checkTblTransac'][] = '';
    }
    $paymentDate = date('Y-m-d');
    $weew = strtotime($paymentDate);

    // $row['paymentDate'] = date('Y-m-d', $weew);
    $row['DateBegin'] = strtotime($row['start_date']);
    $row['DateEnd'] = strtotime($row['end_date']);
    $datediff = $row['DateEnd'] - $row['DateBegin'];
    $row['totalDays'] = round($datediff / (60 * 60 * 24));
    if(($row['totalDays']>=1) && ($row['totalDays']<=30)){
        //first
        $for1Month = $row['totalDays'] /2;
        $ewqeq = ($for1Month * (60 * 60 * 24)) + $row['DateBegin'];
        $r123dsa = $weew - $ewqeq;
        $row['firstpaymentDateEnd'] = $r123dsa /(60 * 60 * 24);
        //second
        $ewqeq1= ($row['totalDays'] * (60 * 60 * 24)) + $row['DateBegin'];
        $r123dsa1 = $weew - $ewqeq1;
        $row['secpaymentDateEnd'] = $r123dsa1 /(60 * 60 * 24);
    }else if(($row['totalDays']>=31) && ($row['totalDays']<=60)){
        //first
        $for2Month = $row['totalDays'] /4;
        $ewqeq = ($for2Month * (60 * 60 * 24)) + $row['DateBegin'];
        $r123dsa = $weew - $ewqeq;
        $row['firstpaymentDateEnd'] = $r123dsa /(60 * 60 * 24);
        //second
        $ewqeq1 = (($for2Month+15) * (60 * 60 * 24)) + $row['DateBegin'];
        $r123dsa1 = $weew - $ewqeq1;
        $row['secpaymentDateEnd'] = $r123dsa1 /(60 * 60 * 24);
        //third
        $ewqeq2 = (($for2Month+30) * (60 * 60 * 24)) + $row['DateBegin'];
        $r123dsa2 = $weew - $ewqeq2;
        $row['thirdpaymentDateEnd'] = $r123dsa2 /(60 * 60 * 24);
        //four
        $ewqeq3= ($row['totalDays'] * (60 * 60 * 24)) + $row['DateBegin'];
        $r123dsa3 = $weew - $ewqeq3;
        $row['fourthpaymentDateEnd'] = $r123dsa3 /(60 * 60 * 24);
    }else if(($row['totalDays']>=61) && ($row['totalDays']<90)){
        //first
        $for3Month = $row['totalDays'] /6;
        $ewqeq = ($for3Month * (60 * 60 * 24)) + $row['DateBegin'];
        $r123dsa = $weew - $ewqeq;
        $row['firstpaymentDateEnd'] = $r123dsa /(60 * 60 * 24);
        //second
        $ewqeq1 = (($for3Month+15) * (60 * 60 * 24)) + $row['DateBegin'];
        $r123dsa1 = $weew - $ewqeq1;
        $row['secpaymentDateEnd'] = $r123dsa1 /(60 * 60 * 24);
        //third
        $ewqeq2 = (($for3Month+30) * (60 * 60 * 24)) + $row['DateBegin'];
        $r123dsa2 = $weew - $ewqeq2;
        $row['thirdpaymentDateEnd'] = $r123dsa2 /(60 * 60 * 24);
        //four
        $ewqeq3= (($for3Month+45) * (60 * 60 * 24)) + $row['DateBegin'];
        $r123dsa3 = $weew - $ewqeq3;
        $row['fourthpaymentDateEnd'] = $r123dsa3 /(60 * 60 * 24);
        //fifth
        $ewqeq4= (($for3Month+60) * (60 * 60 * 24)) + $row['DateBegin'];
        $r123dsa4 = $weew - $ewqeq4;
        $row['fifthpaymentDateEnd'] = $r123dsa4 /(60 * 60 * 24);
        //sixth
        $ewqeq5= ($row['totalDays'] * (60 * 60 * 24)) + $row['DateBegin'];
        $r123dsa5 = $weew - $ewqeq5;
        $row['sixthpaymentDateEnd'] = $r123dsa5 /(60 * 60 * 24);
    }
    
    $data = $row;
}
echo json_encode($data,JSON_NUMERIC_CHECK);
