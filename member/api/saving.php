<?php
include("../../env.php");
$env = new env();
$conn = $env->connectDb();
$data = json_decode(file_get_contents("php://input"));
$dateToday = date('Y-m-d H:i:s');

if(!isset($_SESSION)) {
    session_start();
  }

if(isset($_GET['getAll'])) {
   $res = $conn->query("SELECT tbl_transactions.id, tbl_transactions.user_id, tbl_transactions.season_id, tbl_transactions.type, tbl_transactions.amount, tbl_transactions.image, tbl_transactions.date_created, tbl_transactions.date_update, tbl_transactions.status,tbl_users.first_name,tbl_users.sur_name,tbl_seasons.penalty FROM  tbl_transactions LEFT JOIN tbl_users ON tbl_transactions.user_id = tbl_users.id LEFT JOIN tbl_seasons ON tbl_transactions.season_id = tbl_seasons.id WHERE tbl_transactions.type = 'SAVINGS' AND tbl_transactions.user_id = '".$_SESSION['userLog']['id']."' AND tbl_transactions.status = 1 ");
   $arr = [];
   while($row = $res->fetch_assoc()) {
       array_push($arr,$row);
   }
   echo json_encode($arr,JSON_NUMERIC_CHECK);
}

if(isset($_GET['insert'])) {

  $conn->query("INSERT INTO `tbl_userlogs` ( `user_id`, `message` ,`date_created`)
  VALUES ('".$_SESSION['userLog']['id']."', 'Pending Savings', '".$dateToday."');");

  $imageName = $data->image;
  if($conn->query("INSERT INTO `tbl_transactions` (`user_id`, `season_id`, `type`, `amount`,`image`,
   `date_created`, `date_update`, `remarks`) 
   VALUES ('".$_SESSION['userLog']['id']."', '".$data->seasonId."', '".$data->type."', '".$data->amount."',
    '".$imageName."','".$dateToday."', '".$dateToday."', '".$data->remarks."');")) {
      echo true;
  } else {
      echo false;
  }
}



?>