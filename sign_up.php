<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>CCS Saving and Loaning | Login</title>

    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="assets/plugins/iconic/css/material-design-iconic-font.min.css">
    <!-- bootstrap -->
	<link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- style -->
    <link rel="stylesheet" href="assets/css/pages/extra_pages.css">
	<!-- favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" /> 
</head>
<body>
    <div class="limiter">
		<div class="container-login100 page-background">
			<div class="wrap-login100">
				<form id="frm_register" class="login100-form validate-form">
					<center>
						<img src="assets/img/logo.png">
					</center>
					<span class="login100-form-title p-b-34 p-t-27">
						Registration
					</span>
					<div class="row">
					<div class="col-lg-12 p-t-20">
					<div class="wrap-input100 validate-input" data-validate = "Enter Firstname">
						<input class="input100" pattern=".{2,}" type="text" onkeydown="return alphaOnly(event);"
    onblur="if (this.value == '') {this.value = 'Type Letters Only';}"
    onfocus="if (this.value == 'Type Letters Only') {this.value = '';}" name="first_name" placeholder="FirstName">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>
					<div class="wrap-input100 validate-input" data-validate = "Enter Middlename">
						<input class="input100" type="text" pattern=".{2,}" onkeydown="return alphaOnly(event);"
    onblur="if (this.value == '') {this.value = 'Type Letters Only';}"
    onfocus="if (this.value == 'Type Letters Only') {this.value = '';}" name="middle_name" placeholder="Middlename">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>
					<div class="wrap-input100 validate-input" data-validate = "Enter Surname">
						<input class="input100" type="text" pattern=".{2,}" onkeypress="return (event.charCode > 64 && 
event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" name="sur_name" placeholder="Surname">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>
					<div class="wrap-input100 validate-input" data-validate = "Enter Mobile Number">
						<input class="input100" type="number" name="mobile_number" placeholder="Mobile Number" pattern = "-?[0-9]*(\.[0-9]+)?">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>
					<div class="wrap-input100 validate-input" data-validate = "Enter email">
						<input class="input100" type="text" name="email" placeholder="Email">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>
					<div class="wrap-input100 validate-input" data-validate = "Enter Employee Id">
						<input class="input100" type="text" pattern = "-?[0-9]*(\.[0-9]+)?" name="id" placeholder="Employee Id">
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
					</div>
					</div>
					<div class="col-lg-12 p-t-20">
					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>
					</div>
					<div class="col-lg-12 p-t-20">
					<div class="wrap-input100 validate-input" data-validate="Enter password again">
						<input class="input100" type="password" name="pass2" placeholder="Confirm password">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>
					</div>
					</div>
					<div class="container-login100-form-btn">
						<button id="btn_submit" type="button" class="login100-form-btn" onclick="register()">
							Sign Up
						</button>
					</div>
					<div class="text-center p-t-90">
						<a class="txt1" href="login.php">
							You already have a membership?
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
    <!-- start js include path -->
    <script src="assets/plugins/jquery/jquery.min.js" ></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- bootstrap -->
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <script src="assets/js/pages/extra_pages/login.js" ></script>
    <!-- end js include path -->
</body>

</html>
<script>
function alphaOnly(event) {
  var key = event.keyCode;
  return ((key >= 65 && key <= 90) || key == 8);
};
	
	
function register() {
	
	var values = $("#frm_register").serialize();
	console.log(values);
		$.ajax({
			url: 'controllers/handlers/users.php?insert&role=member',
			type: "POST",
        	data: values,
		}).done(function(response){
			if(response == "Success!") {
				swal({
				title:response,
				icon: "success",
				message:"Pending request",
				dangerMode: false,
				}).then((willDelete) => {
					window.location.href = 	"login.php";
				});
			}else if(response == "Fname"){
				swal("Write First Name",{
					icon: "warning",
					dangerMode: true,
				});
			}else if(response == "Mname"){
				swal("Write Middle Name",{
					icon: "warning",
					dangerMode: true,
				});
			}else if(response == "Lname"){
				swal("Write Last Name",{
					icon: "warning",
					dangerMode: true,
				});
			}else if(response == "Email"){
				swal("Write Email",{
					icon: "warning",
					dangerMode: true,
				});
			}else if(response == "Id"){
				swal("Write ID",{
					icon: "warning",
					dangerMode: true,
				});
			}else if(response == "Num"){
				swal("Write Phone Number",{
					icon: "warning",
					dangerMode: true,
				});
			}else if(response == "Pass1"){
				swal("Write Password",{
					icon: "warning",
					dangerMode: true,
				});
			}else if(response == "Pass2"){
				swal("Write Confirm Password",{
					icon: "warning",
					dangerMode: true,
				});
			}else if(response == "Not"){
				swal("Password And Confirm Password is not Equal",{
					icon: "warning",
					dangerMode: true,
				});
			}else{
				swal({
				title:response,
				icon: "warning",
				dangerMode: true,
				});
			}
			
		});
	}


	
</script>