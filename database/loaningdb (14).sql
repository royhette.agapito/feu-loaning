-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 28, 2019 at 02:38 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `loaningdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_announcement`
--

CREATE TABLE `tbl_announcement` (
  `id` int(50) NOT NULL,
  `image` longtext,
  `title` varchar(100) DEFAULT NULL,
  `descriptions` longtext,
  `date_created` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `date_expired` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_join_season`
--

CREATE TABLE `tbl_join_season` (
  `id` int(100) NOT NULL,
  `user_id` varchar(50) DEFAULT NULL,
  `co_maker_id` varchar(50) DEFAULT NULL,
  `season_id` varchar(50) DEFAULT NULL,
  `shares` int(5) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `remarks` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_join_season`
--

INSERT INTO `tbl_join_season` (`id`, `user_id`, `co_maker_id`, `season_id`, `shares`, `date_created`, `date_update`, `status`, `remarks`) VALUES
(1, '2015', NULL, '1', 5, '2019-03-26 19:12:05', '2019-03-26 19:12:05', 0, NULL),
(2, '201510375', NULL, '1', 5, '2019-03-26 19:12:33', '2019-03-26 19:12:33', NULL, NULL),
(4, '2015', NULL, '2', 5, '2019-03-26 20:23:49', '2019-03-26 20:23:49', 0, NULL),
(5, '201510375', NULL, '2', 5, '2019-03-26 20:24:57', '2019-03-26 20:24:57', 0, NULL),
(6, '201510375', NULL, '3', 5, '2019-03-26 20:29:45', '2019-03-26 20:29:45', 1, NULL),
(7, '2015', NULL, '3', 5, '2019-03-26 20:30:11', '2019-03-26 20:30:11', 1, NULL),
(8, '2016', NULL, '3', 5, '2019-03-26 21:16:29', '2019-03-26 21:16:29', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_loan`
--

CREATE TABLE `tbl_loan` (
  `id` int(11) NOT NULL,
  `user_id` varchar(50) DEFAULT NULL,
  `co1_user_id` varchar(50) DEFAULT NULL,
  `co2_user_id` varchar(50) DEFAULT NULL,
  `season_id` varchar(50) DEFAULT NULL,
  `amount` double(11,2) DEFAULT NULL,
  `month` varchar(50) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `treasurer_status` tinyint(4) DEFAULT NULL,
  `remarks` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_loan`
--

INSERT INTO `tbl_loan` (`id`, `user_id`, `co1_user_id`, `co2_user_id`, `season_id`, `amount`, `month`, `date_created`, `date_update`, `status`, `treasurer_status`, `remarks`) VALUES
(4, '2015', '201510375', NULL, '3', 500.00, '2', '2019-03-26 21:25:28', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notification`
--

CREATE TABLE `tbl_notification` (
  `id` int(50) NOT NULL,
  `loan_id` varchar(50) DEFAULT NULL,
  `user_id` int(50) DEFAULT NULL,
  `co1` varchar(50) DEFAULT NULL,
  `co2` varchar(50) DEFAULT NULL,
  `message` longtext,
  `date_created` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1',
  `remarks` varchar(50) DEFAULT NULL,
  `remarks1` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notification`
--

INSERT INTO `tbl_notification` (`id`, `loan_id`, `user_id`, `co1`, `co2`, `message`, `date_created`, `date_update`, `status`, `remarks`, `remarks1`) VALUES
(1, '1231', 101, '2015', NULL, 'HIRafael BunyiNew season is available! season number1', '2019-03-26 19:11:25', '2019-03-26 19:11:25', 1, 'read', 1),
(2, '1231', 101, '201510375', NULL, 'HICarlos ChingNew season is available! season number1', '2019-03-26 19:11:25', '2019-03-26 19:11:25', 1, 'read', 0),
(3, '1', 102, '2015', NULL, 'Your  has been Paid!', '2019-03-26 19:15:11', '2019-03-26 19:15:11', 1, 'read', 1),
(4, '1231', 101, '2015', NULL, 'HIRafael BunyiNew season is available! season number2', '2019-03-26 19:53:06', '2019-03-26 19:53:06', 1, 'read', 1),
(5, '1231', 101, '201510375', NULL, 'HICarlos ChingNew season is available! season number2', '2019-03-26 19:53:06', '2019-03-26 19:53:06', 1, 'read', 0),
(6, '1231', 101, '2016', NULL, 'HIRafael RoxasNew season is available! season number2', '2019-03-26 19:53:06', '2019-03-26 19:53:06', 1, 'read', 1),
(7, '1231', 101, 'qrqwrq', NULL, 'HIMark BunyiNew season is available! season number2', '2019-03-26 19:53:06', '2019-03-26 19:53:06', 1, 'read', 1),
(8, '1231', 101, '2015', NULL, 'HIRafael BunyiNew season is available! season number3', '2019-03-26 20:29:01', '2019-03-26 20:29:01', 1, 'read', 1),
(9, '1231', 101, '201510375', NULL, 'HICarlos ChingNew season is available! season number3', '2019-03-26 20:29:01', '2019-03-26 20:29:01', 1, 'read', 0),
(10, '1231', 101, '2016', NULL, 'HIRafael RoxasNew season is available! season number3', '2019-03-26 20:29:01', '2019-03-26 20:29:01', 1, 'read', 1),
(11, '1231', 101, 'qrqwrq', NULL, 'HIMark BunyiNew season is available! season number3', '2019-03-26 20:29:01', '2019-03-26 20:29:01', 1, 'read', 1),
(12, '3', 102, '2015', NULL, 'Your First Payment has been Paid!', '2019-03-26 20:42:27', '2019-03-26 20:42:27', 1, 'read', 1),
(13, '3', 102, '201510375', NULL, 'HICarlos Tuazon Chingpayment has been ACCEPTED!', '2019-03-26 20:59:39', '2019-03-26 20:59:39', 1, 'read', 0),
(14, '3', 102, '201510375', NULL, 'HICarlos Tuazon Chingpayment has been ACCEPTED!', '2019-03-26 20:59:44', '2019-03-26 20:59:44', 1, 'read', 0),
(15, '3', 102, '201510375', NULL, 'HICarlos Tuazon Chingpayment has been ACCEPTED!', '2019-03-26 20:59:44', '2019-03-26 20:59:44', 1, 'read', 0),
(16, '4', 2015, '201510375', NULL, ' REQUESTING FOR COMAKER FROM Rafael Bunyi', '2019-03-26 21:25:28', '2019-03-26 21:25:28', 0, NULL, 0),
(17, '3', 102, '2015', NULL, 'Your Second Payment has been Paid!', '2019-03-28 00:04:37', '2019-03-28 00:04:37', 1, 'read', 1),
(18, '3', 102, '2015', NULL, 'Your Third Payment has been Paid!', '2019-03-28 00:04:50', '2019-03-28 00:04:50', 1, 'read', 0),
(19, '3', 102, '2015', NULL, 'Your Fourth Payment has been Paid!', '2019-03-28 00:04:59', '2019-03-28 00:04:59', 1, 'read', 0),
(20, '3', 102, '2016', NULL, 'Your First Payment has been Paid!', '2019-03-28 00:05:09', '2019-03-28 00:05:09', 1, 'read', 1),
(21, '3', 102, '2016', NULL, 'Your Second Payment has been Paid!', '2019-03-28 00:05:21', '2019-03-28 00:05:21', 1, 'read', 1),
(22, '4', 201510375, '2015', NULL, ' REQUEST ACCEPTED BY Carlos Ching', '2019-03-28 13:37:32', '2019-03-28 13:37:32', 1, 'read', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_seasons`
--

CREATE TABLE `tbl_seasons` (
  `id` int(11) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `first_payment` double DEFAULT NULL,
  `second_payment` double DEFAULT NULL,
  `amount_per_share` double DEFAULT NULL,
  `max_share` double DEFAULT NULL,
  `penalty` double DEFAULT NULL,
  `interest` double DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `remarks` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_seasons`
--

INSERT INTO `tbl_seasons` (`id`, `start_date`, `end_date`, `first_payment`, `second_payment`, `amount_per_share`, `max_share`, `penalty`, `interest`, `date_created`, `date_update`, `status`, `remarks`) VALUES
(1, '2019-03-01', '2019-06-30', 5, 20, 100, NULL, 1, 2, '2019-03-26 19:11:25', '2019-03-26 19:11:25', 0, NULL),
(2, '2019-03-01', '2019-05-31', 5, 20, 100, NULL, 1, 2, '2019-03-26 19:53:06', '2019-03-26 19:53:06', 0, NULL),
(3, '2019-03-01', '2019-05-29', 5, 20, 100, NULL, 1, 2, '2019-03-26 20:29:01', '2019-03-26 20:29:01', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transactions`
--

CREATE TABLE `tbl_transactions` (
  `id` int(11) NOT NULL,
  `user_id` varchar(50) DEFAULT NULL,
  `season_id` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `image` longtext,
  `date_created` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `remarks` varchar(50) DEFAULT NULL,
  `remarks1` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transactions`
--

INSERT INTO `tbl_transactions` (`id`, `user_id`, `season_id`, `type`, `amount`, `image`, `date_created`, `date_update`, `status`, `remarks`, `remarks1`) VALUES
(1, '2015', '1', 'SAVINGS', 0, NULL, '2019-03-26 19:15:11', '2019-03-26 19:15:11', 1, '', 'Tresurer Paid'),
(2, '2015', '3', 'SAVINGS', 510, NULL, '2019-03-26 20:42:27', '2019-03-26 20:42:27', 1, 'First Payment', 'Tresurer Paid'),
(3, '201510375', '3', 'SAVINGS', 510, '1553605135.jpg', '2019-03-26 20:58:56', '2019-03-26 20:58:56', 1, 'First Payment', NULL),
(4, '2015', '3', 'SAVINGS', 500, NULL, '2019-03-28 00:04:37', '2019-03-28 00:04:37', 1, 'Second Payment', 'Tresurer Paid'),
(5, '2015', '3', 'SAVINGS', 500, NULL, '2019-03-28 00:04:50', '2019-03-28 00:04:50', 1, 'Third Payment', 'Tresurer Paid'),
(6, '2015', '3', 'SAVINGS', 500, NULL, '2019-03-28 00:04:59', '2019-03-28 00:04:59', 1, 'Fourth Payment', 'Tresurer Paid'),
(7, '2016', '3', 'SAVINGS', 510, NULL, '2019-03-28 00:05:09', '2019-03-28 00:05:09', 1, 'First Payment', 'Tresurer Paid'),
(8, '2016', '3', 'SAVINGS', 500, NULL, '2019-03-28 00:05:21', '2019-03-28 00:05:21', 1, 'Second Payment', 'Tresurer Paid');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_userlogs`
--

CREATE TABLE `tbl_userlogs` (
  `id` int(11) NOT NULL,
  `user_id` varchar(50) DEFAULT NULL,
  `message` varchar(50) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_userlogs`
--

INSERT INTO `tbl_userlogs` (`id`, `user_id`, `message`, `date_created`, `status`) VALUES
(1, '', 'Login', '2019-03-26 19:04:15', 1),
(2, '', 'Login', '2019-03-26 19:05:59', 1),
(3, '2015', 'New registered user', '2019-03-26 19:08:21', 1),
(4, '101', 'Activated Employee 2015', '2019-03-26 19:08:35', 1),
(5, '', 'Login', '2019-03-26 19:08:48', 1),
(6, '201510375', 'New registered user', '2019-03-26 19:09:09', 1),
(7, '101', 'Activated Employee 201510375', '2019-03-26 19:09:31', 1),
(8, '', 'Login', '2019-03-26 19:10:08', 1),
(9, '101', 'Insert Season 1', '2019-03-26 19:11:25', 1),
(10, '', 'Login', '2019-03-26 19:12:05', 1),
(11, '', 'Login', '2019-03-26 19:18:06', 1),
(12, 'qrqwrq', 'New registered user', '2019-03-26 19:23:37', 1),
(13, '101', 'ActDeactivateivate Season 1', '2019-03-26 19:47:06', 1),
(14, '', 'Login', '2019-03-26 19:48:34', 1),
(15, '', 'Login', '2019-03-26 19:50:46', 1),
(16, '2016', 'New registered user', '2019-03-26 19:51:41', 1),
(17, '101', 'Activated Employee 2016', '2019-03-26 19:52:02', 1),
(18, '', 'Login', '2019-03-26 19:52:12', 1),
(19, '101', 'Insert Season 2', '2019-03-26 19:53:06', 1),
(20, '2016', 'Login', '2019-03-26 19:54:05', 1),
(21, '', 'Login', '2019-03-26 20:13:24', 1),
(22, '', 'Login', '2019-03-26 20:15:45', 1),
(23, '', 'Login', '2019-03-26 20:16:46', 1),
(24, '101', 'ActDeactivateivate Season 1', '2019-03-26 20:16:56', 1),
(25, '', 'Login', '2019-03-26 20:19:11', 1),
(26, '101', 'ActDeactivateivate Season 1', '2019-03-26 20:20:59', 1),
(27, '101', 'ActDeactivateivate Season 2', '2019-03-26 20:28:32', 1),
(28, '101', 'Insert Season 3', '2019-03-26 20:29:01', 1),
(29, '201510375', 'Pending Savings', '2019-03-26 20:58:56', 1),
(30, '3', 'Approved payment', '2019-03-26 20:59:39', 1),
(31, '3', 'Approved payment', '2019-03-26 20:59:44', 1),
(32, '3', 'Approved payment', '2019-03-26 20:59:44', 1),
(33, '', 'Login', '2019-03-26 21:06:20', 1),
(34, '', 'Login', '2019-03-26 21:10:46', 1),
(35, '2016', 'Login', '2019-03-26 21:16:21', 1),
(36, '2016', 'member no 2016 has been Join Season NO!3', '2019-03-26 21:16:29', 1),
(37, '', 'Login', '2019-03-26 21:18:10', 1),
(38, '', 'Login', '2019-03-26 21:19:10', 1),
(39, '', 'Login', '2019-03-27 23:55:44', 1),
(40, '', 'Login', '2019-03-27 23:57:05', 1),
(41, '', 'Login', '2019-03-27 23:59:57', 1),
(42, '', 'Login', '2019-03-28 00:03:43', 1),
(43, '102', 'Login', '2019-03-28 00:05:33', 1),
(44, '', 'Login', '2019-03-28 00:06:11', 1),
(45, '', 'Login', '2019-03-28 13:09:35', 1),
(46, '', 'Login', '2019-03-28 13:10:08', 1),
(47, '', 'Login', '2019-03-28 13:13:56', 1),
(48, '', 'Login', '2019-03-28 13:17:30', 1),
(49, '', 'Login', '2019-03-28 13:19:57', 1),
(50, '', 'Login', '2019-03-28 13:20:32', 1),
(51, '', 'Login', '2019-03-28 13:21:33', 1),
(52, '', 'Login', '2019-03-28 13:22:25', 1),
(53, '', 'Login', '2019-03-28 13:24:08', 1),
(54, '', 'Login', '2019-03-28 13:25:53', 1),
(55, '', 'Login', '2019-03-28 13:26:28', 1),
(56, '', 'Login', '2019-03-28 13:26:50', 1),
(57, '', 'Login', '2019-03-28 13:27:32', 1),
(58, '', 'Login', '2019-03-28 13:28:51', 1),
(59, '', 'Login', '2019-03-28 13:31:03', 1),
(60, '', 'Login', '2019-03-28 14:01:47', 1),
(61, '', 'Login', '2019-03-28 14:19:33', 1),
(62, '', 'Login', '2019-03-28 16:04:07', 1),
(63, '', 'Login', '2019-03-28 16:30:16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` varchar(50) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `sur_name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `image` longtext,
  `password` varchar(50) DEFAULT NULL,
  `mobile_number` varchar(50) DEFAULT NULL,
  `shares` double DEFAULT NULL,
  `role` varchar(25) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_update` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `remarks` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `first_name`, `middle_name`, `sur_name`, `email`, `image`, `password`, `mobile_number`, `shares`, `role`, `date_created`, `date_update`, `status`, `remarks`) VALUES
('101', 'admin', 'adminm', 'admin', 'admin@gmail.com', 'a32a67759ffe885408a72aae2b02b3ef.png', 'admin', '0909090909', 0, 'admin', '2019-03-03 07:54:46', '2019-03-03 07:54:46', 1, NULL),
('102', 'treasurer', 'adminm', 'treasurer', 'admin@gmail.com', '913f18bfb65047ab86f4aff7de88d713.png', 'treasurer', '0909090909', 0, 'treasurer', '2019-03-03 07:54:46', '2019-03-03 07:54:46', 1, NULL),
('2015', 'Rafael', 'Man', 'Bunyi', 'commanderpogi15@gmail.com', NULL, '123', '09267587608', 0, 'member', '2019-03-26 19:08:21', '2019-03-26 19:08:21', 1, NULL),
('201510375', 'Carlos', 'Tuazon', 'Ching', 'carlosmiguelching@gmail.com', NULL, '123', '09165513648', 0, 'member', '2019-03-26 19:09:09', '2019-03-26 19:09:09', 1, NULL),
('2016', 'Rafael', 'Mar', 'Roxas', 'commanderpog@gmail.com', NULL, '123', '09267587609', 0, 'member', '2019-03-26 19:51:41', '2019-03-26 19:51:41', 1, NULL),
('qrqwrq', 'Mark', 'Mar', 'Bunyi', 'commanderpogi@gmail.com', NULL, '123', '09267587609', 0, 'member', '2019-03-26 19:23:37', '2019-03-26 19:23:37', 0, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_announcement`
--
ALTER TABLE `tbl_announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_join_season`
--
ALTER TABLE `tbl_join_season`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_loan`
--
ALTER TABLE `tbl_loan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_seasons`
--
ALTER TABLE `tbl_seasons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_transactions`
--
ALTER TABLE `tbl_transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_userlogs`
--
ALTER TABLE `tbl_userlogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_announcement`
--
ALTER TABLE `tbl_announcement`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_join_season`
--
ALTER TABLE `tbl_join_season`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_loan`
--
ALTER TABLE `tbl_loan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tbl_transactions`
--
ALTER TABLE `tbl_transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_userlogs`
--
ALTER TABLE `tbl_userlogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
