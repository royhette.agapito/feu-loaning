        <div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy; CCS Savings and Loaning System By
            <a href="mailto:redstartheme@gmail.com" target="_top" class="makerCss">FEU Tech</a>
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->

    <script src="../assets/plugins/jquery/jquery.min.js" ></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="jspdf.debug.js"></script>

    <script src="../assets/plugins/popper/popper.min.js" ></script>
    <script src="../assets/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
    <script src="../assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../assets/plugins/jquery-ui/jquery-ui.min.js" ></script>
    <script src="../assets/plugins/counterup/jquery.waypoints.min.js" ></script>
    <script src="../assets/plugins/counterup/jquery.counterup.min.js" ></script>
    <!-- bootstrap -->
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- dropzone -->
    <script src="../assets/plugins/dropzone/dropzone.js" ></script>
    <script src="../assets/plugins/dropzone/dropzone-call.js" ></script>
    <!-- morris chart -->
    <script src="../assets/plugins/morris/morris.min.js" ></script>
    <script src="../assets/plugins/morris/raphael-min.js" ></script>
    <script src="../assets/js/pages/chart/morris/morris_home_data.js" ></script>
    <!-- calendar -->
    <script src="../assets/plugins/moment/moment.min.js" ></script>
    <script src="../assets/plugins/fullcalendar/fullcalendar.min.js" ></script>
    <script src="../assets/js/pages/calendar/calendar.min.js" ></script>
    <!-- data tables -->
    <script src="../assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
    <script src="../assets/js/pages/table/table_data.js" ></script>
    <!-- Common js-->
    <script src="../assets/js/app.js" ></script>
    <script src="../assets/js/layout.js" ></script>
    <script src="../assets/js/theme-color.js" ></script>
    <!-- Material -->
    <script src="../assets/plugins/material/material.min.js"></script>
    <script src="../assets/js/pages/material_select/getmdl-select.js" ></script>
    <script  src="../assets/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
    <script  src="../assets/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
    <script  src="../assets/plugins/material-datetimepicker/datetimepicker.js"></script>
    <!-- animation -->
    <script src="../assets/js/pages/ui/animations.js" ></script>
    <!-- end js include path -->
  </body>


</html>

<script>
  function print(divID) {
    var divToPrint=document.getElementById(divID);
        newWin= window.open("");
        newWin.document.write(divToPrint.outerHTML);
        newWin.print();
        newWin.close();

    }



    function printpdf(divId) {
            var pdf = new jsPDF('p', 'pt', 'letter');
            // source can be HTML-formatted string, or a reference
            // to an actual DOM element from which the text will be scraped.
            source = $('#'+divId)[0];

            // we support special element handlers. Register them with jQuery-style 
            // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
            // There is no support for any other type of selectors 
            // (class, of compound) at this time.
            specialElementHandlers = {
                // element with id of "bypass" - jQuery style selector
                '#bypassme': function(element, renderer) {
                    // true = "handled elsewhere, bypass text extraction"
                    return true
                }
            };
            margins = {
                top: 80,
                bottom: 60,
                left: 40,
                width: 522
            };
            // all coords and widths are in jsPDF instance's declared units
            // 'inches' in this case
            pdf.fromHTML(
                    source, // HTML string or DOM elem ref.
                    margins.left, // x coord
                    margins.top, {// y coord
                        'width': margins.width, // max width of content on PDF
                        'elementHandlers': specialElementHandlers
                    },
            function(dispose) {
                // dispose: object with X, Y of the last line add to the PDF 
                //          this allow the insertion of new lines after html
                pdf.save('Test.pdf');
            }
            , margins);
        }



</script>