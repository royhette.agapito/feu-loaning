<?php include 'header.php'; ?>
<style type="text/css">
  .place { width: 12px; height: 12px; background-color: green;  border-radius: 50%;}
  .place.green { background-color: red; }
</style>



<!-- start page content -->
<div class="page-content-wrapper">
  <div class="page-content">
    <div class="page-bar">
      <div class="page-title-breadcrumb">
        <div class=" pull-left">
          <div class="page-title">Users</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
          </li>
          <li class="active">Users</li>
        </ol>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">  
        <div class="profile-tab-box">
          <div class="p-l-20">
            <ul class="nav ">
              <li class="nav-item tab-all"><a
                class="nav-link active show" href="#tab1" data-toggle="tab">Member</a></li>
              <li class="nav-item tab-all p-l-20"><a class="nav-link"
                href="#tab2" data-toggle="tab">Staff</a></li>
            </ul>
          </div>
        </div>
        <!-- Tab panes -->
        <div class="tab-content">
          <div class="tab-pane active fontawesome-demo" id="tab1">
            <div class="card card-topline-green">
              <div class="card-head">
                <header>List of Members</header>
                <div class="tools">
                  <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>

                </div>
              </div>
              <div class="card-body ">
                <div class="row p-b-20">
                  <div class="col-md-6 col-sm-6 col-6">
                    <div class="btn-group">
                      <button id="addRow1" class="btn btn-info" onclick="window.location.href='add_member.php'">
                      Add New Member<i class="fa fa-plus"></i>
                      </button>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-6">
                    <div class="btn-group pull-right">
                      <button class="btn deepPink-bgcolor  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                      <i class="fa fa-angle-down"></i>
                      </button>
                      <ul class="dropdown-menu pull-right">
                        <li>
                          <a href="javascript:;">
                          <i class="fa fa-print"></i> Print </a>
                        </li>
                        <li>
                          <a href="javascript:;">
                          <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                        </li>
                        <li>
                          <a href="javascript:;">
                          <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                
                <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="tbl_users">
                    <thead>
                      <tr>
                        <th> Status </th>
                        <th> Employee ID </th>
                        <th> Name </th>
                        <th> Email </th>
                        <th> Joined </th> 
                        <th> Actions </th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    $res = $conn->query("SELECT * FROM tbl_users WHERE role = 'member'  ");
                    while($row = $res->fetch_assoc()){ 
                      $status = ($row['status'] == 0)?"green":"";
                      $id = $row['id'];
                    ?>

                    
                      <!-- Modal Member -->
                      <div class="modal fade" id="<?=$id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content card-box">
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-sm-12">
                                  
                                  <div class="card-head">
                                    <header>Basic Information</header>
                                  </div>
                                  <form id="<?='frm'.$id?>">
                                  <div class="card-body row">
                                    <!-- <div class="col-lg-6 p-t-20">
                                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                        <input class = "mdl-textfield__input" type = "text" id = "txtEmpid" value="<?=$row['id']?>" readonly>
                                        <label class = "mdl-textfield__label">Employee ID</label>
                                      </div>
                                    </div> -->
                                    <div class="col-lg-6 p-t-20">
                                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                        <input class = "mdl-textfield__input" type = "number" id = "txtEmail" name="mobile_number" value="<?=$row['mobile_number']?>"  pattern= "[0-9]">
                                        <label class = "mdl-textfield__label">Mobile Number</label>
                                      </div>
                                    </div>
                                    <div class="col-lg-6 p-t-20">
                                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                        <input class = "mdl-textfield__input" type = "email" id = "txtEmail" value="<?=$row['email']?>" name="email">
                                        <label class = "mdl-textfield__label">Email</label>
                                      </div>
                                    </div>
                                    <div class="col-lg-6 p-t-20">
                                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                        <input class = "mdl-textfield__input"  minlength="2" pattern="[A-Za-z]{3}"  type = "text" id = "txtFirstName" value="<?=$row['first_name']?>" name="first_name">
                                        <label class = "mdl-textfield__label">First Name</label>
                                      </div>
                                    </div>
                                    <div class="col-lg-6 p-t-20">
                                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                        <input class = "mdl-textfield__input"  minlength="2" pattern="[A-Za-z]{3}"  type = "text" id = "txtLasttName" value="<?=$row['sur_name']?>" name="sur_name">
                                        <label class = "mdl-textfield__label" >Sur Name</label>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                            <button type="button"  onclick="update('<?=$id?>','<?=$id?>');" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Save changes</button>
                            </form>
                              <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>



                      <tr class="odd gradeX">
                        <td>
                          <center><div class="place <?=$status?>"></div></center>
                        </td>
                        <td>
                        <?=$row['id']?> 
                        </td>
                        <td>
                        <?=$row['first_name']." ".$row['middle_name']." ".$row['sur_name']?> 
                        </td>
                        <td>
                        <?=$row['email']?> 
                        </td>
                        <td> 
                        <?=$row['date_created']?> 
                        </td>
                        <td class="valigntop">
                          <button class="btn btn-primary btn-xs tooltips" data-placement="top" data-original-title="Edit" data-toggle="modal" data-target="#<?=$id?>">
                            <i class="fa fa-pencil">&nbsp;</i>
                            Edit
                          </button>
                          <?php if($status == "green"){ ?>
                          <button onclick="activate('<?=$id?>');" class="btn btn-primary btn-xs tooltips"  data-placement="top" data-original-title="Activate">
                            <i class="fa fa-check">&nbsp;</i>
                            Activate
                          </button>
                          <?php } else { ?>
                            <button onclick="deactivate('<?=$id?>');" class="btn btn-primary btn-xs tooltips" style="color:red;" data-placement="top" data-original-title="Deactivate">
                            <i class="fa fa-times">&nbsp;</i>
                            Deactivate
                          </button>
                          <?php } ?>
                        </td>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane" id="tab2">
            <div class="card card-topline-green">
              <div class="card-head">
                <header>List of Staffs</header>
                <div class="tools">
                  <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                </div>
              </div>
              <div class="card-body ">
                <div class="row p-b-20">
                  <div class="col-md-6 col-sm-6 col-6">
                    <div class="btn-group mt-3">
                      <button id="addRow1" class="btn btn-info" onclick="window.location.href='add_treasurer.php'">
                      Add New Treasurer<i class="fa fa-plus"></i>
                      </button>
                    </div>
                    <div class="btn-group mt-3">
                      <button id="addRow1" class="btn btn-info" onclick="window.location.href='add_admin.php'">
                      Add New Admin<i class="fa fa-plus"></i>
                      </button>
                    </div>
                  </div>
                  <div class="col-md-6 col-sm-6 col-6 mt-3">
                    <div class="btn-group pull-right">
                      <button class="btn deepPink-bgcolor  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                      <i class="fa fa-angle-down"></i>
                      </button>
                      <ul class="dropdown-menu pull-right">
                        <li>
                          <a href="javascript:;">
                          <i class="fa fa-print"></i> Print </a>
                        </li>
                        <li>
                          <a href="javascript:;">
                          <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                        </li>
                        <li>
                          <a href="javascript:;">
                          <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                
                <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                    <thead>
                      <tr>
                        <th> Status </th>
                        <th> Employee ID </th>
                        <th> Name </th>
                        <th> Email </th>
                        <th> Joined </th> 
                        <th> Actions </th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                    $res = $conn->query("SELECT * FROM tbl_users WHERE role <> 'member'");
                    while($row = $res->fetch_assoc()){ 
                      $status = ($row['status'] == 0)?"green":"";
                      $id = $row['id'];
                    ?>
                    
                      <!-- Modal Member -->
                      <div class="modal fade" id="<?=$id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content card-box">
                            <div class="modal-body">
                              <div class="row">
                                <div class="col-sm-12">
                                  
                                  <div class="card-head">
                                    <header>Basic Information</header>
                                  </div>
                                  <form id="<?='frm'.$id?>">
                                  <div class="card-body row">
                                    <!-- <div class="col-lg-6 p-t-20">
                                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                        <input class = "mdl-textfield__input" type = "text" id = "txtEmpid" value="<?=$row['id']?>" readonly>
                                        <label class = "mdl-textfield__label">Employee ID</label>
                                      </div>
                                    </div> -->
                                    <div class="col-lg-6 p-t-20">
                                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                        <input class = "mdl-textfield__input" type = "number" id = "txtEmail" name="mobile_number" value="<?=$row['mobile_number']?>"  pattern= "[0-9]">
                                        <label class = "mdl-textfield__label">Mobile Number</label>
                                      </div>
                                    </div>
                                    <div class="col-lg-6 p-t-20">
                                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                        <input class = "mdl-textfield__input" type = "email" id = "txtEmail" value="<?=$row['email']?>" name="email">
                                        <label class = "mdl-textfield__label">Email</label>
                                      </div>
                                    </div>
                                    <div class="col-lg-6 p-t-20">
                                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                        <input class = "mdl-textfield__input"  minlength="2" pattern="[A-Za-z]{3}"  type = "text" id = "txtFirstName" value="<?=$row['first_name']?>" name="first_name">
                                        <label class = "mdl-textfield__label">First Name</label>
                                      </div>
                                    </div>
                                    <div class="col-lg-6 p-t-20">
                                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                        <input class = "mdl-textfield__input"  minlength="2" pattern="[A-Za-z]{3}"  type = "text" id = "txtLasttName" value="<?=$row['sur_name']?>" name="sur_name">
                                        <label class = "mdl-textfield__label" >Sur Name</label>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="modal-footer">
                            </form>
                            <button type="button" onclick="update('<?=$id?>','<?=$id?>');" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Save changes</button>
                              <button type="button" class="mdl-button msdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>

                      <tr class="odd gradeX">
                        <td>
                          <center><div class="place <?=$status?>"></div></center>
                        </td>
                        <td>
                        <?=$row['id']?> 
                        </td>
                        <td>
                        <?=$row['first_name']." ".$row['middle_name']." ".$row['sur_name']?> 
                        </td>
                        <td>
                        <?=$row['email']?> 
                        </td>
                        <td> 
                        <?=$row['date_created']?> 
                        </td>
                        <td class="valigntop">
                          <button class="btn btn-primary btn-xs tooltips" data-placement="top" data-original-title="Edit" data-toggle="modal" data-target="#<?=$id?>">
                            <i class="fa fa-pencil">&nbsp;</i>
                            Edit
                          </button>
                          <?php if($status == "green"){ ?>
                          <button onclick="activate('<?=$id?>');"  class="btn btn-primary btn-xs tooltips"  data-placement="top" data-original-title="Activate">
                            <i class="fa fa-check">&nbsp;</i>
                            Activate
                          </button>
                          <?php } else { ?>
                            <button onclick="deactivate('<?=$id?>');"  class="btn btn-primary btn-xs tooltips" style="color:red;" data-placement="top" data-original-title="Deactivate">
                            <i class="fa fa-times">&nbsp;</i>
                            Deactivate
                          </button>
                          <?php } ?>
                        </td>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end page content -->
</div>
<!-- end page container -->
<?php include 'footer.php'; ?>
<script type="text/javascript">
  $("#users").addClass("active");

// $(".place").click(function () {
//    $(this).toggleClass("green");
// });

function activate(myid) {
  $.ajax({
			url: '../controllers/handlers/users.php?activate&id='+myid,
			type: "GET",
      data: {id:myid},
		}).done(function(response){
			if(response == "Success!") {
			swal({
				  	title:response,
				  	icon: "success",
				  	dangerMode: false,
					}).then((willDelete) => {
            location.reload();
				});
			} else {
        swal("Invalid", "something went wrong!", "error");
      }
		});
}

function deactivate(myid) {
  $.ajax({
			url: '../controllers/handlers/users.php?deactivate&id='+myid,
			type: "GET",
      data: {id:myid},
		}).done(function(response){
			if(response == "Success!") {
			swal({
				  	title:response,
				  	icon: "success",
				  	dangerMode: false,
					}).then((willDelete) => {
            location.reload();
				});
			} else {
        swal("Invalid", "something went wrong!", "error");
      }
		});
}


function update(myid,frm) {

    // Get form
    var form = $('#frm'+frm)[0];
    var data = new FormData(form);
    
  $.ajax({
			url: '../controllers/handlers/users.php?update2&id='+myid,
      type: "POST",
      data: data,
      enctype: 'multipart/form-data',
            processData: false,  // Important!
        contentType: false,
        cache: false,
		}).done(function(response){
			if(response == "Success!") {
			swal({
				  	title:response,
				  	icon: "success",
				  	dangerMode: false,
					}).then((willDelete) => {
            location.reload();
				});
			} else {
        swal(response, "something went wrong!", "error");
      }
		});
}



$('#tbl_users').dataTable( {
  "columns": [
    { "width": "15%" },
    { "width": "15%" },
    { "width": "15%" },
    { "width": "3%" },
    { "width": "15%" },
    { "width": "15%" }
  ]
} );
</script>