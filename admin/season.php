<?php include 'header.php'; ?>
<style type="text/css">
  .place { width: 12px; height: 12px; background-color: green;  border-radius: 50%;}
  .place.green { background-color: red; }

  #tbl_season tbody td {
line-height: 8px;
}



</style>
<!-- start page content -->
<div class="page-content-wrapper">
  <div class="page-content">
    <div class="page-bar">
      <div class="page-title-breadcrumb">
        <div class=" pull-left">
          <div class="page-title">Seasons</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
          </li>
          <li class="active">Seasons</li>
        </ol>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card card-topline-green">
          <div class="card-head">
            <header>List of Seasons</header>
            <div class="tools">
              <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
            </div>
          </div>
          <div class="card-body ">
            <div class="row p-b-20">
              <div class="col-md-6 col-sm-6 col-6">
                <div class="btn-group">
                  <button id="addRow1" class="btn btn-info" onclick="window.location.href='add_season.php'">
                  Add New <i class="fa fa-plus"></i>
                  </button>
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-6">
                <div class="btn-group pull-right">
                  <button class="btn deepPink-bgcolor  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                  <i class="fa fa-angle-down"></i>
                  </button>
                  <ul class="dropdown-menu pull-right">
                    <li>
                      <a href="javascript:;">
                      <i class="fa fa-print"></i> Print </a>
                    </li>
                    <li>
                      <a href="javascript:;">
                      <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                    </li>
                    <li>
                      <a href="javascript:;">
                      <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="tbl_season">
                <thead>
                  <tr>
                    <th style="display: none;"> Season ID </th>
                    <th> Season ID </th>
                    <th> Start Date </th>
                    <th> End Date </th>
                    <th> First Payment </th>
                    <th> Second Payment </th>
                    <th> Amount per Share </th>
                    <th> Penalty </th>
                    <th> Interest </th>
                    <th> Settings </th>

                  </tr>
                </thead>
                <tbody>
                <?php
                    $res = $conn->query("SELECT * FROM tbl_seasons");
                    while($row = $res->fetch_assoc()){ 
                      $status = ($row['status'] == 0)?"green":"";
                      $id = $row['id'];
                    ?>
                  <tr class="odd gradeX">
                    <td> <?=$row['id']?> </td>
                    <td> <?=$row['start_date']?> </td>
                    <td> <?=$row['end_date']?> </td>
                    <td> <?=$row['first_payment']?> </td>
                    <td> <?=$row['second_payment']?> </td>
                    <td> <?=$row['amount_per_share']?> </td>
                    <td> <?=$row['penalty']?> </td>
                    <td> <?=$row['interest']?> </td>
                    <td class="valigntop">
                          <?php if($status == "green"){ ?>
                          <button onclick="activate('<?=$id?>');"  class="btn btn-primary btn-xs tooltips">
                            <i class="fa fa-check">&nbsp;Activate</i>
                          </button>
                          <?php } else { ?>
                            <button onclick="deactivate('<?=$id?>');"  class="btn btn-primary btn-xs tooltips" style="color:red;" >
                            <i class="fa fa-times">&nbsp;Deactivate</i>
                          </button>
                          <?php } ?>
                        </td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end page content -->

<?php include 'footer.php'; ?>
<script type="text/javascript">
  $("#season").addClass("active");

$(".place").click(function () {
   $(this).toggleClass("green");
});


function activate(myid) {
  $.ajax({
			url: '../controllers/handlers/seasons.php?activate&id='+myid,
			type: "GET",
      data: {id:myid},
		}).done(function(response){
			if(response == "Success!") {
			swal({
				  	title:response,
				  	icon: "success",
				  	dangerMode: false,
					}).then((willDelete) => {
            location.reload();
				});
			} else {
        swal("Invalid", "something went wrong!", "error");
      }
		});
}

function deactivate(myid) {
  $.ajax({
			url: '../controllers/handlers/seasons.php?deactivate&id='+myid,
			type: "GET",
      data: {id:myid},
		}).done(function(response){
			if(response == "Success!") {
			swal({
				  	title:response,
				  	icon: "success",
				  	dangerMode: false,
					}).then((willDelete) => {
            location.reload();
				});
			} else {
        swal("Invalid", "something went wrong!", "error");
      }
		});
}



$('#tbl_season').dataTable( {
        "order": [[ 1, "desc" ]],
  "columns": [
    { "width": "20%" },
    { "width": "12%" },
    { "width": "12%" },
    { "width": "10%" },
    { "width": "10%" },
    { "width": "10%" },
    { "width": "5%" },
    { "width": "5%" },
    { "width": "10%" }
 
  ]
} );


</script>