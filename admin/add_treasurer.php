<?php include 'header.php'; ?>
        <!-- start page content -->
        <div class="page-content-wrapper">
          <div class="page-content">
            <div class="page-bar">
              <div class="page-title-breadcrumb">
                <div class=" pull-left">
                  <div class="page-title">Add Staff Details</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                  <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                  </li>
                  <li><a class="parent-item" href="user.php">Users</a>&nbsp;<i class="fa fa-angle-right"></i>
                  </li>
                  <li class="active">Add Staff Details</li>
                </ol>
              </div>
            </div>
            <form id="frm_register">
            <div class="row">
              <div class="col-sm-12">
                <div class="card-box">
                  <div class="card-head">
                    <header>Basic Information</header>
                  </div>
                  <div class="card-body row">
                  	<div class="col-lg-6 col-md-6 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class = "mdl-textfield__input" type = "text" id = "txtEmpid" name="id">
                        <label class = "mdl-textfield__label">Employee ID</label>
                      </div>
                    </div>
                  	<div class="col-lg-6 col-md-6 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class = "mdl-textfield__input" type = "email" id = "txtemail" name="email">
                        <label class = "mdl-textfield__label" >Email</label>
                        <span class = "mdl-textfield__error">Enter Valid Email Address!</span>
                      </div>
                    </div>  
                    <div class="col-lg-6 col-md-6 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class = "mdl-textfield__input" onkeypress="return (event.charCode > 64 && 
event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" type = "text" id = "txtFirstName" name="first_name">
                        <label class = "mdl-textfield__label">First Name</label>
                      </div>
                    </div>
                    <div class="col-lg-6 col-md-6 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class = "mdl-textfield__input" onkeypress="return (event.charCode > 64 && 
event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" type = "text" id = "txtMiddleName" name="middle_name"> 
                        <label class = "mdl-textfield__label" >Middle Name</label>
                      </div>
                    </div>
                    <div class="col-lg-6 col-md-6 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class = "mdl-textfield__input" onkeypress="return (event.charCode > 64 && 
event.charCode < 91) || (event.charCode > 96 && event.charCode < 123)" type = "text" id = "txtLasttName" name="sur_name"> 
                        <label class = "mdl-textfield__label" >Last Name</label>
                      </div>
                    </div>
                    <div class="col-lg-6 col-md-6 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class = "mdl-textfield__input" type = "password" id = "txtPwd" name="password">
                        <label class = "mdl-textfield__label" >Password</label>
                      </div>
                    </div>
                    <div class="col-lg-6 col-md-6 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class = "mdl-textfield__input" type = "password" id = "txtConfirmPwd" name="pass2" >
                        <label class = "mdl-textfield__label" >Confirm Password</label>
                      </div>
                    </div>
                    <div class="col-lg-6 col-md-6 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class = "mdl-textfield__input" type = "number" pattern= "[0-9]"id = "text5" name="mobile_number">
                        <label class = "mdl-textfield__label" for = "text5">Mobile Number</label>
                        <span class = "mdl-textfield__error">Number required!</span>
                      </div>
                    </div>
                    </form>
                    <div class="col-lg-12 p-t-20 text-center"> 
                      <button   onclick="register();"  type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Submit</button>
                      <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- end page content -->
      </div>
      <!-- end page container -->

<?php include 'footer.php'; ?>

<script type="text/javascript">
	$("#users").addClass("active");

  function register() {
    var values = $("#frm_register").serialize();
	console.log(values);
		$.ajax({
			url: '../controllers/handlers/users.php?insert&role=treasurer',
			type: "POST",
        	data: values,
		}).done(function(response){
			if(response == "Success!") {
			swal({
				  	title:response,
				  	icon: "success",
				  	dangerMode: false,
					}).then((willDelete) => {
						window.location.href = "user.php";
				});
			} else {
        swal("Invalid", response, "error");
      }
			
		});
  }

</script>