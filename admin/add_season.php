<?php include 'header.php'; ?>
<style>
.mdl-textfield:not(.is-dirty) input::-webkit-datetime-edit {
     color: transparent; 
}

input:focus::-webkit-datetime-edit {
    color: rgba(255, 255, 255, .46) !important; 
}
</style>
        <!-- start page content -->
        <div class="page-content-wrapper">
          <div class="page-content">
            <div class="page-bar">
              <div class="page-title-breadcrumb">
                <div class=" pull-left">
                  <div class="page-title">Add Season Details</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                  <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                  </li>
                  <li><a class="parent-item" href="season.php">Season</a>&nbsp;<i class="fa fa-angle-right"></i>
                  </li>
                  <li class="active">Add Season Details</li>
                </ol>
              </div>
            </div>
            <form id="frm_register">
            <div class="row">
              <div class="col-sm-12">
                <div class="card-box">
                  <div class="card-head">
                    <header>Basic Information</header>
                  </div>
                  <div class="card-body row">
                  	<div class="col-lg-6 col-md-6 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class = "mdl-textfield__input" type = "text" id = "id" name="id">
                        <label class = "mdl-textfield__label">Season Number</label>
                      </div>
                    </div>
                  	<div class="col-lg-6 col-md-6 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class = "mdl-textfield__input" type = "date" id = "start_date" name="start_date">
                        <label class = "mdl-textfield__label" >Start Date</label>
                      </div>
                    </div>  
                    <div class="col-lg-6 col-md-6 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class = "mdl-textfield__input" type = "date" id = "end_date" name="end_date">
                        <label class = "mdl-textfield__label" >End Date</label>
                      </div>
                    </div>  
                    <div class="col-lg-6 col-md-6 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <!-- <input pattern="-?[0-9]*(\.[0-9]+)?" type = "number"  >  -->
                        <select  class = "mdl-textfield__input" id = "first_payment" name="first_payment">
                        <?php for($i = 1 ; $i <= 31 ; $i ++) {  ?>
                        <option><?=$i?></option>
                        <?php }  ?>
                        </select>
                        <label class = "mdl-textfield__label" >First Payment</label>
                      </div>
                    </div>
                    <div class="col-lg-6 col-md-6 p-t-20">
                    <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <!-- <input pattern="-?[0-9]*(\.[0-9]+)?" type = "number" id = "first_payment" >  -->
                        <select  class = "mdl-textfield__input" id = "second_payment"  name="second_payment" >
                        <?php for($i = 1 ; $i <= 31 ; $i ++) {  ?>
                        <option><?=$i?></option>
                        <?php }  ?>
                        </select>
                        <label class = "mdl-textfield__label" >Second Payment</label>
                      </div>
                    </div>
                    <div class="col-lg-6 col-md-6 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class = "mdl-textfield__input" type = "number" id = "amount_per_share" name="amount_per_share"> 
                        <label class = "mdl-textfield__label" >Amount Per Share</label>
                      </div>
                    </div>
                    <!-- <div class="col-lg-6 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class = "mdl-textfield__input" type = "number" id = "txtMiddleName" name="max_share"> 
                        <label class = "mdl-textfield__label" >Max Share</label>
                      </div>
                    </div> -->
                    <div class="col-lg-6 col-md-6 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class = "mdl-textfield__input" type = "number" id = "penalty" name="penalty" value="1" readonly> 
                        <label class = "mdl-textfield__label" >Penalty (%)</label>
                      </div>
                    </div>
                    <div class="col-lg-6 col-md-6 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class = "mdl-textfield__input" type = "number" id = "interest" name="interest"  value="2" readonly> 
                        <label class = "mdl-textfield__label" >Interest (%)</label>
                      </div>
                    </div>
                    </form>
                    <div class="col-lg-12 p-t-20 text-center"> 
                      <button   onclick="register();"  type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Submit</button>
                      <button type="button" onclick="location.href='season.php'" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- end page content -->
      </div>
      <!-- end page container -->

<?php include 'footer.php'; ?>

<script type="text/javascript">

	$("#season").addClass("active");

  function register() {

    var bool = false;
  var ar = ["id","start_date","end_date","first_payment","second_payment","amount_per_share","penalty","interest"];
  for(var i = 0 ; i < ar.length ; i ++) {
    if($(ar[i]).val() == "") {
      bool = false;
    } else {
      bool =  true;
    }
  }
  if(bool == false) {
 
          swal("Invalid!", "Fields are required", "error");
  } else {
    var values = $("#frm_register").serialize();
	console.log(values);
		$.ajax({
			url: '../controllers/handlers/seasons.php?insert',
			type: "POST",
        	data: values,
		}).done(function(response){

			if(response == "Success!") {

			swal({
				  	title:response,
				  	icon: "success",
				  	dangerMode: false,
					}).then((willDelete) => {
						window.location.href = "season.php";
				});
			} else {
        swal("Invalid", response, "error");
      }
			
		});
  }



  
  }


</script>