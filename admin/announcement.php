<?php include 'header.php'; ?>
<style type="text/css">
  .place { width: 12px; height: 12px; background-color: green;  border-radius: 50%;}
  .place.green { background-color: red; }
</style>

<script src="../assets/plugins/jquery/jquery.min.js" ></script>

<!-- start page content -->
<div class="page-content-wrapper">
  <div class="page-content">
    <div class="page-bar">
      <div class="page-title-breadcrumb">
        <div class=" pull-left">
          <div class="page-title">Announcements</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
          </li>
          <li class="active">Announcement</li>
        </ol>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card card-topline-green">
          <div class="card-head">
            <header>List of Announcements</header>
            <div class="tools">
              <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
            </div>
          </div>
          <div class="card-body ">
            <div class="row p-b-20">
              <div class="col-md-6 col-sm-6 col-6">
                <div class="btn-group">
                  <button id="addRow1" class="btn btn-info" onclick="window.location.href='add_announcement.php'">
                  Add New <i class="fa fa-plus"></i>
                  </button>
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-6">
                <div class="btn-group pull-right">
                  <button class="btn deepPink-bgcolor  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                  <i class="fa fa-angle-down"></i>
                  </button>
                  <ul class="dropdown-menu pull-right">
                    <li>
                      <a href="javascript:;" onclick="print('tbl_announcements');">
                      <i class="fa fa-print"></i> Print </a>
                    </li>
                    <li>
                      <a href="javascript:;" onclick="printpdf('tbl_announcements');">
                      <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                    </li>
                    <li>
                      <a href="javascript:;">
                      <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="tbl_announcements">
                <thead>
                  <tr>
                    <th style="display: none;"> Announcement ID </th>
                    <th> Announcement ID </th>
                    <th>Title</th>
                    <th> Description </th>
                    <th> Date Created </th>
                    <th> Date Update </th>
                    <th> Actions </th>

                  </tr>
                </thead>
                <tbody>
                  <?php
                    $res = $conn->query("SELECT * FROM tbl_announcement ORDER BY date_created DESC ");
                    while($row = $res->fetch_assoc()){ 
                      $status = ($row['status'] == 0)?"green":"";
                      $id = $row['id'];
                    ?>
            
                  <!-- Modal Member -->
                  <div class="modal fade" id="<?=$id?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content card-box">
                        <div class="modal-body">
                          <div class="row">
                            <div class="col-sm-12">
                              
                              <div class="card-head">
                                <header>Basic Information</header>
                              </div>
                              <form action="../controllers/handlers/announcement.php?update&id=<?=$id?>" method="POST" enctype="multipart/form-data">
                              <div class="card-body row">

                              <div class="col-lg-12 p-t-20">
                                  <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                    <!-- <input class = "mdl-textfield__input" type = "file" id = "image" name="image" > -->
                                    <div class="dropzone">
                                      <center>
                                        <label for="fileLoader<?=$id?>" class="mdl-js-button dz-message" data-tooltip="Add an Image" data-position="center">
                                          <img id="123<?=$id?>" src="../assets/uploads/<?=$row['image']?>" class="img-responsive" alt=""> 
                                        </label>
                                      </center>
                                      <input type="file" id="fileLoader<?=$id?>" style="display:none" name="image" title="Load File" />
                                    </div>
                                  </div>
                                </div>


                                <div class="col-lg-6 p-t-20">
                                  <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                    <input class = "mdl-textfield__input" type = "text" id = "title" name="title" value="<?=$row['title']?>">
                                    <label class = "mdl-textfield__label">Title</label>
                                  </div>
                                </div>
                                <div class="col-lg-6 p-t-20">
                                  <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                    <input class = "mdl-textfield__input" type = "text" id = "descriptions" value="<?=$row['descriptions']?>" name="descriptions">
                                    <label class = "mdl-textfield__label">Description</label>
                                  </div>
                                </div>
                              
                              
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="modal-footer">
                        <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Save changes</button>
                        </form>
                          <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default" data-dismiss="modal">Close</button>
                        </div>
                      </div>
                    </div>
                  </div>

                  <script>
                  $("#fileLoader<?=$id?>").change(function () {
                        if (this.files && this.files[0]) {
                            var reader = new FileReader();
                            reader.onload = function (e) {
                                $('#123<?=$id?>').attr('src', e.target.result);
                            }
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                  </script>

                  <tr class="odd gradeX">
                    <td> <?=$row['id']?> </td>
                    <td> <?=$row['title']?> </td>
                    <td> <?=$row['descriptions']?> </td>
                    <td> <?=$row['date_created']?> </td>
                    <td> <?=$row['date_update']?> </td>
                    <td class="valigntop">
                    <button class="btn btn-primary btn-xs tooltips" data-placement="top" data-original-title="Edit" data-toggle="modal" data-target="#<?=$id?>">
                            <i class="fa fa-pencil">&nbsp;</i>
                            Edit
                          </button>

                          <?php if($status == "green"){ ?>
                          <button onclick="activate('<?=$id?>');"  class="btn btn-primary btn-xs tooltips">
                            <i class="fa fa-check">&nbsp;Activate</i>
                          </button>
                          <?php } else { ?>
                            <button onclick="deactivate('<?=$id?>');"  class="btn btn-primary btn-xs tooltips" style="color:red;" >
                            <i class="fa fa-times">&nbsp;Deactivate</i>
                          </button>
                          <?php } ?>
                        </td>
                  </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end page content -->

<?php include 'footer.php'; ?>

<script>


$(document).ready(function() {
    $('#tbl_announcements').DataTable( {
        "order": [[ 3, "desc" ]]
    } );
} );
</script>


<script type="text/javascript">
  $("#announcement").addClass("active");

$(".place").click(function () {
   $(this).toggleClass("green");
});


function activate(myid) {
  $.ajax({
			url: '../controllers/handlers/announcement.php?activate&id='+myid,
			type: "GET",
      data: {id:myid},
		}).done(function(response){
			if(response == "Success!") {
			swal({
				  	title:response,
				  	icon: "success",
				  	dangerMode: false,
					}).then((willDelete) => {
            location.reload();
				});
			} else {
        swal("Invalid", "something went wrong!", "error");
      }
		});
}

function deactivate(myid) {
  $.ajax({
			url: '../controllers/handlers/announcement.php?deactivate&id='+myid,
			type: "GET",
      data: {id:myid},
		}).done(function(response){
			if(response == "Success!") {
			swal({
				  	title:response,
				  	icon: "success",
				  	dangerMode: false,
					}).then((willDelete) => {
            location.reload();
				});
			} else {
        swal("Invalid", "something went wrong!", "error");
      }
		});
}






</script>

