
<?php
if(!isset($_SESSION)) {
  session_start();
}

if(empty($_SESSION['userLog']['id'])) {
  header("location:../staff/login.php");
}


 $img = (empty($_SESSION["userLog"]["image"]))?"../assets/img/empty.png":"../assets/uploads/".$_SESSION["userLog"]["image"];



include("../env.php");
$env = new env();
$conn = $env->connectDb();

$date_now = date("Y-m-d"); 

$check_season = $conn->query("SELECT * FROM tbl_seasons WHERE status = 1 ");
while($row_season = $check_season->fetch_assoc()) {
  if($date_now > $row_season['end_date']) {
    $conn->query("UPDATE tbl_seasons SET status = 0  WHERE id = '".$row_season['id']."' ; ");
  }
}
?>

<?php

$res1 = $conn->query("SELECT * FROM tbl_users WHERE role = 'member' AND status = 1 ");
$count_members = $res1->num_rows;
$res2 = $conn->query("SELECT * FROM tbl_users WHERE role = 'treasurer' AND status = 1 ");
$count_treasurer = $res2->num_rows;
$res3 = $conn->query("SELECT * FROM tbl_users WHERE role = 'admin' AND status = 1 ");
$count_admin = $res3->num_rows;
?>

        

<!DOCTYPE html>
<html lang="en">
  <!-- BEGIN HEAD -->
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="Responsive Admin Template" />
    <meta name="author" content="SmartUniversity" />
    <title>CCS | Saving and Loaning</title>
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
    <!-- icons -->
    <link href="../assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <!--bootstrap -->
    <!-- dropzone -->
    <link href="../assets/plugins/dropzone/dropzone.css" rel="stylesheet" media="screen">
    <link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- morris chart -->
    <link href="../assets/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- full calendar -->
    <!-- <link href="../assets/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" /> -->
    <!-- data tables -->
    <link href="../assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <!-- Material Design Lite CSS -->
    <link rel="stylesheet" href="../assets/plugins/material/material.min.css">
    <link rel="stylesheet" href="../assets/css/material_style.css">
    <!-- animation -->
    <link href="../assets/css/pages/animate_page.css" rel="stylesheet">
    <!-- Template Styles -->
    <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/theme-color.css" rel="stylesheet" type="text/css" />

    <!-- favicon -->
    <link rel="shortcut icon" href="../assets/img/logo.png" />
  </head>
  <!-- END HEAD -->
  <body ng-app="myApp" ng-cloak class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">
    <div class="page-wrapper">
      <!-- start header -->
      <div class="page-header navbar navbar-fixed-top" ng-controller="headerCtrl" ng-cloak>
        <div class="page-header-inner ">
          <!-- logo start -->
          <div class="page-logo">
            <a href="dashboard.php">
            <img alt="" src="../assets/img/logo.png" width="40px">
            <span class="logo-default" >CCS </span> </a>
          </div>
          <!-- logo end -->
          <ul class="nav navbar-nav navbar-left in">
            <li><a href="#" class="menu-toggler sidebar-toggler"><i class="icon-menu"></i></a></li>
          </ul>
          <!-- start mobile menu -->
          <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
          <span></span>
          </a>
          <!-- end mobile menu -->
          <!-- start header menu -->
          <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
         
              <!-- start manage user dropdown -->
              <li class="dropdown dropdown-user">
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <img style="width: 30px; height: 30px" alt="" class="img-circle " src='<?=$img?>'/>
                <span class="username username-hide-on-mobile"> <?=$_SESSION['userLog']['first_name']?> </span>
                <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-default animated jello">
                  <li>
                    <a href="profile.php">
                    <i class="icon-user" id="profile"></i> Profile </a>
                  </li>
                  <li>
                    <a href="../controllers/handlers/users.php?logout">
                    <i class="icon-logout"></i> Log Out </a>
                  </li>
                </ul>
              </li>
              <!-- end manage user dropdown -->
            </ul>
          </div>
        </div>
      </div>
      <!-- end header -->
      <!-- start page container -->
      <div class="page-container">
        <!-- start sidebar menu -->
        <div class="sidebar-container">
          <div class="sidemenu-container navbar-collapse collapse fixed-menu">
            <div id="remove-scroll">
              <ul class="sidemenu page-header-fixed p-t-20" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                <li class="sidebar-toggler-wrapper hide">
                  <div class="sidebar-toggler">
                    <span></span>
                  </div>
                </li>
                <li class="sidebar-user-panel">
                  <div class="user-panel">
                    <div class="row">
                      <div class="sidebar-userpic">
                        <img style="width: 100px; height: 100px" src="<?=$img?>" class="img-responsive" alt=""> 
                      </div>
                    </div>
                    <div class="profile-usertitle">
                      <div class="sidebar-userpic-name"> <?=$_SESSION['userLog']['first_name']?>&nbsp;<?=$_SESSION['userLog']['sur_name']?> </div>
                      <div class="profile-usertitle-job"> Admin </div>
                    </div>
                    <div class="sidebar-userpic-btn">
                      <a class="tooltips" href="profile.php" data-placement="top" data-original-title="Profile">
                        <i class="material-icons">person_outline</i>
                      </a>
                      <label class="btn btn-circle btn-success btn-xs m-b-10">Online</label>
                      <a class="tooltips" href="../controllers/handlers/users.php?logout" data-placement="top" data-original-title="Logout">
                        <i class="material-icons">input</i>
                      </a>
                  </div>
                  </div>
                </li>
                <li class="menu-heading">
                  <span>-- Main</span>
                </li>
                <li class="nav-item" id="dashboard">
                  <a href="dashboard.php" class="nav-link nav-toggle"> <i class="material-icons">dashboard</i>
                  <span class="title">Dashboard</span> 
                  </a>
                </li>
                <li class="nav-item" id="users">
                  <a href="user.php" class="nav-link nav-toggle"> <i class="material-icons">account_circle</i>
                  <span class="title">Users</span> 
                  </a>
                </li>
                <li class="nav-item" id="season">
                  <a href="season.php" class="nav-link nav-toggle"> <i class="material-icons">payment</i>
                  <span class="title">Season</span> 
                  </a>
                </li>
                <li class="nav-item" id="announcement">
                  <a href="announcement.php" class="nav-link nav-toggle">  <i class="material-icons">announcement</i>
                  <span class="announcement">Announcement</span> 
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <!-- end sidebar menu --> 