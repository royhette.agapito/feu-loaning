<?php include 'header.php'; ?>

  <!-- start page content -->
  <div class="page-content-wrapper">
    <div class="page-content">
      <div class="page-bar">
        <div class="page-title-breadcrumb">
          <div class=" pull-left">
            <div class="page-title">Dashboard</div>
          </div>
          <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Dashboard</li>
          </ol>
        </div>
      </div>
      <!-- start widget -->
      <div class="state-overview">
        <div class="row">
          <div class="col-sm-4">
            <div class="overview-panel purple">
              <div class="symbol">
                <i class="fa fa-users usr-clr"></i>
              </div>
              <div class="value white">
                <p class="sbold addr-font-h1" data-counter="counterup" data-value="<?=$count_members?>"><?=$count_members?></p>
                <p>Members</p>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="overview-panel deepPink-bgcolor">
              <div class="symbol">
                <i class="fa fa-key"></i>
              </div>
              <div class="value white">
                <p class="sbold addr-font-h1" data-counter="counterup" data-value="<?=$count_admin?>"><?=$count_admin?></p>
                <p>Administrator</p>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="overview-panel orange">
              <div class="symbol">
                <i class="fa fa-briefcase"></i>
              </div>
              <div class="value white">
                <p class="sbold addr-font-h1" data-counter="counterup" data-value="<?=$count_treasurer?>"><?=$count_treasurer?></p>
                <p>Treasurer</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end widget -->





<div class="row">
      <div class="col-md-12">
        <div class="card card-topline-green">
          <div class="card-head">
            <header>AUDIT TRAIL</header>
            <div class="tools">
              <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
            </div>
          </div>
          <div class="card-body ">
            <div class="row p-b-20">
              <div class="col-md-6 col-sm-6 col-6">
              </div>
              <div class="col-md-6 col-sm-6 col-6">
                <div class="btn-group pull-right">
                  <button class="btn deepPink-bgcolor  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                  <i class="fa fa-angle-down"></i>
                  </button>
                  <ul class="dropdown-menu pull-right">
                    <li>
                      <a href="javascript:;">
                      <i class="fa fa-print"></i> Print </a>
                    </li>
                    <li>
                      <a href="javascript:;">
                      <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                    </li>
                    <li>
                      <a href="javascript:;">
                      <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            


      
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="season_tbl">
                <thead>
                  <tr>
                    <th> Season ID </th>
                    <th> Total Earnings (INTEREST & PENALTIES) </th>
                  </tr>
                </thead>
                <tbody>
                <?php
                $res = $conn->query("SELECT id FROM tbl_seasons");
                 
                while($row = $res->fetch_assoc()) { 

                  $res2 = $conn->query("SELECT a.user_id , a.season_id , a.amount , (SELECT tbl_seasons.amount_per_share FROM tbl_seasons WHERE tbl_seasons.id = a.season_id) as perShare , 
                 (SELECT tbl_join_season.shares FROM tbl_join_season WHERE tbl_join_season.season_id = a.season_id AND tbl_join_season.user_id = a.user_id) as shares 
                 FROM tbl_transactions as a WHERE a.season_id = '".$row['id']."' ");

                 $penalty = 0;
                 while($row2 = $res2->fetch_assoc()) {
                    $prod = $row2['perShare']*$row2['shares'];
                    $penalty += $row2['amount'] - $prod; 
                  }

                 $res3 = $conn->query("SELECT a.amount , a.month FROM tbl_loan as a WHERE a.season_id = '".$row['id']."' ");
                 while($row3 = $res3->fetch_assoc()) {
                   $monthInterest = $row3['month'];
                   $perc = $monthInterest*0.02;
                   $prod2 = $row3['amount']*$perc;
                  $penalty += $prod2; 
                }

                ?>
                  <tr class="odd gradeX">
                    <td> <?=$row['id']?> </td>
                    <td> <?=($penalty > 0)?$penalty:0;?> </td>
                  </tr>
                <?php } ?>

                </tbody>
              </table>
            </div>


          </div>
        </div>
    </div>
  </div>

      
      <div class="row">
      <div class="col-md-12">
        <div class="card card-topline-green">
          <div class="card-head">
            <header>AUDIT TRAIL</header>
            <div class="tools">
              <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
            </div>
          </div>
          <div class="card-body ">
            <div class="row p-b-20">
              <div class="col-md-6 col-sm-6 col-6">
              </div>
              <div class="col-md-6 col-sm-6 col-6">
                <div class="btn-group pull-right">
                  <button class="btn deepPink-bgcolor  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                  <i class="fa fa-angle-down"></i>
                  </button>
                  <ul class="dropdown-menu pull-right">
                    <li>
                      <a href="javascript:;">
                      <i class="fa fa-print"></i> Print </a>
                    </li>
                    <li>
                      <a href="javascript:;">
                      <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                    </li>
                    <li>
                      <a href="javascript:;">
                      <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            


            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="audit_tbl">
                <thead>
               
                  <tr>
                    <th> Name </th>
                    <th> Role </th>
                    <th> Actions </th>
                    <th> Date </th>
                  </tr>
                </thead>
                <tbody>
                <?php
                $res = $conn->query("SELECT a.first_name,a.middle_name,a.sur_name,a.role,b.message,b.date_created
                FROM tbl_userlogs as b INNER JOIN tbl_users as a ON b.user_id = a.id WHERE b.status = 1 ");
                while($row = $res->fetch_assoc()) { 
                ?>
                  <tr class="odd gradeX">
                    <td> <?=$row['first_name']." ".$row['middle_name']." ".$row['sur_name']?> </td>
                    <td>
                    <?=$row['role']?>
                    </td>
                    <td>
                    <?=$row['message']?>
                    </td>
                    <td>
                    <?=$row['date_created']?>
                    </td>
                  </tr>
                <?php } ?>

                </tbody>
              </table>
            </div>



          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end page content -->

<?php include 'footer.php'; ?>

<script type="text/javascript">
  $("#dashboard").addClass("active");


  

  $(document).ready(function() {
    $('#audit_tbl').DataTable( {
        "order": [[ 3, "desc" ]]
    } );
} );


$(document).ready(function() {
    $('#season_tbl').DataTable( {
    } );
} );
</script>