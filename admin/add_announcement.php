<?php include 'header.php'; ?>
        <!-- start page content -->
        <div class="page-content-wrapper">
          <div class="page-content">
            <div class="page-bar">
              <div class="page-title-breadcrumb">
                <div class=" pull-left">
                  <div class="page-title">Add Announcement Details</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                  <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                  </li>
                  <li><a class="parent-item" href="announcement.php">Announcement</a>&nbsp;<i class="fa fa-angle-right"></i>
                  </li>
                  <li class="active">Add Announcement Details</li>
                </ol>
              </div>
            </div>
            <form id="frm_register" >
            <div class="row">
              <div class="col-sm-12">
                <div class="card-box">
                  <div class="card-head">
                    <header>Basic Information</header>
                  </div>
                  <div class="card-body row">
                  	<div class="col-lg-3 col-md-3 col-sm-4 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <div class="dropzone">
                          <center>
                            <label for="fileLoader" class="mdl-js-button dz-message" data-tooltip="Add an Image" data-position="center">
                              <img id="imgshow" src="../assets/img/bg.png" class="img-responsive" alt=""> 
                            </label>
                          </center>
                          <input type="file" id="fileLoader" style="display:none" name="image" title="Load File" />
                        </div>
                      </div>
                    </div>
                  	<div class="col-lg-6 col-md-6 col-sm-8 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class = "mdl-textfield__input" type = "text" id = "txtemail" name="title">
                        <label class = "mdl-textfield__label" >Title</label>
                      </div>
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class = "mdl-textfield__input" type = "text" id = "txtemail" name="descriptions">
                        <label class = "mdl-textfield__label" >Description</label>
                      </div>
                    </div>  
                    <div class="col-lg-6 p-t-20">
                      
                    </div>  
                    </form>
                    <div class="col-lg-12 p-t-20 text-center"> 
                      <button   onclick="register();"  type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Submit</button>
                      <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Cancel</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- end page content -->
      </div>
      <!-- end page container -->

<?php include 'footer.php'; ?>

<script type="text/javascript">
	$("#announcement").addClass("active");

  function register() {
        // Get form
        var form = $('#frm_register')[0];

		// Create an FormData object 
        var data = new FormData(form);
    
		$.ajax({
			url: '../controllers/handlers/announcement.php?insert',
			type: "POST",
            data: data,
            enctype: 'multipart/form-data',
            processData: false,  // Important!
        contentType: false,
        cache: false,
		}).done(function(response){
			if(response == "Success!") {
			swal({
				  	title:response,
				  	icon: "success",
				  	dangerMode: false,
					}).then((willDelete) => {
						window.location.href = "announcement.php";
				});
			} else {
        swal("Invalid", response, "error");
      }
			
		});
  }
  $("#fileLoader").change(function () {
    if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#imgshow').attr('src', e.target.result);
        }
        reader.readAsDataURL(this.files[0]);
    }
});

</script>