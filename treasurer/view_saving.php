<?php include 'header.php'; ?>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content card-box">
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            
            <div class="card-head">
              <header>Contribution</header>
            </div>
            <div class="card-body row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-12 p-t-20">
                <div class="form-group">
                  <label>Employee ID</label>
                  <input type="number" class="form-control" placeholder="Enter employee ID">
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-6 p-t-20">
                <div class="form-group">
                  <label>First Name</label>
                  <input type="text" class="form-control" placeholder="Enter first name">
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-6 p-t-20">
                <div class="form-group">
                  <label>Last Name</label>
                  <input type="text" class="form-control" placeholder="Enter last name">
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-6 p-t-20">
                <div class="form-group">
                  <label>Shares</label>
                  <input type="text" class="form-control" placeholder="Shares">
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-6 p-t-20">
                <div class="form-group">
                  <label>Amount</label>
                  <input type="number" class="form-control" placeholder="Enter Amount">
                </div>
              </div>
              <div class="col-lg-12 p-t-20"> 
                <div class="form-group">
                  <label>Payment</label>
                  <select class="form-control">
                    <option>First Payment</option>
                    <option>Second Payment</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink"">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- start page content -->
<div class="page-content-wrapper">
  <div class="page-content">
    <div class="page-bar">
      <div class="page-title-breadcrumb">
        <div class=" pull-left">
          <div class="page-title">View Savings (Season #)</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
          </li>
          <li><a class="parent-item" href="saving.php">Savings</a>&nbsp;<i class="fa fa-angle-right"></i>
          </li>
          <li class="active">View</li>
        </ol>
      </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          
        <div class="profile-tab-box">
          <div class="p-l-20">
            <ul class="nav ">
              <li class="nav-item tab-all"><a
                class="nav-link active show" href="#tab1" data-toggle="tab">Savings</a></li>
              <li class="nav-item tab-all p-l-20"><a class="nav-link"
                href="#tab2" data-toggle="tab">Transactions</a></li>
            </ul>
          </div>
        </div>
        <!-- Tab panes -->
        <div class="tab-content">
          <div class="tab-pane active fontawesome-demo" id="tab1">
            <div class="card card-topline-green">
              <div class="card-head">
                <header>List of Savings</header>
              </div>
              <div class="card-body ">
                <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                    <thead>
                      <tr>
                        <th>
                          <label class="rt-chkbox rt-chkbox-single rt-chkbox-outline">
                          <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                          <span></span>
                          </label>
                        </th>
                        <th style="display: none;"> Username </th>
                        <th> Username </th>
                        <th> Email </th>
                        <th> Status </th>
                        <th> Joined </th>
                        <th> Actions </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="odd gradeX">
                        <td>
                          <label class="rt-chkbox rt-chkbox-single rt-chkbox-outline">
                          <input type="checkbox" class="checkboxes" value="1" />
                          <span></span>
                          </label>
                        </td>
                        <td> shuxer </td>
                        <td>
                          <a href="mailto:shuxer@gmail.com"> shuxer@gmail.com </a>
                        </td>
                        <td>
                          <div class="place"></div>
                        </td>
                        <td> 12 Jan 2012 </td>
                        <td class="valigntop">
                          <button class="btn btn-primary btn-xs tooltips" data-placement="top" data-original-title="Savings" data-toggle="modal" data-target="#exampleModal">
                            <i class="fa fa-dollar"></i>
                          </button>
                        </td>
                      </tr>
                      <tr class="odd gradeX">
                        <td>
                          <label class="rt-chkbox rt-chkbox-single rt-chkbox-outline">
                          <input type="checkbox" class="checkboxes" value="1" />
                          <span></span>
                          </label>
                        </td>
                        <td> shuxer </td>
                        <td>
                          <a href="mailto:shuxer@gmail.com"> shuxer@gmail.com </a>
                        </td>
                        <td>
                          <div class="place"></div>
                        </td>
                        <td> 12 Jan 2012 </td>
                        <td class="valigntop">
                          <button class="btn btn-primary btn-xs tooltips" data-placement="top" data-original-title="Savings">
                            <i class="fa fa-dollar"></i>
                          </button>
                        </td>
                      </tr>
                      <tr class="odd gradeX">
                        <td>
                          <label class="rt-chkbox rt-chkbox-single rt-chkbox-outline">
                          <input type="checkbox" class="checkboxes" value="1" />
                          <span></span>
                          </label>
                        </td>
                        <td> shuxer </td>
                        <td>
                          <a href="mailto:shuxer@gmail.com"> shuxer@gmail.com </a>
                        </td>
                        <td>
                          <div class="place"></div>
                        </td>
                        <td> 12 Jan 2012 </td>
                        <td class="valigntop">
                          <button class="btn btn-primary btn-xs tooltips" data-placement="top" data-original-title="Savings">
                            <i class="fa fa-dollar"></i>
                          </button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane" id="tab2">
            <div class="card card-topline-green">
              <div class="card-head">
                <header>List of Transaction</header>
                <div class="tools">
                  <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                </div>
              </div>
              <div class="card-body ">
                <div class="row p-b-20">
                  <div class="col-md-6 col-sm-6 col-6">
                  </div>
                  <div class="col-md-6 col-sm-6 col-6">
                    <div class="btn-group pull-right">
                      <button class="btn deepPink-bgcolor  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                      <i class="fa fa-angle-down"></i>
                      </button>
                      <ul class="dropdown-menu pull-right">
                        <li>
                          <a href="javascript:;">
                          <i class="fa fa-print"></i> Print </a>
                        </li>
                        <li>
                          <a href="javascript:;">
                          <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                        </li>
                        <li>
                          <a href="javascript:;">
                          <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                
                <div class="table-responsive">
                  <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="s">
                    <thead>
                      <tr>
                        <th>
                          <label class="rt-chkbox rt-chkbox-single rt-chkbox-outline">
                          <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                          <span></span>
                          </label>
                        </th>
                        <th> Username </th>
                        <th> Email </th>
                        <th> Status </th>
                        <th> Joined </th>
                        <th> Actions </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="odd gradeX">
                        <td>
                          <label class="rt-chkbox rt-chkbox-single rt-chkbox-outline">
                          <input type="checkbox" class="checkboxes" value="1" />
                          <span></span>
                          </label>
                        </td>
                        <td> shuxer </td>
                        <td>
                          <a href="mailto:shuxer@gmail.com"> shuxer@gmail.com </a>
                        </td>
                        <td>
                          <div class="place"></div>
                        </td>
                        <td> 12 Jan 2012 </td>
                        <td class="valigntop">
                          <button class="btn btn-primary btn-xs tooltips" data-placement="top" data-original-title="Edit">
                            <i class="fa fa-pencil"></i>
                          </button>
                        </td>
                      </tr>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end page content -->
</div>
<!-- end page container -->
<?php include 'footer.php'; ?>
<script type="text/javascript">
  $("#savings").addClass("active");
</script>