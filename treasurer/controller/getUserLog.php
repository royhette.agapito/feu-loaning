<?php
include("../../env.php");
$env = new env();
$conn = $env->connectDb();
$data = [];
$res = $conn->query("SELECT a.first_name,a.middle_name,a.sur_name,a.role,b.message,b.date_created
FROM tbl_userlogs as b INNER JOIN tbl_users as a ON b.user_id = a.id WHERE b.status = 1 AND a.role='member' ORDER BY b.date_created DESC ");
while($row = $res->fetch_assoc()) { 
    $data[] = $row;
}
echo json_encode($data,JSON_NUMERIC_CHECK);