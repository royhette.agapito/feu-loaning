<?php
include("../../env.php");
$env = new env();
$conn = $env->connectDb();
$data = [];
$res = $conn->query("SELECT tbl_transactions.id, tbl_transactions.user_id, tbl_transactions.season_id, tbl_transactions.type, tbl_transactions.amount, tbl_transactions.image, tbl_transactions.date_created, tbl_transactions.date_update, tbl_transactions.status, tbl_transactions.remarks,tbl_users.first_name,tbl_users.sur_name FROM `tbl_transactions` LEFT JOIN tbl_users ON tbl_transactions.user_id = tbl_users.id WHERE tbl_transactions.remarks1 IS NULL ORDER BY tbl_transactions.status");
while($row = $res->fetch_assoc()) { 
    $data[] = $row;
}
echo json_encode($data,JSON_NUMERIC_CHECK);
