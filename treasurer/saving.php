<?php include 'header.php'; ?>

  <!-- start page content -->
  <div class="page-content-wrapper" ng-controller="savingsCtrl" ng-cloak>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content card-box">
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12">
                
                <div class="card-head">
                  <header>Contribution</header>
                </div>
                <div class="card-body row">
                  <div class="col-lg-12 p-t-20">
                    <div class="form-group">
                      <label>Employee ID</label>
                      <input type="number" class="form-control" value="{{getSelectedMemberData.user_id}}" placeholder="Enter employee ID" disabled>
                    </div>
                  </div>
                  <div class="col-lg-6 p-t-20">
                    <div class="form-group">
                      <label>First Name</label>
                      <input type="text" class="form-control" value="{{getSelectedMemberData.fname}}" placeholder="Enter first name" disabled>
                    </div>
                  </div>
                  <div class="col-lg-6 p-t-20">
                    <div class="form-group">
                      <label>Last Name</label>
                      <input type="text" class="form-control" value="{{getSelectedMemberData.lname}}" placeholder="Enter last name" disabled>
                    </div>
                  </div>
                  <div class="col-lg-6 p-t-20">
                    <div class="form-group">
                      <label>Shares</label>
                      <input type="text" class="form-control" value="{{getSelectedMemberData.shares}}" placeholder="Shares" disabled>
                    </div>
                  </div>
                  <div class="col-lg-6 p-t-20">
                    <div class="form-group">
                      <label>Amount</label>
                      <input type="number" class="form-control" value="{{getSelectedMemberData.amount}}" placeholder="Enter Amount" disabled>
                    </div>
                  </div>
                  <div class="col-lg-12 p-t-20"> 
                    <div class="form-group">
                      <!-- {{getSelectedMemberData.totalDays}} -->{{paymentOption.options}}
                      <label>Payment</label>
                      <div ng-show="getSelectedMemberData.totalDays==1">
                        <select class="form-control" ng-model="paymentOption.options" ng-change="onChangeSelectedPayment()">
                          <option ng-if="checkpaymentStatus.first == ''" ng-value="0">First Payment</option>
                          <option ng-if="checkpaymentStatus.second == ''" ng-value="1">Second Payment</option>
                        </select>
                      </div>
                      <div ng-show="getSelectedMemberData.totalDays==2">
                        <select class="form-control" ng-model="paymentOption.options" ng-change="onChangeSelectedPayment()">
                          <option ng-if="checkpaymentStatus.first == ''" ng-value="0">First Payment</option>
                          <option ng-if="checkpaymentStatus.second == ''" ng-value="1">Second Payment</option>
                          <option ng-if="checkpaymentStatus.Third == ''" ng-value="2">third Payment</option>
                          <option ng-if="checkpaymentStatus.Fourth == ''" ng-value="3">Fourth Payment</option>
                        </select>
                      </div>
                      <div ng-show="getSelectedMemberData.totalDays==3">
                        <select class="form-control" ng-model="paymentOption.options" ng-change="onChangeSelectedPayment()">
                          <option ng-if="checkpaymentStatus.first == ''" ng-value="0">First Payment</option>
                          <option ng-if="checkpaymentStatus.second == ''" ng-value="1">Second Payment</option>
                          <option ng-if="checkpaymentStatus.Third == ''" ng-value="2">third Payment</option>
                          <option ng-if="checkpaymentStatus.Fourth == ''" ng-value="3">Fourth Payment</option>
                          <option ng-if="checkpaymentStatus.fifth == ''" ng-value="4">fifth Payment</option>
                          <option ng-if="checkpaymentStatus.sixth == ''" ng-value="5">sixth Payment</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12 p-t-20">
                      <div class="form-group">
                        <label>BreakDown:</label><br>
                        <center>{{breakdown}}</center>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink" ng-click="pay()">Pay</button>
          </div>
        </div>
      </div>
    </div>
    <div class="page-content" ng-if="viewseasons == ''">
      <div class="page-bar">
        <div class="page-title-breadcrumb">
          <div class=" pull-left">
            <div class="page-title">Savings</div>
          </div>
          <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Savings</li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-sm-12 col-12" ng-repeat="data in allSeasonData">
          <div class="card  card-box">
            <div class="card-head">
              <header>Season Details</header>
              <div class="tools">
                <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
              </div>
            </div>
            <div class="card-body no-padding height-9">
              <div class="row">
                <div class="noti-information notification-menu">
                  <div class="notification-list mail-list not-list small-slimscroll-style">
                    <a href="javascript:;" class="single-mail"> <span class="icon bg-primary">#
                    </span>{{data.id}}
                    <span class="notificationtime">
                    <small>Season number</small>
                    </span>
                    </a>
                    <a href="javascript:;" class="single-mail"> <span class="icon blue-bgcolor"> <i class="fa fa-calendar"></i>
                    </span>{{data.start_date+' '+data.end_date}}
                    <span class="notificationtime">
                    <small>Duration</small>
                    </span>
                    </a>
                    <a href="javascript:;" class="single-mail"> <span class="icon bg-success"> <i class="fa fa-credit-card"></i>
                    </span> {{data.first_payment}}
                    <span class="notificationtime">
                    <small> 1st Payment</small>
                    </span>
                    </a>
                    <a href="javascript:;" class="single-mail"> <span class="icon bg-success"> <i class="fa fa-credit-card"></i>
                    </span> {{data.second_payment}}
                    <span class="notificationtime">
                    <small> 2nd Payment</small>
                    </span>
                    </a>
                    <a href="javascript:;" class="single-mail"> <span class="icon bg-warning"> <i class="fa fa-share-alt"></i>
                    </span>{{data.amount_per_share}}
                    <span class="notificationtime">
                    <small>Amount Per Share</small>
                    </span>
                    </a>
                    <a href="javascript:;" class="single-mail"> <span class="icon bg-warning"> <i class="fa fa-share-alt"></i>
                    </span>{{data.max_share}}
                    <span class="notificationtime">
                    <small>Max Share</small>
                    </span>
                    </a>
                  </div>
                  <div class="full-width text-center p-t-10" >
                    <button type="button" class="btn purple btn-outline btn-circle margin-0" ng-click="ViewSeason(data.id)">View Season</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="page-content"  ng-if="viewseasons != ''">
      <div class="page-bar">
        <div class="page-title-breadcrumb">
          <div class=" pull-left">
            <div class="page-title">View Savings (Season #  {{seasonId}})</div>
          </div>
          <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
            </li>
            <li><a class="parent-item" href="saving.php">Savings</a>&nbsp;<i class="fa fa-angle-right"></i>
            </li>
            <li class="active">View</li>
          </ol>
        </div>
      </div>
      <div class="row">
          <div class="col-md-12">
            
          <div class="profile-tab-box">
            <div class="p-l-20">
              <ul class="nav ">
                <li class="nav-item tab-all"><a
                  class="nav-link active show" ng-click="getAllJoinedSeason()" href="#tab1" data-toggle="tab">Savings</a></li>
                <li class="nav-item tab-all p-l-20"><a class="nav-link"
                  href="#tab2" ng-click="getAllTransaction()" data-toggle="tab">Transactions</a></li>
              </ul>
            </div>
          </div>
          <!-- Tab panes -->
          <div class="tab-content">
            <div class="tab-pane active fontawesome-demo" id="tab1">
              <div class="card card-topline-green">
                <div class="card-head">
                  <header>List of Savings</header>
                </div>
                <div class="card-body ">
                  <div class="table-responsive">
                    <div class="table-wrap">
                      <div class="table-responsive">
                        <table  datatable="ng" dt-options="vm.dtOptions" class="table display product-overview mb-30">
                          <thead>
                            <tr>
                              <th> Employee ID </th>
                              <th> First Name </th>
                              <th> Last Name </th>
                              <th> Email </th>
                              <th> Shares </th>
                              <th> Action </th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr class="odd gradeX" ng-repeat="data in getAllJoinedSeasonData">
                              <td> {{data.user_id}} </td>
                              <td> {{data.first_name}} </td>
                              <td> {{data.sur_name}} </td>
                              <td>
                                <a href="mailto:{{data.email}}"> {{data.email}} </a>
                              </td>
                              <td>
                                <div>{{data.shares}}</div>
                              </td>
                              <td class="valigntop">
                                <button class="btn btn-primary btn-xs tooltips" ng-click="getSelectedMember(data.user_id,data.first_name,data.sur_name,data.shares)" data-placement="top" data-original-title="Savings" data-toggle="modal" data-target="#imageupload">
                                  <i class="fa fa-dollar"></i>
                                </button>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="tab2">
              <div class="card card-topline-green">
                <div class="card-head">
                  <header>List of Transaction</header>
                  <div class="tools">
                    <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                  </div>
                </div>
                <div class="card-body ">
                  <div class="row p-b-20">
                    <div class="col-md-6 col-sm-6 col-6">
                    </div>
                    <div class="col-md-6 col-sm-6 col-6">
                      <div class="btn-group pull-right">
                        <button class="btn deepPink-bgcolor  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                        <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu pull-right">
                          <li>
                            <a href="javascript:;" ng-click="print()">
                            <i class="fa fa-print"></i> Print </a>
                          </li>
                          <li>
                            <a href="javascript:;">
                            <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                          </li>
                          <li>
                            <a href="javascript:;">
                            <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  
                  <div class="table-responsive">
                    <table id="order_table1" datatable="ng" dt-options="vm.dtOptions" class="table table-striped table-bordered table-hover table-checkable order-column full-width">
                      <thead>
                        <tr>
                          <th> Employee ID </th>
                          <th> First Name </th>
                          <th> Last Name </th>
                          <th> Savings Date </th>
                          <th> Amount </th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr class="odd gradeX" ng-repeat="data in getAllTransactionData">
                          <td> {{data.user_id}} </td>
                          <td>{{data.first_name}}</td>
                          <td> {{data.sur_name}} </td>
                          <td> {{data.date_created}} </td>
                          <td> {{data.amount}}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end page content -->

<?php include 'footer.php'; ?>

<script type="text/javascript">
  $("#savings").addClass("active");
  $('#print').click(function(){
    alert();
	var divToPrint=document.getElementById("order_table1");
   newWin= window.open("");
   newWin.document.write(divToPrint.outerHTML);
   newWin.print();
   newWin.close();
});
</script>