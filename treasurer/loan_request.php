<?php include 'header.php'; ?>

  <!-- start page content -->
  <div class="page-content-wrapper" ng-controller="LoanReqCtrl" ng-cloak>
    <div class="page-content">
      <div class="page-bar">
        <div class="page-title-breadcrumb">
          <div class=" pull-left">
            <div class="page-title">Loan Requests</div>
          </div>
          <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Loan Requests</li>
          </ol>
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-12">
        <div class="card card-topline-green">
          <div class="card-head">
            <header>AUDIT TRAIL</header>
            <div class="tools">
              <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
            </div>
          </div>
          <div class="card-body ">
            <div class="row p-b-20">
              <div class="col-md-6 col-sm-6 col-6">
              </div>
              <div class="col-md-6 col-sm-6 col-6">
                <div class="btn-group pull-right">
                  <button class="btn deepPink-bgcolor  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                  <i class="fa fa-angle-down"></i>
                  </button>
                  <ul class="dropdown-menu pull-right">
                    <li>
                      <a href="javascript:;" ng-click="print()">
                      <i class="fa fa-print"></i> Print </a>
                    </li>
                    <li>
                      <a href="javascript:;">
                      <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                    </li>
                    <li>
                      <a href="javascript:;">
                      <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="table-responsive">
              <table id="order_table1" datatable="ng" dt-options="vm.dtOptions" class="table table-striped table-bordered table-hover table-checkable order-column full-width">
                <thead>
               
                  <tr>
                    <th> Name </th>
                    <th> Season No. </th>
                    <th> Co-maker </th>
                    <th> Amount</th>
                    <th> Date </th>
                    <th> Status </th>
                    <th> Action </th>
                  </tr>
                </thead>
                <tbody>
                
                  <tr class="odd gradeX" ng-repeat="data in getAllReqData">
                    <td> {{data.first_name + ' ' +data.sur_name}} </td>
                    <td>
                    {{data.season_id}}
                    </td>
                    <td>
                    {{data.comaker.first_name + " " + data.comaker.sur_name}}
                    </td>
                    <td>
                    {{data.amount}}
                    </td>
                    <td>
                    {{data.date_created}}
                    </td>
                    <td>
                      <span ng-if="data.treasurer_status==null" class="label label-sm label-danger">Pending</span>
                      <span ng-if="data.treasurer_status==1" class="label label-sm label-success">Approved</span>
                      <span ng-if="data.treasurer_status==0" class="label label-sm label-danger">Declined</span>
                    </td>
                    <td>
                      <a ng-if="data.treasurer_status==null" ng-click="acceptLoanReq(data.id,data.user_id)" href="" class="btn btn-tbl-edit btn-xs btn-success tooltips" data-placement="top" data-original-title="Approve">
                      <i class="material-icons">thumb_up</i>
                      </a>
                      <a ng-if="data.treasurer_status==null" ng-click="rejectLoanReq(data.id,data.user_id)" href="" class="btn btn-tbl-edit btn-xs btn-danger tooltips" data-placement="top" data-original-title="Reject">
                      <i class="material-icons">thumb_down</i>
                      </a>
                      <a ng-if="data.treasurer_status!=null" href="" style="visibility: hidden;" class="btn btn-tbl-edit btn-xs btn-danger tooltips" data-placement="top" data-original-title="Reject">
                        <i class="material-icons">thumb_down</i>
                      </a>
                    </td>
                  </tr>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end page content -->

<?php include 'footer.php'; ?>

<script type="text/javascript">
  $("#requests").addClass("active");
  
</script>