<?php include 'header.php'; ?>

  <!-- start page content -->
  <div class="page-content-wrapper" ng-controller="dashboardCtrl" ng-cloak>
    <div class="page-content">
      <div class="page-bar">
        <div class="page-title-breadcrumb">
          <div class=" pull-left">
            <div class="page-title">Dashboard</div>
          </div>
          <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Dashboard</li>
          </ol>
        </div>
      </div>
      <!-- start widget -->
      <div class="state-overview">
        <div class="row">
          <div class="col-sm-4">
            <div class="overview-panel purple">
              <div class="symbol">
                <i class="fa fa-users usr-clr"></i>
              </div>
              <div class="value white">
                <p class="sbold addr-font-h1">{{numOfMembers}}</p>
                <p>Members</p>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="overview-panel deepPink-bgcolor">
              <div class="symbol">
                <i class="fa fa-key"></i>
              </div>
              <div class="value white">
                <p class="sbold addr-font-h1">{{numOfadmin}}</p>
                <p>Administrator</p>
              </div>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="overview-panel orange">
              <div class="symbol">
                <i class="fa fa-briefcase"></i>
              </div>
              <div class="value white">
                <p class="sbold addr-font-h1">{{numOftreasurer}}</p>
                <p>Treasurer</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- end widget -->
      
      <div class="row">
        <div class="col-md-12">
        <div class="card card-topline-green">
          <div class="card-head">
            <header>AUDIT TRAIL</header>
            <div class="tools">
              <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
            </div>
          </div>
          <div class="card-body ">
            <div class="row p-b-20">
              <div class="col-md-6 col-sm-6 col-6">
              </div>
              <div class="col-md-6 col-sm-6 col-6">
                <div class="btn-group pull-right">
                  <button class="btn deepPink-bgcolor  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                  <i class="fa fa-angle-down"></i>
                  </button>
                  <ul class="dropdown-menu pull-right">
                    <li>
                      <a href="javascript:;" ng-click="print()">
                      <i class="fa fa-print"></i> Print </a>
                    </li>
                    <li>
                      <a href="javascript:;">
                      <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                    </li>
                    <li>
                      <a href="javascript:;">
                      <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="table-responsive">
              <table id="order_table1" datatable="ng" dt-options="vm.dtOptions" class="table table-striped table-bordered table-hover table-checkable order-column full-width">
                <thead>
               
                  <tr>
                    <th> Name </th>
                    <th> Role </th>
                    <th> Actions </th>
                    <th> Date </th>
                  </tr>
                </thead>
                <tbody>
                
                  <tr class="odd gradeX" ng-repeat="data in UserLogDatas">
                    <td> {{data.first_name}} </td>
                    <td>
                    {{data.role}}
                    </td>
                    <td>
                    {{data.message}}
                    </td>
                    <td>
                    {{data.date_created}}
                    </td>
                  </tr>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end page content -->

<?php include 'footer.php'; ?>

<script type="text/javascript">
  $("#dashboard").addClass("active");
  
</script>