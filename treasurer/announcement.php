<?php include 'header.php'; ?>
  <!-- start page content -->
  <div class="page-content-wrapper" ng-controller="announcementCtrl" ng-cloak>
    <div class="page-content" ng-if="showReadMore == ''">
      <div class="page-bar">
        <div class="page-title-breadcrumb">
          <div class=" pull-left">
            <div class="page-title">Announcements</div>
          </div>
          <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Announcements</li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6" ng-repeat="data in allAnnouncementData"> 
          <div class="blogThumb">
            <div class="white-box">
              <div class="row">
                <div class="col-sm-3">
                  <div class="thumb-center">
                  	<img class="img-responsive" alt="user" src="../assets/uploads/{{data.image}}">
                  </div>
                </div>
                <div class="col-sm-9">
                  <div class="text-muted"><span class="m-r-10">{{data.date_created}}</span> 
                  </div>
                  <h3 class="m-t-20 m-b-20">{{data.title}}</h3>
                  <button ng-click="ReadMore(data.id)" class="btn btn-success btn-rounded waves-effect waves-light m-t-20 float-right">Read more</button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- <div class="col-md-6"> 
          <div class="blogThumb">
            <div class="white-box">
              <div class="row">
                <div class="col-sm-3">
                  <div class="thumb-center">
                  	<img class="img-responsive" alt="user" src="../assets/img/dp.jpg">
                  </div>
                </div>
                <div class="col-sm-9">
                  <div class="text-muted"><span class="m-r-10">June 16</span> 
                    <a class="text-muted m-l-10" href="#"><i class="fa fa-heart-o"></i> 56</a>
                  </div>
                  <h3 class="m-t-20 m-b-20">White Woman Practices Yoga In</h3>
                  <p>There is a new neighbor on Sesame Street. Her name is Julia </p>
                  <button onclick="window.location.href='view_announcement.php'" class="btn btn-success btn-rounded waves-effect waves-light m-t-20 float-right">Read more</button>
                </div>
              </div>
            </div>
          </div>
        </div> -->
      </div>
    </div>
    <div class="page-content"  ng-if="showReadMore != ''">
      <div class="page-bar">
        <div class="page-title-breadcrumb">
          <div class=" pull-left">
            <div class="page-title">Announcements</div>
          </div>
          <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Announcements</li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12"> 
          <div class="blogThumb">
            <div class="white-box">
              <button ng-click="CloseReadMore()" class="btn btn-success btn-rounded waves-effect waves-light m-t-20 float-right">Close</button>
              <div class="row">
                <div class="col-sm-3">
                  <div class="thumb-center">
                  	<img class="img-responsive" alt="user" src="../assets/uploads/{{AnnouncementData.image}}">
                  </div>
                </div>
                <div class="col-sm-9">
                  <div class="text-muted"><span class="m-r-10">{{AnnouncementData.date_created}}</span> 
                  </div>
                  <h3 class="m-t-20 m-b-20">{{AnnouncementData.title}}</h3>
                  <p>{{AnnouncementData.descriptions}}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end page content -->

<?php include 'footer.php'; ?>

<script type="text/javascript">
  $("#announcement").addClass("active");
</script>