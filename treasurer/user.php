<?php include 'header.php'; ?>
<style type="text/css">
  .place { width: 12px; height: 12px; background-color: green;  border-radius: 50%;}
  .place.green { background-color: red; }
</style>




<!-- start page content -->
<div class="page-content-wrapper" ng-controller="userCtrl" ng-cloak>

  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content card-box">
        <div class="modal-body">
          <div class="row">
            <div class="col-sm-12">
              
              <div class="card-head">
                <header>Contribution</header>
              </div>
              <div class="card-body row">
                <div class="col-lg-6 p-t-20">
                  <div class="form-group">
                    <label>Employee ID</label>
                    <input type="text" class="form-control" placeholder="Enter employee ID" ng-model="userDetails.id" disabled>
                  </div>
                </div>
                <div class="col-lg-6 p-t-20">
                  <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" placeholder="Enter email" ng-model="userDetails.email">
                  </div>
                </div>
                <div class="col-lg-6 p-t-20">
                  <div class="form-group">
                    <label>First Name</label>
                    <input type="text" class="form-control" placeholder="Enter first name" ng-model="userDetails.fname">
                  </div>
                </div>
                <div class="col-lg-6 p-t-20">
                  <div class="form-group">
                    <label>Last Name</label>
                    <input type="text" class="form-control" placeholder="Enter last name" ng-model="userDetails.lname">
                  </div>
                </div>
                <div class="col-lg-12 p-t-20">
                  <div class="form-group">
                    <label>Phone</label>
                    <input type="number" class="form-control" placeholder="Enter phone number" ng-model="userDetails.phone">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink" ng-click="saveUpdateUser()">Save changes</button>
        </div>
      </div>
    </div>
  </div>
  <div class="page-content" ng-if="addNewMember ==0">
    <div class="page-bar">
      <div class="page-title-breadcrumb">
        <div class=" pull-left">
          <div class="page-title">Users</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
          </li>
          <li class="active">Users</li>
        </ol>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card card-topline-green">
          <div class="card-head">
            <header>List of Members</header>
            <div class="tools">
              <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>

            </div>
          </div>
          <div class="card-body ">
            <div class="row p-b-20">
              <div class="col-md-6 col-sm-6 col-6">
                <div class="btn-group">
                  <button id="addRow1" class="btn btn-info" ng-click="addButton()">
                  Add New Member<i class="fa fa-plus"></i>
                  </button>
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-6">
                <div class="btn-group pull-right">
                  <button class="btn deepPink-bgcolor  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                  <i class="fa fa-angle-down"></i>
                  </button>
                  <ul class="dropdown-menu pull-right">
                    <li>
                      <a href="javascript:;" ng-click="print()">
                      <i class="fa fa-print"></i> Print </a>
                    </li>
                    <li>
                      <a href="javascript:;">
                      <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                    </li>
                    <li>
                      <a href="javascript:;">
                      <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            
            <div class="table-responsive">
              <table id="order_table1" datatable="ng" dt-options="vm.dtOptions" class="table table-striped table-bordered table-hover table-checkable order-column full-width">
                <thead>
                  <tr>
                    <th> Status </th>
                    <th> First Name </th>
                    <th> Last Name </th>
                    <th> Email </th>
                    <th> Joined </th>
                    <th> Actions </th>
                  </tr>
                </thead>
                <tbody>
                    <tr class="odd gradeX" ng-repeat="data in userData">
                      <td>
                        <center><div class="place" ng-if="data.status==1" ng-click="activedeactivateStatus(data.id,data.status)"></div>
                        <div class="place" ng-if="data.status==0" style="background-color:red" ng-click="activedeactivateStatus(data.id,data.status)"></div></center>
                      </td> 
                      <td> {{data.first_name}} </td>
                      <td> {{data.sur_name}} </td>
                      <td>
                        <a href="mailto:{{data.email}}"> {{data.email}}</a>
                      </td>
                      <td>{{data.date_created}} </td>
                      <td class="valigntop">
                        <button class="btn btn-primary btn-xs tooltips" data-placement="top" data-original-title="Edit" data-toggle="modal" data-target="#exampleModal" ng-click="updateUser(data.id,data.first_name,data.sur_name,data.email,data.mobile_number)">
                          <i class="fa fa-pencil"></i>
                        </button>
                      </td>
                    </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="page-content" ng-if=" addNewMember ==1">
    <div class="page-bar">
      <div class="page-title-breadcrumb">
        <div class=" pull-left">
          <div class="page-title">Add Member Details</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
          </li>
          <li><a class="parent-item" href="user.php">Users</a>&nbsp;<i class="fa fa-angle-right"></i>
          </li>
          <li class="active">Add Member Details</li>
        </ol>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="card-box">
          <div class="card-head">
            <header>Basic Information</header>
          </div>
          <div class="card-body row">
            <div class="col-lg-6 p-t-20">
              <div class="form-group">
                <label>Employee ID</label>
                <input type = "text" ng-model="NewMember.empId" class="form-control" placeholder="Employee ID">
              </div>
            </div>
            <div class="col-lg-6 p-t-20">
              <div class="form-group">
                <label>Email</label>
                <input type = "email" ng-model="NewMember.email" class="form-control" placeholder="Email">
              </div>
            </div>
            <div class="col-lg-6 p-t-20">
              <div class="form-group">
                <label>First Name</label>
                <input type = "text" ng-model="NewMember.fname" class="form-control" placeholder="First Name">
              </div>
            </div>
            <div class="col-lg-6 p-t-20">
              <div class="form-group">
                <label>Middle Name</label>
                <input type = "text" ng-model="NewMember.mname" class="form-control" placeholder="Middle Name">
              </div>
            </div>
            <div class="col-lg-6 p-t-20">
              <div class="form-group">
                <label>Last Name</label>
                <input type = "text" ng-model="NewMember.lname" class="form-control" placeholder="Last Name">
              </div>
            </div>
            <div class="col-lg-6 p-t-20">
              <div class="form-group">
                <label>Mobile Number</label>
                <input type = "number" ng-model="NewMember.phone" class="form-control" placeholder="Mobile Number" pattern = "-?[0-9]*(\.[0-9]+)?">
              </div>
            </div>
            <div class="col-lg-6 p-t-20">
              <div class="form-group">
                <label>Password</label>
                <input type = "password" ng-model="NewMember.password" class="form-control" placeholder="Password">
              </div>
            </div>
            <div class="col-lg-6 p-t-20">
              <div class="form-group">
                <label>Confirm Password</label>
                <input type = "password" ng-model="NewMember.confirmPass" class="form-control" placeholder="Confirm Password">
              </div>
            </div>
            
            <div class="col-lg-12 p-t-20 text-center"> 
              <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink" ng-click="addNewMembers()">Submit</button>
              <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default" ng-click="cancelButton()">Cancel</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end page content -->
</div>
<!-- end page container -->
<?php include 'footer.php'; ?>
<script type="text/javascript">
  $("#users").addClass("active");
  
  

// $(".place").click(function () {
//    $(this).toggleClass("green");
// });
</script>