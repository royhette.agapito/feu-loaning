<div class="page-footer">
            <div class="page-footer-inner"> 2019 &copy; CCS Savings and Loaning System By
            <a href="mailto:redstartheme@gmail.com" target="_top" class="makerCss">FEU Tech</a>
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- end footer -->
    </div>
    <!-- start js include path -->

    <script src="../assets/plugins/jquery/jquery.min.js" ></script>
    
    <script src="../assets/plugins/popper/popper.min.js" ></script>
    <script src="../assets/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
    <script src="../assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../assets/plugins/jquery-ui/jquery-ui.min.js" ></script>
    <script src="../assets/plugins/counterup/jquery.waypoints.min.js" ></script>
    <script src="../assets/plugins/counterup/jquery.counterup.min.js" ></script>
    <!-- bootstrap -->
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <!-- dropzone -->
    <script src="../assets/plugins/dropzone/dropzone.js" ></script>
    <script src="../assets/plugins/dropzone/dropzone-call.js" ></script>
    <!-- morris chart -->
    <script src="../assets/plugins/morris/morris.min.js" ></script>
    <script src="../assets/plugins/morris/raphael-min.js" ></script>
    <script src="../assets/js/pages/chart/morris/morris_home_data.js" ></script>
    <!-- calendar -->
    <script src="../assets/plugins/moment/moment.min.js" ></script>
    <script src="../assets/plugins/fullcalendar/fullcalendar.min.js" ></script>
    <script src="../assets/js/pages/calendar/calendar.min.js" ></script>
    <!-- data tables -->
    <script src="../assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="../assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
    <script src="../assets/js/pages/table/table_data.js" ></script>
    <!-- Common js-->
    <script src="../assets/js/app.js" ></script>
    <script src="../assets/js/layout.js" ></script>
    <script src="../assets/js/theme-color.js" ></script>
    <!-- Material -->
    <script src="../assets/plugins/material/material.min.js"></script>
    <script src="../assets/js/pages/material_select/getmdl-select.js" ></script>
    <script  src="../assets/plugins/material-datetimepicker/moment-with-locales.min.js"></script>
    <script  src="../assets/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"></script>
    <script  src="../assets/plugins/material-datetimepicker/datetimepicker.js"></script>
    <!-- animation -->
    <script src="../assets/js/pages/ui/animations.js" ></script>
    <!-- end js include path -->
    <script src="../assets/js/angular.min.js" ></script>
    <script src="../assets/js/angular-datatables.min.js"></script>
    <script src="../assets/js/jquery.dataTables.min.js"></script>
    <script src="../assets/js/angular-animate.min.js" ></script>
    <script src="../assets/js/angular-cookies.js" ></script>
    <script src="../assets/js/sweetalert.min.js" ></script>
    <script src="../assets/js/pages/sweet-alert/sweet-alert-data.js" ></script>
    <script type="text/javascript">
    	var app = angular.module("myApp", ['ngCookies','datatables']);
		app.controller('dashboardCtrl', function($scope, $http, $filter, $timeout, $cookies){
            $http.post('controller/getallmembers.php').then(function(response){
                $scope.numOfMembers = parseInt(response.data);
            });
            $http.post('controller/getalladmin.php').then(function(response){
                $scope.numOfadmin = parseInt(response.data);
            });
            $http.post('controller/getallTreasurer.php').then(function(response){
                $scope.numOftreasurer = parseInt(response.data);
            });
            $http.post('controller/getUserLog.php').then(function(response){
                $scope.UserLogDatas = response.data;
            });
            $scope.print = function(){
                var divToPrint=document.getElementById("order_table1");
                newWin= window.open("");
                newWin.document.write(divToPrint.outerHTML);
                newWin.print();
                newWin.close();
            }
        });
        app.controller('headerCtrl', function($scope, $http, $filter, $timeout, $cookies){
            $http.post('controller/getPendingTransaction.php').then(function(response){
                $scope.PendingTransac = parseInt(response.data);
                console.log(response.data);
            });
        });
        app.controller('announcementCtrl', function($scope, $http, $filter, $timeout, $cookies){
            $scope.showReadMore = '';
            $http.post('controller/announcement.php?getAll').then(function(response){
                $scope.allAnnouncementData = response.data;
            });
            $scope.ReadMore = function(id){
                $http.post('controller/announcement.php?getReadAnnouncement',{id:id}).then(function(response){
                    $scope.AnnouncementData = response.data;
                });
                $scope.showReadMore = "read";
            }
            $scope.CloseReadMore = function(){
                $scope.showReadMore = '';
            }
        });
        app.controller('savingsCtrl', function($scope, $http, $filter, $timeout, $cookies){
            $scope.viewseasons = ''; 
            $scope.seasonId = '';
            $scope.paymentOption={
                options:'',
            };
            $scope.checkpaymentStatus = {
                first:'',
                second:'',
                Third:'',
                Fourth:'',
                fifth:'',
                sixth:'',
            }
            $scope.getSelectedMemberData = {
                user_id:'',
                fname:'',
                lname:'',
                shares:'',
                amount:'',
                remarks:'',
                totalDays:'',

            }
            $scope.print = function(){
                var divToPrint=document.getElementById("order_table1");
                newWin= window.open("");
                newWin.document.write(divToPrint.outerHTML);
                newWin.print();
                newWin.close();
            }
            $scope.getAllSeason = function (){
                $http.post('controller/getAllSeason.php').then(function(response){
                    $scope.allSeasonData = response.data;
                });
            }
            $scope.getAllSeason();
            $scope.ViewSeason = function(id){
                $scope.viewseasons = 'read'; 
                $scope.seasonId = id;
                $scope.getAllJoinedSeason();
            }
            $scope.getAllJoinedSeason = function(){
                $http.post('controller/getAllJoinedSeason.php',{id: $scope.seasonId}).then(function(response){
                    $scope.getAllJoinedSeasonData = response.data;
                    console.log(response);
                });
            }
            $scope.getAllTransaction = function (){
                $http.post('controller/getAllTransactionSeasons.php',{id: $scope.seasonId}).then(function(response){
                    $scope.getAllTransactionData = response.data;
                });
            }
            $scope.getSelectedMember = function(user_id,fname,lname,shares){
                $scope.getSelectedMemberData.user_id = user_id;
                $scope.getSelectedMemberData.fname = fname;
                $scope.getSelectedMemberData.lname = lname;
                $scope.getSelectedMemberData.shares = shares;
                $scope.onChangeSelectedPayment();
            }
            $scope.onChangeSelectedPayment = function(){
                $http.post('controller/getSeasonData.php',{id: $scope.seasonId,userId:$scope.getSelectedMemberData.user_id}).then(function(response){
                    try {
                        var index = response.data.checkTblTransac.findIndex(x=>x.remarks=="First Payment");
                        var index1 = response.data.checkTblTransac.findIndex(x=>x.remarks=="Second Payment");
                        var index2 = response.data.checkTblTransac.findIndex(x=>x.remarks=="Third Payment");
                        var index3 = response.data.checkTblTransac.findIndex(x=>x.remarks=="Fourth Payment");
                        var index4 = response.data.checkTblTransac.findIndex(x=>x.remarks=="Fifth Payment");
                        var index5 = response.data.checkTblTransac.findIndex(x=>x.remarks=="Sixth Payment");
                        if((index<0) && (index1<0) &&(index2<0) && (index3<0) && (index4<0) && (index5<0)){
                            $scope.checkpaymentStatus.first = '';
                            $scope.checkpaymentStatus.second = 'meron';
                            $scope.checkpaymentStatus.Third = 'meron';
                            $scope.checkpaymentStatus.Fourth = 'meron';
                            $scope.checkpaymentStatus.fifth = 'meron';
                            $scope.checkpaymentStatus.sixth = 'meron';
                        }else if((index>=0) && (index1<0) &&(index2<0) && (index3<0) && (index4<0) && (index5<0)){
                            $scope.checkpaymentStatus.first = 'meron';
                            $scope.checkpaymentStatus.second = '';
                            $scope.checkpaymentStatus.Third = 'meron';
                            $scope.checkpaymentStatus.Fourth = 'meron';
                            $scope.checkpaymentStatus.fifth = 'meron';
                            $scope.checkpaymentStatus.sixth = 'meron';
                        }else if((index>=0) && (index1>=0) &&(index2<0) && (index3<0) && (index4<0) && (index5<0)){
                            $scope.checkpaymentStatus.first = 'meron';
                            $scope.checkpaymentStatus.second = 'meron';
                            $scope.checkpaymentStatus.Third = '';
                            $scope.checkpaymentStatus.Fourth = 'meron';
                            $scope.checkpaymentStatus.fifth = 'meron';
                            $scope.checkpaymentStatus.sixth = 'meron';
                        }else if((index>=0) && (index1>=0) &&(index2>=0) && (index3<0) && (index4<0) && (index5<0)){
                            $scope.checkpaymentStatus.first = 'meron';
                            $scope.checkpaymentStatus.second = 'meron';
                            $scope.checkpaymentStatus.Third = 'meron';
                            $scope.checkpaymentStatus.Fourth = '';
                            $scope.checkpaymentStatus.fifth = 'meron';
                            $scope.checkpaymentStatus.sixth = 'meron';
                        }else if((index>=0) && (index1>=0) &&(index2>=0) && (index3>=0) && (index4<0) && (index5<0)){
                            $scope.checkpaymentStatus.first = 'meron';
                            $scope.checkpaymentStatus.second = 'meron';
                            $scope.checkpaymentStatus.Third = 'meron';
                            $scope.checkpaymentStatus.Fourth = 'meron';
                            $scope.checkpaymentStatus.fifth = '';
                            $scope.checkpaymentStatus.sixth = 'meron';
                        }else if((index>=0) && (index1>=0) &&(index2>=0) && (index3>=0) && (index4>=0) && (index5<0)){
                            $scope.checkpaymentStatus.first = 'meron';
                            $scope.checkpaymentStatus.second = 'meron';
                            $scope.checkpaymentStatus.Third = 'meron';
                            $scope.checkpaymentStatus.Fourth = 'meron';
                            $scope.checkpaymentStatus.fifth = 'meron';
                            $scope.checkpaymentStatus.sixth = '';
                        }else if((index>=0) && (index1>=0) &&(index2>=0) && (index3>=0) && (index4>=0) && (index5>=0)){
                            $scope.checkpaymentStatus.first = 'meron';
                            $scope.checkpaymentStatus.second = 'meron';
                            $scope.checkpaymentStatus.Third = 'meron';
                            $scope.checkpaymentStatus.Fourth = 'meron';
                            $scope.checkpaymentStatus.fifth = 'meron';
                            $scope.checkpaymentStatus.sixth = 'meron';
                        }
                    } catch (error) {
                        console.log(error);
                    }
                    if((response.data.totalDays>=1) && (response.data.totalDays<=30)){
                        $scope.getSelectedMemberData.totalDays =1;
                        if($scope.paymentOption.options =='0'){
                            if(response.data.firstpaymentDateEnd<=0){
                                $scope.getSelectedMemberData.remarks="First Payment";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares * response.data.amount_per_share;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * " + response.data.amount_per_share + "amount";
                            }else if((response.data.firstpaymentDateEnd>0) && (response.data.firstpaymentDateEnd<=7)){
                                $scope.getSelectedMemberData.remarks="First Payment";
                                var ewq = response.data.amount_per_share * .01;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.firstpaymentDateEnd>7) && (response.data.firstpaymentDateEnd<=14)){
                                $scope.getSelectedMemberData.remarks="First Payment";
                                var ewq = response.data.amount_per_share * .02;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.firstpaymentDateEnd>14) && (response.data.firstpaymentDateEnd<=21)){
                                $scope.getSelectedMemberData.remarks="First Payment";
                                var ewq = response.data.amount_per_share * .03;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }
                        }else if($scope.paymentOption.options =='1'){
                            if(response.data.secpaymentDateEnd<=0){
                                $scope.getSelectedMemberData.remarks="Second Payment";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares * response.data.amount_per_share;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * " + response.data.amount_per_share + "amount";
                            }else if((response.data.secpaymentDateEnd>0) && (response.data.secpaymentDateEnd<=7)){
                                $scope.getSelectedMemberData.remarks="Second Payment";
                                var ewq = response.data.amount_per_share * .01;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.secpaymentDateEnd>7) && (response.data.secpaymentDateEnd<=14)){
                                $scope.getSelectedMemberData.remarks="Second Payment";
                                var ewq = response.data.amount_per_share * .02;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.secpaymentDateEnd>14) && (response.data.secpaymentDateEnd<=21)){
                                $scope.getSelectedMemberData.remarks="Second Payment";
                                var ewq = response.data.amount_per_share * .03;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }
                        }
                    }else if((response.data.totalDays>=31) && (response.data.totalDays<=60)){
                        $scope.getSelectedMemberData.totalDays =2;
                        if($scope.paymentOption.options =='0'){
                            if(response.data.firstpaymentDateEnd<=0){
                                $scope.getSelectedMemberData.remarks="First Payment";
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * " + response.data.amount_per_share + "amount";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares * response.data.amount_per_share;
                            }else if((response.data.firstpaymentDateEnd>0) && (response.data.firstpaymentDateEnd<=7)){
                                $scope.getSelectedMemberData.remarks="First Payment";
                                var ewq = response.data.amount_per_share * .01;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.firstpaymentDateEnd>7) && (response.data.firstpaymentDateEnd<=14)){
                                $scope.getSelectedMemberData.remarks="First Payment";
                                var ewq = response.data.amount_per_share * .02;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.firstpaymentDateEnd>14) && (response.data.firstpaymentDateEnd<=21)){
                                $scope.getSelectedMemberData.remarks="First Payment";
                                var ewq = response.data.amount_per_share * .03;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }
                        }else if($scope.paymentOption.options =='1'){
                            if(response.data.secpaymentDateEnd<=0){
                                $scope.getSelectedMemberData.remarks="Second Payment";
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * " + response.data.amount_per_share + "amount";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares * response.data.amount_per_share;
                            }else if((response.data.secpaymentDateEnd>0) && (response.data.secpaymentDateEnd<=7)){
                                $scope.getSelectedMemberData.remarks="Second Payment";
                                var ewq = response.data.amount_per_share * .01;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.secpaymentDateEnd>7) && (response.data.secpaymentDateEnd<=14)){
                                $scope.getSelectedMemberData.remarks="Second Payment";
                                var ewq = response.data.amount_per_share * .02;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.secpaymentDateEnd>14) && (response.data.secpaymentDateEnd<=21)){
                                $scope.getSelectedMemberData.remarks="Second Payment";
                                var ewq = response.data.amount_per_share * .03;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }
                        }else if($scope.paymentOption.options =='2'){
                            if(response.data.thirdpaymentDateEnd<=0){
                                $scope.getSelectedMemberData.remarks="Third Payment";
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * " + response.data.amount_per_share + "amount";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares * response.data.amount_per_share;
                            }else if((response.data.thirdpaymentDateEnd>0) && (response.data.thirdpaymentDateEnd<=7)){
                                $scope.getSelectedMemberData.remarks="Third Payment";
                                var ewq = response.data.amount_per_share * .01;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.thirdpaymentDateEnd>7) && (response.data.thirdpaymentDateEnd<=14)){
                                $scope.getSelectedMemberData.remarks="Third Payment";
                                var ewq = response.data.amount_per_share * .02;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.thirdpaymentDateEnd>14) && (response.data.thirdpaymentDateEnd<=21)){
                                $scope.getSelectedMemberData.remarks="Third Payment";
                                var ewq = response.data.amount_per_share * .03;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }
                        }else if($scope.paymentOption.options  =='3'){
                            if(response.data.fourthpaymentDateEnd<=0){
                                $scope.getSelectedMemberData.remarks="Fourth Payment";
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * " + response.data.amount_per_share + "amount";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares * response.data.amount_per_share;
                            }else if((response.data.fourthpaymentDateEnd>0) && (response.data.fourthpaymentDateEnd<=7)){
                                $scope.getSelectedMemberData.remarks="Fourth Payment";
                                var ewq = response.data.amount_per_share * .01;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.fourthpaymentDateEnd>7) && (response.data.fourthpaymentDateEnd<=14)){
                                $scope.getSelectedMemberData.remarks="Fourth Payment";
                                var ewq = response.data.amount_per_share * .02;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.fourthpaymentDateEnd>14) && (response.data.fourthpaymentDateEnd<=21)){
                                $scope.getSelectedMemberData.remarks="Fourth Payment";
                                var ewq = response.data.amount_per_share * .03;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }
                        }
                    }else if((response.data.totalDays>=61) && (response.data.totalDays<90)){
                        $scope.getSelectedMemberData.totalDays =3;
                        if($scope.paymentOption.options =='0'){
                            if(response.data.firstpaymentDateEnd<=0){
                                $scope.getSelectedMemberData.remarks="First Payment";
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * " + response.data.amount_per_share + "amount";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares * response.data.amount_per_share;
                            }else if((response.data.firstpaymentDateEnd>0) && (response.data.firstpaymentDateEnd<=7)){
                                $scope.getSelectedMemberData.remarks="First Payment";
                                var ewq = response.data.amount_per_share * .01;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.firstpaymentDateEnd>7) && (response.data.firstpaymentDateEnd<=14)){
                                $scope.getSelectedMemberData.remarks="First Payment";
                                var ewq = response.data.amount_per_share * .02;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.firstpaymentDateEnd>14) && (response.data.firstpaymentDateEnd<=21)){
                                $scope.getSelectedMemberData.remarks="First Payment";
                                var ewq = response.data.amount_per_share * .03;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }
                        }else if($scope.paymentOption.options  =='1'){
                            if(response.data.secpaymentDateEnd<=0){
                                $scope.getSelectedMemberData.remarks="Second Payment";
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * " + response.data.amount_per_share + "amount";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares * response.data.amount_per_share;
                            }else if((response.data.secpaymentDateEnd>0) && (response.data.secpaymentDateEnd<=7)){
                                $scope.getSelectedMemberData.remarks="Second Payment";
                                var ewq = response.data.amount_per_share * .01;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.secpaymentDateEnd>7) && (response.data.secpaymentDateEnd<=14)){
                                $scope.getSelectedMemberData.remarks="Second Payment";
                                var ewq = response.data.amount_per_share * .02;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.secpaymentDateEnd>14) && (response.data.secpaymentDateEnd<=21)){
                                $scope.getSelectedMemberData.remarks="Second Payment";
                                var ewq = response.data.amount_per_share * .03;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }
                        }else if($scope.paymentOption.options  =='2'){
                            if(response.data.thirdpaymentDateEnd<=0){
                                $scope.getSelectedMemberData.remarks="Third Payment";
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * " + response.data.amount_per_share + "amount";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares * response.data.amount_per_share;
                            }else if((response.data.thirdpaymentDateEnd>0) && (response.data.thirdpaymentDateEnd<=7)){
                                $scope.getSelectedMemberData.remarks="Third Payment";
                                var ewq = response.data.amount_per_share * .01;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.thirdpaymentDateEnd>7) && (response.data.thirdpaymentDateEnd<=14)){
                                $scope.getSelectedMemberData.remarks="Third Payment";
                                var ewq = response.data.amount_per_share * .02;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.thirdpaymentDateEnd>14) && (response.data.thirdpaymentDateEnd<=21)){
                                $scope.getSelectedMemberData.remarks="Third Payment";
                                var ewq = response.data.amount_per_share * .03;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }
                        }else if($scope.paymentOption.options  =='3'){
                            if(response.data.fourthpaymentDateEnd<=0){
                                $scope.getSelectedMemberData.remarks="Fourth Payment";
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * " + response.data.amount_per_share + "amount";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares * response.data.amount_per_share;
                            }else if((response.data.fourthpaymentDateEnd>0) && (response.data.fourthpaymentDateEnd<=7)){
                                $scope.getSelectedMemberData.remarks="Fourth Payment";
                                var ewq = response.data.amount_per_share * .01;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.fourthpaymentDateEnd>7) && (response.data.fourthpaymentDateEnd<=14)){
                                $scope.getSelectedMemberData.remarks="Fourth Payment";
                                var ewq = response.data.amount_per_share * .02;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.fourthpaymentDateEnd>14) && (response.data.fourthpaymentDateEnd<=21)){
                                $scope.getSelectedMemberData.remarks="Fourth Payment";
                                var ewq = response.data.amount_per_share * .03;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }
                        }else if($scope.paymentOption.options  =='4'){
                            if(response.data.fifthpaymentDateEnd<=0){
                                $scope.getSelectedMemberData.remarks="Fifth Payment";
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * " + response.data.amount_per_share + "amount";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares * response.data.amount_per_share;
                            }else if((response.data.fifthpaymentDateEnd>0) && (response.data.fifthpaymentDateEnd<=7)){
                                $scope.getSelectedMemberData.remarks="Fifth Payment";
                                var ewq = response.data.amount_per_share * .01;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.fifthpaymentDateEnd>7) && (response.data.fifthpaymentDateEnd<=14)){
                                $scope.getSelectedMemberData.remarks="Fifth Payment";
                                var ewq = response.data.amount_per_share * .02;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.fifthpaymentDateEnd>14) && (response.data.fifthpaymentDateEnd<=21)){
                                $scope.getSelectedMemberData.remarks="Fifth Payment";
                                var ewq = response.data.amount_per_share * .03;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }
                        }else if($scope.paymentOption.options  =='5'){
                            if(response.data.sixthpaymentDateEnd<=0){
                                $scope.getSelectedMemberData.remarks="Sixth Payment";
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * " + response.data.amount_per_share + "amount";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares * response.data.amount_per_share;
                            }else if((response.data.sixthpaymentDateEnd>0) && (response.data.sixthpaymentDateEnd<=7)){
                                $scope.getSelectedMemberData.remarks="Sixth Payment";
                                var ewq = response.data.amount_per_share * .01;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.sixthpaymentDateEnd>7) && (response.data.sixthpaymentDateEnd<=14)){
                                $scope.getSelectedMemberData.remarks="Sixth Payment";
                                var ewq = response.data.amount_per_share * .02;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }else if((response.data.sixthpaymentDateEnd>14) && (response.data.sixthpaymentDateEnd<=21)){
                                $scope.getSelectedMemberData.remarks="Sixth Payment";
                                var ewq = response.data.amount_per_share * .03;
                                $scope.breakdown = $scope.getSelectedMemberData.shares+" share * ( " + response.data.amount_per_share + "amount + " +ewq+ " penalty )";
                                $scope.getSelectedMemberData.amount=$scope.getSelectedMemberData.shares *(response.data.amount_per_share+ewq);
                            }
                        }
                    }
                });
            }
            $scope.pay = function(){
                if($scope.paymentOption.options == null){
                    swal("select Payment");
                }else if($scope.paymentOption.options!= null){
                    $http.post('controller/insertTransaction.php',{user_id: $scope.getSelectedMemberData.user_id,seasonId:$scope.seasonId,amount:$scope.getSelectedMemberData.amount,remarks:$scope.getSelectedMemberData.remarks}).then(function(response){
                        if(response.data == 'true'){
                            $('#exampleModal').modal('hide');
                            $scope.paymentOption.options = '';
                            swal("Successfully Ordered", {icon: "success",});
                        }else{
                            swal('error');
                        }
                        $scope.getAllTransaction();
                    });
                }else{
                    swal("error");
                }
                
            }
        });
        app.controller('loanCtrl', function($scope, $http, $filter, $timeout, $cookies){
            $scope.viewseasons = ''; 
            $scope.PayForData = '';
            $scope.seasonId = '';
            $scope.getApplyLoanDetails = {
                id:'',
                fname:'',
                lname:'',
                amount:'',
                comaker:'',
            }
            $scope.getSelectedMemberData = {
                user_id:'',
                fname:'',
                lname:'',
                amount:'',
                remarks:'',
                totalDays:'',

            }
            $scope.PayForDataAmount = {
                amountD:'',
                amountDeposit:'',
                max:'',
                id:'',
                amount_left:''
            }
            $scope.getAllTransaction = function (){
                $http.post('controller/loan.php?getAllTransactionLoan',{id: $scope.seasonId}).then(function(response){
                    $scope.getAllTransactionData = response.data;
                    console.log(response);
                });
            }
            $scope.ChangeLoanAmount = function(id){
                var index = $scope.PayForData.findIndex(x=>x.id==id);
                $scope.PayForDataAmount.max = $scope.PayForData[index].amount;
                $scope.PayForDataAmount.amountD = $scope.PayForData[index].amount;
                $scope.PayForDataAmount.amount_left = $scope.PayForData[index].amount_left;
            }
            $scope.saveDepositSlipLoan = function(amountD,amount){
                var remark;
                var types;
                var ttl;
                if($scope.PayForDataAmount.amountDeposit == null || $scope.PayForDataAmount.amountDeposit ==''){
                    swal("Input amount or Select Pay for");
                }
                else if($scope.PayForDataAmount.amountDeposit!= null || $scope.PayForDataAmount.amountDeposit !=''){
                    remark = "For loan";
                    types = "LOAN";
                    if($scope.PayForDataAmount.amount_left!= null){
                        ttl = parseInt($scope.PayForDataAmount.amount_left) - parseInt($scope.PayForDataAmount.amountDeposit);
                    }else{
                        ttl = parseInt($scope.PayForDataAmount.max) - parseInt($scope.PayForDataAmount.amountDeposit);
                    }
                    $http.post("controller/loan.php?insert", {user_id:$scope.getSelectedMemberData.user_id,seasonId: $scope.seasonId, type:types, amount: amountD,remarks:remark,ttl: ttl,id:$scope.PayForDataAmount.id}).then(function(response){
                        if($scope.PayForDataAmount.amountDeposit>=$scope.PayForDataAmount.max){
                            $http.post("controller/loan.php?Update", {id:$scope.PayForDataAmount.id}).then(function(response){
                                console.log(response);
                                if(response.data=true){
                                    swal("Successfully Upload!");
                                    $('#imageupload').modal('hide');
                                    $scope.PayForDataAmount.amountDeposit ='';
                                    $scope.PayForDataAmount.amountD ='';
                                    $scope.PayForDataAmount.max ='';
                                    $scope.PayForDataAmount.id ='';
                                    $scope.PayFor();
                                }else{
                                    swal("error");
                                    console.log(response);
                                }
                            });
                        }else{
                            if(response.data=true){
                            swal("Successfully Upload!");
                            $('#imageupload').modal('hide');
                            $scope.PayFor();
                            $scope.PayForDataAmount.amountDeposit ='';
                            $scope.PayForDataAmount.amountD ='';
                            $scope.PayForDataAmount.max ='';
                            $scope.PayForDataAmount.id ='';
                            }else{
                                swal("error");
                                console.log(response);
                            }
                        }
                    });
                }else{
                    swal("error");
                }
            }
            $scope.getSelectedMember = function(user_id,fname,lname,shares){
                $scope.getSelectedMemberData.user_id = user_id;
                $scope.getSelectedMemberData.fname = fname;
                $scope.getSelectedMemberData.lname = lname;
                $scope.getSelectedMemberData.shares = shares;
                $scope.PayFor();
            }
            $scope.PayFor = function(){
                $http.post('controller/loan.php?PayFor',{seasonId:$scope.seasonId,userId:$scope.getSelectedMemberData.user_id}).then(function(response){
                    console.log(response);
                    $scope.PayForData = response.data;
                    // if(response.data == true){
                    //     swal("success");
                    // }else{
                    //     $('#apply').modal('hide');
                    //     swal('error');
                    //     console.log(response);
                    // }
                });
            }
            $scope.PayFor();
            $scope.getAllSeason = function (){
                $http.post('controller/getAllSeason.php').then(function(response){
                    $scope.allSeasonData = response.data;
                });
            }
            $scope.getAllSeason();
            $scope.ViewSeason = function(id){
                $scope.viewseasons = 'read'; 
                $scope.seasonId = id;
                $scope.getAllReqLoan();
                $scope.getAllJoinedSeason();
                $scope.PayFor();
            }
            $scope.getAllJoinedSeason = function(){
                $http.post('controller/getAllJoinedSeason.php',{id: $scope.seasonId}).then(function(response){
                    $scope.getAllJoinedSeasonData = response.data;
                    console.log(response);
                });
            }
            $scope.getAllReqLoan = function(){
                $http.post('controller/getAllApplyLoan.php',{id:$scope.seasonId}).then(function(response){
                    $scope.getAllApplyLoanData = response.data;
                    
                });
            }
            $scope.getApplyLoanData = function(id,fname,lname,amount,cofname,colname){
                $scope.getApplyLoanDetails.id = id;
                $scope.getApplyLoanDetails.fname = fname;
                $scope.getApplyLoanDetails.lname = lname;
                $scope.getApplyLoanDetails.amount = amount;
                $scope.getApplyLoanDetails.comaker = cofname+" "+colname;

            }
            $scope.declineRequestLoan = function(){
                $http.post('controller/approveDeclineLoan.php?reject',{id:$scope.getApplyLoanDetails.id}).then(function(response){
                    $scope.getAllReqLoan();
                });
            }
            $scope.approveRequestLoan = function(){
                $http.post('controller/approveDeclineLoan.php?accept',{id:$scope.getApplyLoanDetails.id}).then(function(response){
                    $scope.getAllReqLoan();
                });
            }
        });
        app.controller('LoanReqCtrl', function($scope, $http, $filter, $timeout, $cookies){
            $scope.getAllReq = function(){
                $http.post('controller/loanReq.php?getAll').then(function(response){
                    $scope.getAllReqData = response.data;
                    console.log(response);
                });
            }
            $scope.getAllReq();
            $scope.acceptLoanReq = function(id,userId){
                $http.post('controller/loanReq.php?accept',{id:id,userId:userId}).then(function(response){
                    $scope.getAllReq();
                });
            }
            $scope.rejectLoanReq = function(id,userId){
                $http.post('controller/loanReq.php?reject',{id:id,userId:userId}).then(function(response){
                    $scope.getAllReq();
                });
            }
            $scope.print = function(){
                var divToPrint=document.getElementById("order_table1");
                newWin= window.open("");
                newWin.document.write(divToPrint.outerHTML);
                newWin.print();
                newWin.close();
            }
        });
        app.controller('depositCtrl', function($scope, $http, $filter, $timeout, $cookies){
            $scope.getAllTransaction = function (){
                $http.post('controller/getAllTransaction.php').then(function(response){
                    $scope.getAllTransactionData = response.data;
                });
            }
            $scope.getAllTransaction();
            $scope.acceptTransactionPayment = function(id,user_id){
                $http.post('controller/updateStatus.php?accept',{id:id,user_id:user_id}).then(function(response){
                    $scope.getAllTransaction();
                    console.log(response);
                });
            }
            $scope.rejectTransactionPayment = function(id,user_id){
                $http.post('controller/updateStatus.php?reject',{id:id,user_id:user_id}).then(function(response){
                    $scope.getAllTransaction();
                    console.log(response);
                });
            }
            $scope.print = function(){
                var divToPrint=document.getElementById("order_table1");
                newWin= window.open("");
                newWin.document.write(divToPrint.outerHTML);
                newWin.print();
                newWin.close();
            }
        });
        app.controller('userCtrl', function($scope, $http, $filter, $timeout, $cookies){
            $scope.addNewMember = 0;
            $scope.userData;
            $scope.NewMember={
                empId:'',
                fname:'',
                mname:'',
                lname:'',
				email:'',
                phone:'',
                password:'',
                confirmPass:'',
            }
            $scope.userDetails = {
                id:'',
                email:'',
                fname:'',
                lname:'',
                phone:'',
            }
            $scope.print = function(){
                var divToPrint=document.getElementById("order_table1");
                newWin= window.open("");
                newWin.document.write(divToPrint.outerHTML);
                newWin.print();
                newWin.close();
            }
            $scope.addNewMembers = function(){
                if($scope.NewMember.empId == null || $scope.NewMember.empId == ""){
                    swal("Write your Member ID",{
                        icon: "warning",
                        dangerMode: true,
                    });
                }else if($scope.NewMember.fname == null || $scope.NewMember.fname == ""){
                    swal("Write your first name",{
                        icon: "warning",
                        dangerMode: true,
                    });}
                else if($scope.NewMember.mname == null || $scope.NewMember.mname == ""){
                    swal("Write your first name",{
                        icon: "warning",
                        dangerMode: true,
                    });
                }else if($scope.NewMember.lname == null || $scope.NewMember.lname == ""){
                    swal("Write your last name",{
                        icon: "warning",
                        dangerMode: true,
                    });
                }else if($scope.NewMember.email == null || $scope.NewMember.email == ""){
                    swal("Write email",{
                        icon: "warning",
                        dangerMode: true,
                    });
                }else if($scope.NewMember.phone == null || $scope.NewMember.phone == ""){
                    swal("Write phone number",{
                        icon: "warning",
                        dangerMode: true,
                    });
                }else if($scope.NewMember.password == null || $scope.NewMember.password == ""){
                    swal("Write Password",{
                        icon: "warning",
                        dangerMode: true,
                    });
                }else if($scope.NewMember.confirmPass == null || $scope.NewMember.confirmPass == ""){
                    swal("Write Confirm Password",{
                        icon: "warning",
                        dangerMode: true,
                    });
                }else if($scope.NewMember.password != $scope.NewMember.confirmPass){
                    swal("Password And Confirm Password is not Equal",{
                        icon: "warning",
                        dangerMode: true,
                    });
                }else if($scope.NewMember.password == $scope.NewMember.confirmPass){
                    $http.post('controller/CreateUser.php', {UserId: $scope.NewMember.empId, fname: $scope.NewMember.fname, lname: $scope.NewMember.lname, email: $scope.NewMember.email, phone: $scope.NewMember.phone, password: $scope.NewMember.password}).then(function(response){
                        if(response.data = "true"){
                            swal("Successfully Created", {icon: "success",}).then((WillOk)=>{
                                $scope.NewMember.empId = '';
                                $scope.NewClient.fname='';
                                $scope.NewClient.lname='';
                                $scope.NewClient.email='';
                                $scope.NewClient.password='';
                                $scope.NewClient.confirmPass='';
                                    $scope.addNewMember = 0;
                            });
                        }else{
                            swal("Error",{icon: "error", customClass: 'animated tada'}).then((willTrue)=>{
                            });
                        }
                    });
                }else{}
            }
            $scope.activedeactivateStatus = function(id,status){
                $http.post('controller/activatedeactivate.php',{id:id,status:status}).then(function(response){
                    console.log(response);
                    $scope.getallUser();
                });
            }
            $scope.getallUser = function(){
                $http.post('controller/getAllUser.php').then(function(response){
                    console.log(response);
                    $scope.userData = response.data;
                });
            }
            $scope.getallUser();
            $scope.updateUser = function(id,first_name,sur_name,email,mobile_number){
                $scope.userDetails.id = id;
                $scope.userDetails.email = email;
                $scope.userDetails.fname = first_name;
                $scope.userDetails.lname = sur_name;
                $scope.userDetails.phone = mobile_number;
            }
            $scope.saveUpdateUser = function(){
                var index = $scope.userData.findIndex(x=>x.id==$scope.userDetails.id);
                if($scope.userData[index].email == $scope.userDetails.email && $scope.userData[index].first_name == $scope.userDetails.fname && $scope.userData[index].sur_name == $scope.userDetails.lname && $scope.userData[index].mobile_number == $scope.userDetails.phone){
                    swal("no data change");
                }else{
                    if($scope.userDetails.email == null || $scope.userDetails.email == ' '){
                        swal("Write Email");
                    }else if($scope.userDetails.fname == null || $scope.userDetails.fname == ' '){
                        swal("Write First name");
                    }else if($scope.userDetails.lname == null || $scope.userDetails.lname == ' '){
                        swal("Write Last name");
                    }else if($scope.userDetails.phone == null || $scope.userDetails.phone == ' '){
                        swal("Write Mobile number");
                    }else{
                        $http.post('controller/updateDetailsUser.php',{id:$scope.userDetails.id,email:$scope.userDetails.email,fname:$scope.userDetails.fname,lname:$scope.userDetails.lname,phone:$scope.userDetails.phone}).then(function(response){
                            if(response.data = true){
                                $('#exampleModal').modal('hide');
                                swal('successfully update');
                            }else{
                                swal('error');
                            }
                        });
                    }
                }
            }
            $scope.addButton = function(){
                $scope.addNewMember = 1;
            }
            $scope.cancelButton = function(){
                $scope.addNewMember = 0;
            }
        });
        
    </script>
  </body>


</html>