<?php include 'header.php'; ?>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content card-box">
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-12">
            
            <div class="card-head">
              <header>Basic Information</header>
            </div>
            <div class="card-body row">
              <div class="col-lg-6 p-t-20">
                <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                  <input class = "mdl-textfield__input" type = "text" id = "txtFirstName">
                  <label class = "mdl-textfield__label">First Name</label>
                </div>
              </div>
              <div class="col-lg-6 p-t-20">
                <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                  <input class = "mdl-textfield__input" type = "text" id = "txtLasttName">
                  <label class = "mdl-textfield__label" >Last Name</label>
                </div>
              </div>
              <div class="col-lg-12 p-t-20">
                <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                  <input class = "mdl-textfield__input" type = "text" id = "txtAmount">
                  <label class = "mdl-textfield__label">Amount</label>
                </div>
              </div>
              <div class="col-lg-12 p-t-20">
                <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                  <input class = "mdl-textfield__input" type = "text" id = "txtComaker">
                  <label class = "mdl-textfield__label" >Co-Maker</label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink"">Save changes</button>
      </div>
    </div>
  </div>
</div>

  <!-- start page content -->
  <div class="page-content-wrapper">
    <div class="page-content">
      <div class="page-bar">
        <div class="page-title-breadcrumb">
          <div class=" pull-left">
            <div class="page-title">View Loans (Season #)</div>
          </div>
          <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Loans</li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="card  card-box">
            <div class="card-head">
              <header>Season Details</header>
              <div class="tools">
                <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
              </div>
            </div>
            <div class="card-body ">
              <div class="table-wrap">
                <div class="table-responsive">
                  <table class="table display product-overview mb-30" id="support_table5">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Name</th>
                        <th>Check In</th>
                        <th>Check Out</th>
                        <th>Status</th>
                        <th>Phone</th>
                        <th>Room Type</th>
                        <th>Edit</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>Jens Brincker</td>
                        <td>23/05/2016</td>
                        <td>27/05/2016</td>
                        <td>
                          <span class="label label-sm label-success">Approved</span>
                        </td>
                        <td>123456789</td>
                        <td>Single</td>
                        <td>
                          <a href="#" class="btn btn-tbl-edit btn-xs">
                          <i class="fa fa-pencil"></i>
                          </a>
                          <button class="btn btn-tbl-delete btn-xs">
                          <i class="fa fa-trash-o "></i>
                          </button>
                        </td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>Mark Hay</td>
                        <td>24/05/2017</td>
                        <td>26/05/2017</td>
                        <td>
                          <span class="label label-sm label-warning" style="cursor: pointer;" data-toggle="modal" data-target="#exampleModal">Pending </span>
                        </td>
                        <td>123456789</td>
                        <td>Double</td>
                        <td>
                          <a href="#" class="btn btn-tbl-edit btn-xs">
                          <i class="fa fa-pencil"></i>
                          </a>
                          <button class="btn btn-tbl-delete btn-xs">
                          <i class="fa fa-trash-o "></i>
                          </button>
                        </td>
                      </tr>

                      <tr>
                        <td>4</td>
                        <td>David Perry</td>
                        <td>19/04/2016</td>
                        <td>20/04/2016</td>
                        <td>
                          <span class="label label-sm label-danger">Declined</span>
                        </td>
                        <td>123456789</td>
                        <td>King</td>
                        <td>
                          <a href="#" class="btn btn-tbl-edit btn-xs">
                          <i class="fa fa-pencil"></i>
                          </a>
                          <button class="btn btn-tbl-delete btn-xs">
                          <i class="fa fa-trash-o "></i>
                          </button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end page content -->

<?php include 'footer.php'; ?>

<script type="text/javascript">
  $("#loans").addClass("active");
</script>