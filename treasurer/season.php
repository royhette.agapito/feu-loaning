<?php include 'header.php'; ?>
<style type="text/css">
  .place { width: 12px; height: 12px; background-color: green;  border-radius: 50%;}
  .place.green { background-color: red; }
</style>
<!-- start page content -->
<div class="page-content-wrapper">
  <div class="page-content">
    <div class="page-bar">
      <div class="page-title-breadcrumb">
        <div class=" pull-left">
          <div class="page-title">Seasons</div>
        </div>
        <ol class="breadcrumb page-breadcrumb pull-right">
          <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
          </li>
          <li class="active">Seasons</li>
        </ol>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card card-topline-green">
          <div class="card-head">
            <header>List of Seasons</header>
            <div class="tools">
              <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
            </div>
          </div>
          <div class="card-body ">
            <div class="row p-b-20">
              <div class="col-md-6 col-sm-6 col-6">
                <div class="btn-group">
                  <button id="addRow1" class="btn btn-info">
                  Add New <i class="fa fa-plus"></i>
                  </button>
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-6">
                <div class="btn-group pull-right">
                  <button class="btn deepPink-bgcolor  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                  <i class="fa fa-angle-down"></i>
                  </button>
                  <ul class="dropdown-menu pull-right">
                    <li>
                      <a href="javascript:;">
                      <i class="fa fa-print"></i> Print </a>
                    </li>
                    <li>
                      <a href="javascript:;">
                      <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                    </li>
                    <li>
                      <a href="javascript:;">
                      <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            
            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover table-checkable order-column full-width" id="example4">
                <thead>
                  <tr>
                    <th>
                      <label class="rt-chkbox rt-chkbox-single rt-chkbox-outline">
                      <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                      <span></span>
                      </label>
                    </th>
                    <th style="display: none;"> Status </th>
                    <th> Status </th>
                    <th> Username </th>
                    <th> Email </th>
                    <th> Joined </th>
                    <th> Actions </th>
                  </tr>
                </thead>
                <tbody>
                  <tr class="odd gradeX">
                    <td>
                      <label class="rt-chkbox rt-chkbox-single rt-chkbox-outline">
                      <input type="checkbox" class="checkboxes" value="1" />
                      <span></span>
                      </label>
                    </td>
                    <td>
                      <center><div class="place"></div></center>
                    </td>
                    <td> shuxer </td>
                    <td>
                      <a href="mailto:shuxer@gmail.com"> shuxer@gmail.com </a>
                    </td>
                    <td> 12 Jan 2012 </td>
                    <td class="valigntop">
                      <button class="btn btn-primary btn-xs tooltips" data-placement="top" data-original-title="Edit">
                        <i class="fa fa-pencil"></i>
                      </button>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- end page content -->

<?php include 'footer.php'; ?>
<script type="text/javascript">
  $("#season").addClass("active");

$(".place").click(function () {
   $(this).toggleClass("green");
});
</script>