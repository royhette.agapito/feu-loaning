<?php include 'header.php'; ?>
        <!-- start page content -->
        <div class="page-content-wrapper">
          <div class="page-content" ng-if=" addNewMember ==1">
            <div class="page-bar">
              <div class="page-title-breadcrumb">
                <div class=" pull-left">
                  <div class="page-title">Add Member Details</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                  <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                  </li>
                  <li><a class="parent-item" href="user.php">Users</a>&nbsp;<i class="fa fa-angle-right"></i>
                  </li>
                  <li class="active">Add Member Details</li>
                </ol>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="card-box">
                  <div class="card-head">
                    <header>Basic Information</header>
                  </div>
                  <div class="card-body row">
                    <div class="col-lg-6 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class = "mdl-textfield__input" type = "text" ng-model="NewMember.empId">
                        <label class = "mdl-textfield__label">Employee ID</label>
                      </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class = "mdl-textfield__input" type = "email"  ng-model="NewMember.email">
                        <label class = "mdl-textfield__label" >Email</label>
                        <span class = "mdl-textfield__error">Enter Valid Email Address!</span>
                      </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class = "mdl-textfield__input" type = "text" ng-model="NewMember.fname">
                        <label class = "mdl-textfield__label">First Name</label>
                      </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class = "mdl-textfield__input" type = "text" ng-model="NewMember.mname">
                        <label class = "mdl-textfield__label">Middle Name</label>
                      </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class = "mdl-textfield__input" type = "text" ng-model="NewMember.lname">
                        <label class = "mdl-textfield__label" >Last Name</label>
                      </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class = "mdl-textfield__input" type = "text" ng-model="NewMember.phone" 
                          pattern = "-?[0-9]*(\.[0-9]+)?" id = "text5">
                        <label class = "mdl-textfield__label" for = "text5">Mobile Number</label>
                        <span class = "mdl-textfield__error">Number required!</span>
                      </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class = "mdl-textfield__input" type = "password" ng-model="NewMember.password">
                        <label class = "mdl-textfield__label" >Password</label>
                      </div>
                    </div>
                    <div class="col-lg-6 p-t-20">
                      <div class = "mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                        <input class = "mdl-textfield__input" type = "password" ng-model="NewMember.confirmPass">
                        <label class = "mdl-textfield__label" >Confirm Password</label>
                      </div>
                    </div>
                    
                    <div class="col-lg-12 p-t-20 text-center"> 
                      <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink" ng-click="addNewMembers()">Submit</button>
                      <button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default" ng-click="cancelButton()">Cancel</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- end page content -->
      </div>
      <!-- end page container -->

<?php include 'footer.php'; ?>

<script type="text/javascript">
	$("#users").addClass("active");
</script>