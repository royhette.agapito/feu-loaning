<?php include 'header.php'; ?>


  <!-- start page content -->
  <div class="page-content-wrapper" ng-controller="depositCtrl" ng-cloak>
    <div class="page-content">
      <div class="page-bar">
        <div class="page-title-breadcrumb">
          <div class=" pull-left">
            <div class="page-title">Deposit Slips</div>
          </div>
          <ol class="breadcrumb page-breadcrumb pull-right">
            <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
            </li>
            <li class="active">Deposit Slips</li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="card  card-box">
            <div class="card-head">
              <header>Slips Details</header>
              <div class="tools">
                <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
              </div>
            </div>
            <div class="card-body ">
              <div class="row p-b-20">
                <div class="col-md-6 col-sm-6 col-6">
                </div>
                <div class="col-md-6 col-sm-6 col-6">
                  <div class="btn-group pull-right">
                    <button class="btn deepPink-bgcolor  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                    <i class="fa fa-angle-down"></i>
                    </button>
                    <ul class="dropdown-menu pull-right">
                      <li>
                        <a href="javascript:;" ng-click="print()">
                        <i class="fa fa-print"></i> Print </a>
                      </li>
                      <li>
                        <a href="javascript:;">
                        <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                      </li>
                      <li>
                        <a href="javascript:;">
                        <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="table-wrap">
                <div class="table-responsive">
                  <table id="order_table1" datatable="ng" dt-options="vm.dtOptions" class="table display product-overview mb-30">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Date Created</th>
                        <th>Type</th>
                        <th>Employee</th>
                        <th>Status</th>
                        <th>Season Id</th>
                        <th>Image</th>
                        <th>Amount</th>
                        <th>Options</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="odd gradeX" ng-repeat="data in getAllTransactionData">
                        <td>{{data.id}}</td>
                        <td>{{data.date_created}}</td>
                        <td>{{data.type}}</td>
                        <td>{{data.first_name + " "+ data.sur_name}}</td>
                        <td>
                          <span ng-if="data.status==null" class="label label-sm label-danger">Pending</span>
                          <span ng-if="data.status==1" class="label label-sm label-success">Approved</span>
                          <span ng-if="data.status==0" class="label label-sm label-danger">Declined</span>
                        </td>
                        <td>{{data.season_id}}</td>
                        <td><img class="img-responsive" src="../assets/uploads/{{data.image}}" alt=""></td>
                        <td>{{data.amount}}</td>
                        <td>
                          <a ng-if="data.status==null" ng-click="acceptTransactionPayment(data.id,data.user_id)" href="" class="btn btn-tbl-edit btn-xs btn-success tooltips" data-placement="top" data-original-title="Approve">
                          <i class="material-icons">thumb_up</i>
                          </a>
                          <a ng-if="data.status==null" ng-click="rejectTransactionPayment(data.id,data.user_id)" href="" class="btn btn-tbl-edit btn-xs btn-danger tooltips" data-placement="top" data-original-title="Reject">
                          <i class="material-icons">thumb_down</i>
                          </a>
                          <a ng-if="data.status!=null" href="" style="visibility: hidden;" class="btn btn-tbl-edit btn-xs btn-danger tooltips" data-placement="top" data-original-title="Reject">
                            <i class="material-icons">thumb_down</i>
                          </a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end page content -->

<?php include 'footer.php'; ?>

<script type="text/javascript">
  $("#deposit").addClass("active");
</script>